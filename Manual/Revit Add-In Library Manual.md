<link href="styles.css" rel="stylesheet"></link>

<img src="images\RevitAddInLibrary.png" align="left" style="margin-right:10px;">
#Revit Add-In Library Manual#


Version: 0.5.0  
Author: Eric Anastas  
Email: eric.anastas@som.com  



The Revit Add-In Library suite of tools provides a way to easily package, distribute, and update add-ins for Autodesk Revit. Add-Ins are bundled into versioned packages, which are saved to a central library. Users can then install the tools of their choice from the central library. Later updates can be rolled-out by adding new versions of each add-in to the library. The suite consists of three tools:


Revit Add-In Library Manager (RvtAddInLibManager.exe)    
: Creates add-in packages and manages the central library

![Revit Add-In Library Manager](images\libmanager.png)
     



Revit Add-In Library Installer (RvtAddInLibInstaller.exe)  
: Installs, uninstalls, and upgrades add-ins from the central library on a user's computer.

![Revit Add-In Library Installer](images\installer.png)

Revit Add-In Library Utility (RvtAddInLibUtil.exe)
: A console application which can be used to automate installing and upgrading add-ins as well as managing an add-in library.

![Revit Add-In Library Utility](images\RvtAddInLibUtil.png)  



#Installation and Setup#

The Revit Add-In Library suite is installed through an MSI installer file. There are a couple configuration settings which can be set automatically during installation by running the installer from the command line. These properties are explained in detail later in this document.  

LIBRARY
: The path to the add-in library. Typically this is a shared network directory which users have access to.

REQLIST  
: The path to the required add-ins list. This is a text file which lists the add-ins which all users must have installed.


To set these properties during installation the MSI should not be launch directly, but instead installed using [msiexec](http://www.microsoft.com/resources/documentation/windows/xp/all/proddocs/en-us/msiexec.mspx?mfr=true). The following example shows how these properties can be passed to the installer. 

    msiexec /i "C:\Revit Add-In Library.msi" /qb- LIBRARY="Z:\Revit Add-In Library"
	REQLIST="Z:\Revit Add-In Library\required_packages.txt"


##Manual Configuration##

    

These settings can also be set or changed after installation by editing a configuration file in the installation directory. Open `RvtAddInLib.config`, which will be located in `%ProgramFiles%\Revit Add-In Library\`. The file contains values for `Library`, and `RequiredList` as shown below.

	<?xml version="1.0" encoding="utf-8"?>
	<appSettings>
  		<add key="Library" value="U:\Revit Add-In Library"/>
  		<add key="RequiredList" value="U:\Revit Add-In Library\required_packages.txt"/>
	</appSettings>




##Library Setup##

The Revit Add-In Library Utility tool can be used to initialize a new library using the `new-library` command. This command takes a single option which is the directory path of the new library.

    RvtAddInLibUtil new-library -l "X:\New Library Directory\"

Typically an add-n library is setup in a central shared network directory which users have at least read access to. Developers or anyone else that will administer the library will need write access to the library folder.


##Required Add-Ins List##


The required add-ins list specifies a list of add-ins that must be installed. Users will be prevented from uninstalling these add-ins, and the update process will install them automatically if missing.

This list is stored as a simple text file. Each line identifies a release of Revit and a required package code for that release separated by a single space as show in the example below.

 
    2013 som.activitylog
    2013 som.viewowner
    2013 som.ddglinks
    2014 som.activitylog
    2014 som.viewowner

While it is best to have a single add-in library, different users or groups of users may possibly reference different required add-ins lists depending on their needs. New add-ins can easily be pushed out to users by adding entries to the required add-ins file.




#Packaged Add-In Development#


For the most part developing a Revit add-in for a package is just like developing any other Revit add-in. There are, however, a few restrictions.

The Revit Add-In Library package installation process is much less advanced then other installation technologies. Package installation simply consists of copying the package files to an installation directory, copying the add-in manifest to the Revit `Addins` directory, and finally repathing any file references in the add-in manifest to the installation directory.

The root installation directory will vary between users. Therefore packaged add-ins must not depend on being installed to any specific path, nor should they reference any files by an absolute path. Packaged add-ins must not create, delete, modify, or otherwise depend on files outside of the root installation directory. Instead any references to files by the add-in must be relative to the package/install directory root.    

The C# example below shows how to create a custom ribbon with relative references to the button icon images and assembly file.

    Result IExternalApplication.OnStartup(UIControlledApplication application)
    {
		string assemblyLoc = System.Reflection.Assembly.GetExecutingAssembly().Location;
		string executingDir = Path.GetDirectoryName(assemblyLoc);

		PushButtonData customButtonData = new PushButtonData("CustomButton", "My Command", assemblyLoc, "Acme.CustomToolCommand");
		string imagePath = Path.Combine(executingDir, "custom_16.png");
		string largeImagePath = Path.Combine(executingDir, "custom_32.png");
		customButtonData.Image = new System.Windows.Media.Imaging.BitmapImage(new Uri(imagePath));
		customButtonData.LargeImage = new System.Windows.Media.Imaging.BitmapImage(new Uri(largeImagePath));

		try
		{
                application.CreateRibbonTab("Custom Ribbbon");
		}
		catch (Exception)
		{
			//Catches errors if a ribbon tab named "Custom Ribbon" already exists
		}

		var customPanel = application.CreateRibbonPanel("Custom Ribbbon", "View Reference Graph");
		customPanel.AddItem(graphViewReferencesButtonData);
		return Result.Succeeded;
	}

Additionally, packaged add-ins must not depend on the installation of other libraries or tools. Any third party assemblies required by the add-in must be included with the package files, and must not require any kind of separate installation. 


##Add-In Documentation##

Add-ins which will be exposed to users through controls on the Revit ribbon should also provide documentation for the users. This documentation is best exposed through the contextual help of the ribbon controls. The following example shows how this is done through the Revit API in C#. 

	customButtonData.SetContextualHelp(new ContextualHelp(ContextualHelpType.Url,Path.Combine(executingFolder,"My Command Help.htm")));
	customButtonData.ToolTip = "A sample command";
	customButtonData.LongDescription = "This button does not do anything. It is just sample for the Revit Add-In Library Manual";
    customButtonData.ToolTipImage = new System.Windows.Media.Imaging.BitmapImage(new Uri(toolTipImgPath));


Here is the resulting context menu displayed in Revit after holding the cursor over the button. Pressing F1 will open `My Command Help.htm` in the user's default browser.

![Contextual Help Sample](images\contexthelp.png)


#Revit Add-In Library Manager#


The Revit Add-In Library Manager tool is used to manage a central library of Revit add-in packages. The list on the right displays all the packages grouped by package code. Expanding each package code section show additional details about each version including the version number, version code, target Revit release, and current status. Selecting any version will display its details on the right.

![Revit Add-In Library Manager](images\ManagerNotes.png)






##New Add-In Packages##

The Revit Add-In Library Manager is used to bundle the files of a Revit add-in into a package and save it to the library.

Under the `Library` menu choose `New Package`. This will start the new package wizard.

![New Package](images\newpackage.png) 


###Package Information Template###

The first page allows for the selection of a template package. Selecting a template is useful when creating updates to previous packages, as it will pre-populate many of the values on the next `Package Information` page.

 
![Package Information Template](images\packageinfotemp.png)

Click next to move on to the Package Information Page.


###Package Information###

The Package Information page is used to specify various meta-data about the new package.

![Package Information](images\packageinfo.png)



Package Name 
: The name of the package displayed to users.


Package Code  
: The package code is a short text string used to identify packages which are different versions of the same tool. The installer will not allow two packages with the same package code to be installed for the the same release of Revit.

: Care must be put into selecting an appropriate package code, as it can not be changed in the future. If there are plans to add additional functionality to a tool it is better to select a more generic package code.

: Package codes can only contain lower case letters, numbers, periods, and hyphens. The recommended format for a package code is `company.toolname`. Where `company` is the domain name of the company or person that developed the tool, and `toolname` is the name of the tool. Here are a few example package names.
 
<!-- -->

	som.viewowner
	mertens3d.hatch22
	coins-global.autosectionbox
	autodesk.etransmit
  

Author
: The name of the author of the add-in.
  
Email  
: An email address to contact for support or questions.

Link  
: A website URL to more information about the add-in.

Package Notes  
: A short description of the add-in and its functionality.

Version Number  
: A text string used to identify the version of a package to the user. This may be any code desired, as this field is not used to determine the order of package versions. The library will not allow adding two packages with the same version number and package code.  

Version Code  
: An integer value that represents the version of the application code, relative to other versions. This value is used to determine if one package is more recent then another. Larger values are considered more recent. 

 

Revit Release  
: The release of Revit which the package targets (2013, 2014, 2015...).


###Package Files###

The package files page is used to specify the source directory for all the package files and sub directories. Either enter a directory path in the `Source Directory` field or click `Select Directory` to browse to a directory.

The entire contents of the selected directory, and any sub directories, will be saved with the new package. 

![Package Files](images\packagefiles.png)


###Revit Add-In Manifest###

The specified source directory must contain a single Revit add-in manifest file. This file may contain any number of `Application`, `DbApplication`, or `Command` `AddIn` elements.

This file must be named according to the package code with `*.addin` for the extension. For example if the package code is `acme.testpackage` then the add-in manifest file must be named `acme.testpackage.addin`.

Furthermore, any references to files by the add-in manifest, such as add-in assemblies or images, must be relative and exist in the source directory or sub-directories. 

###Package Summary###

Finally, the Package Summary page summarizes the contents of the new package that is about to be created. 

![Package Summary](images\packagesummary.png)

####Destination####
New packages can either be saved directly to the active library, or saved to an independent package file. Package files can be installed manually through the Revit Add-In Library Installer or later added to a package library.



##Package Library Status##


Each package in the library is assigned a status which controls its visibility to users and the how it should be treated by the update process. The table below describe the three possible package statues.

|Status| Description | Visibility |Installation/Updates|
|:---|:---|:---|:---|
|Unpublished| Has not been officially released | Users will not see the package by default in the installer tool, but may choose to display unpublished versions. | The package version is not installed or upgraded to automatically. A user must manually choose to display and install an unpublished package.
|Published| Officially released | Package will be visible in the installer tool by default. | The upgrade process will automatically upgrade users to the latest published version of each package.
|Suspended| Should not be installed | Suspended packages will not be visible to users in the installer tool. | If a user has a suspended package version installed the upgrade process will upgrade or downgrade them to the latest published version. If no versions of the package are published in the library the package will be uninstalled.|

 
###Setting Package Status###

After a new package has been added to the library it will be set to the default status of `Unpublished`. To change the status of a package select it in the list and click the `Set Status` button. This will open the dialog show below. Click the button for the desired status.
 

![Set Package Status](images\setstatus.png)



##Downloading Library Packages##

Packages can be downloaded from the library as an independent package file, possibly for backup or archive purposes. To download a package select the desired version in the list, and click the `Download` button at the bottom of the window.

![Set Package Status](images\downloadlibpack.png)


##Deleting Library Packages##

Package versions can be deleted from library by selecting a version in the list on the left and clicking the `Delete From Library` button at the bottom of the window. A confirmation dialog will be displayed. Clicking `Yes` will permanantly delete the package version from the library. There is no way to undo this action.

In most cases previous versions of add-ins should be left in the library. This retains a history of the add-in, and it makes it possible to easily roll users back to previous versions if needed. 

![Set Package Status](images\deletelibpack.png)


#Revit Add-In Library Installer#

The Revit Add-In Library installer is used to manage the installed add-in on a client computer. It can install and upgrade add-ins from the central library or stand alone add-in package files.

The main window displays a list of all add-ins available in the library as well as installed add-ins. The list is filtered by the Revit release selected in the drop down at the top of the window. Changing this will show add-ins for other releases. Clicking `Refresh` will refresh the whole list by reading the latest add-ins from the library and the currently installed add-ins.

![Set Package Status](images\installer_refresh.png)



##Installing Library Add-Ins##

To install an add-in package click the `Install` button. This will install the latest published version of the add-in for the currently selected release of Revit.

![Install](images\install.png)

After installation the installed version will be indicated in the top right of the add-in pane.

![Installed](images\installed.png)


##Installing Package Files##

To install a package directly from a file, click the `Install From File...` button at the top of the window. A file open dialog will open. Select the package file to install and click `Open`.

![Install From File](images\installfromfile.png)

Add-in versions that are installed from a file, and are not in the library will be identified by a grey `Not in Library` status box.

![Not in Library](images\notinlibrary.png)




##Uninstalling Add-Ins##

To uninstall installed add-ins click the `Uninstall` button on the add-in pane.

This button may be disable for some add-ins. This occurs when an add-in is identified as being required, and uninstalling is not allowed. This behavior can be disabled temporarily by holding down `Shift` while starting the Revit Add-In Library Installer application. This will ignore the required add-ins list, and allow any add-in to be uninstalled.


![Set Package Status](images\uninstall.png)



##Versions##

The version history of each add-in package can be displayed by expanding the `Versions` section of a package pane.

![](images\versions.png)


##Unpublished Versions##

Package versions in the library are assigned a development status of Published, Unpublished, or Suspended. Published versions are official versions that are always displayed. Unpublished versions are versions that are being tested, and are not visible by default. 

Unpublished versions can be displayed and installed manually by checking `Show Unpublished`. This will display any available unpublished versions in the `Versions` section of each package pane. 
 

![Unpublished Versions](images\unpublished.png)



Clicking install will install the unpublished version. Any installed unpublished add-ins are identified by a yellow unpublished status box, and will be displayed regardless of the status of the `Show Unpublished` check box.

![Unpublished Installed](images\unpublished_installed.png)



Checking `Show Unpublished` may also display additional add-ins to install which have no published versions. These will also be identified by a yellow unpublished status box. These unpublished add-ins can only be installed by expanding the `Versions` section and selecting the unpublished version to install.


![Unpublished Package](images\unpublished_package.png) 



##Updating, Reverting, and Downgrading Add-Ins##

If a newer published versions of an add-in is available the `Install` button will be replaced with an `Update` button which will update to the latest published version.

![Set Package Status](images\update.png)

If an unpublished version of an add-in is installed that is newer then the latest published version, a `Revert` button will be displayed. This will revert to the current official published version.

![Set Package Status](images\revert.png)

If the installed version of an add-in has been suspended a `Downgrade` button will be displayed which will downgrade to the latest published version.

![Set Package Status](images\downgrade.png)




#Revit Add-In Library Utility#

The Revit Add-In Libary Utility is a console application which can be used to automate tasks associated with installing packaged add-ins, creating add-in packages, and managing a package library.

It is run at the command prompt by passing arguments to specify the action to perform. These arguments start with a command name follow by a series of options.


    RvtAddInLibUtil <command> [options]


The following is a summary of the available commands, which are covered in details in the following sections.

list
: Lists the installed packages.

install
: Installs a package from the library.

reinstall
: Reinstalls a currently installed package from the library.

install-file
: Installs a package file.

update
: Updates a package to the latest version in the library.

update-all
: Updates installed packages, and installs missing required add-ins.

uninstall
: Uninstalls an installed package.

uninstall-all
: Uninstalls all installed packages.

new-library
: Creates a new package library.

list-library
: Lists the contents of a package library.

build-package
: Builds a new package file from source files.

add-package
: Adds a package file to the library.

delete-package
: Deletes a package from the library.

download-package
: Downloads a package from the library.

set-status
: Sets the status of a package in the library.



##list##


Lists the installed packages.

    RvtAddInLibUtil list [-r 2013] [-p packagecode]

p, PackageCode
: The package code of the installed package(s) to list.

r, Release
: The release of Revit to list installed packages from.



##install##

Installs a package from the library.

    RvtAddInLibUtil install -p packagecode -r 2013 [-v 1.0]

p, PackageCode
: Required. The package code.

r, Release
: Required. Release of Revit (2013, 2014, 2015 etc..)

v, Version
: Specific version to install from library.

<!-- -->  

    RvtAddInLibUtil install -p packagecode -r 2013

- Installs the latest version in the library.
- Does nothing if any version of package is already installed
  
<!-- -->  

    RvtAddInLibUtil install -p packagecode -r 2013 -v 1.0

- Installs the specified version from the library.
- Upgrades/downgrades if another version is already installed
- Does nothing if specified version is already installed.




##install-file##

Installs a package file.

    RvtAddInLibUtil install-file -f "C:\Path\To\File.rpk"

- Installs the specified package file
- Any existing installed versions of the package for the target release are
replaced

f, PackageFile
: Required. Package file to install.



##reinstall##

Reinstalls a currently installed package from the library.

    RvtAddInLibUtil reinstall -p packagecode -r 2013

- Reinstalls the currently installed version from library
- Does nothing if package is not currently installed
- Fails if installed version can not be found in the library

p, PackageCode
: Required. The package code.

r, Release
: Required. Release of Revit (2013, 2014, 2015 etc..)


##update##

Updates a package to the latest version in the library.

    RvtAddInLibUtil update -p packagecode -r 2013

- Does nothing if no version of the package is installed
- Upgrades to latest published version in library if it is newer than installed
- Does nothing if installed version is newer then published and not suspended
- Upgrades/downgrades to latest published version if installed version is
suspended
- Uninstalls if installed version is suspended and no versions are published

p, PackageCode
: Required. The package to update.

r, Release
: Required. The target release of Revit to update the package.



##update-all##

Updates installed packages, and installs missing required add-ins.

    RvtAddInLibUtil update-all [-i] [-r 2013]

i, installreq
: (Default: False) If true update process will install missing required add-ins, otherwise if false will only update currently installed packages.

r, release
: The release of Revit to update add-ins for. If omitted all installed releases are updated.



##uninstall##

Uninstalls an installed package.

    RvtAddInLibUtil uninstall -r 2013 -p packagecode

r, Release
: Required. The release of Revit to uninstall from.

p, PackageCode
: Required. The package to uninstall.



##uninstall-all##

Uninstalls all installed packages.

    RvtAddInLibUtil uninstall-all [-r 2013]

r, Release
: The release of Revit to uninstall all add-ins from. If omitted all add-ins from all releases will be uninstalled.


##new-library##

Creates a new package library.

    RvtAddInLibUtil new-library -l "X:\New Library Directory\"

l, library
: Required. Directory to create a new library in. If directory does not exist it will be created.



##list-library##

Lists the contents of a package library.

	RvtAddInLibUtil list-library [-p packagecode] [-r 2013]

p, PackageCode
: The package code of the library package(s) to list.

r, release
: The release of Revit to list library packages from.



##add-package##

Adds a package file to the library.

	RvtAddInLibUtil add-package -f "C:\Path\To\File.rpk"

f, PackageFile
: Required. Package file to add to the library.



##delete-package##

Deletes a package from the library.

	RvtAddInLibUtil delete-package -p packagecode -v 1.0

p, PackageCode
: Required. The package code of the package.

v, Version
: Required. The version of the package.


##download-package##

Downloads a package from the library.

	RvtAddInLibUtil download-package [-f "C:\file.rpk] -p packagecode -v 1.0

f, PackageFile
: Destination package file to save. If omitted the packages will be saved as "packagecode_versionnumber.rpk"

p, PackageCode
: Required. The package code of the package.

v, Version
: Required. The version of the package.



##set-status##

Sets the status of a package in the library.

	RvtAddInLibUtil set-status -p packagecode -v 1.0 -s Published

p, PackageCode
: Required. The package code of the package.

v, Version
: Required. The version of the package.

s, Status
: Required. The new package status: `Published`, `Unpublished`, or `Suspended`.


<link href="styles.css" rel="stylesheet"></link>


#Data Storage Formats#


The following specifies the various data storage mechanisms used by the Revit Add-In Library tools.


##Package Files##

Revit Add-In Library packages are stored as package files. Package files are in reality regular Zip files which contain Revit add-in files, a Revit add-in manifest XML file, and a `Package.xml` file.


###Package.xml###

Package files must contain a `Package.xml` file in the root of the archive. This file contains metadata about the package in the following format:



	<Package xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
		<PackageName>Test Package</PackageName>
		<PackageCode>com.acme.testpackage</PackageCode>
		<PackageNotes>This is a test package</PackageNotes>
		<Author>John Smith</Author>
		<Email>jsmith@acme.com</Email>
		<Link>http://www.acme.com/</Link>
        <RevitRelease>2013</RevitRelease>
		<VersionNumber>1.0.0</VersionNumber>
		<VersionCode>1</VersionCode>
		<VersionNotes>Test version</VersionNotes>
	</Package>


 




###Add-In Manifest###

A package file must contain a Revit add-in manifest XML file named according to the package code specified in `Package.xml`. For example if the package code is `acme.sampletool` the add-in manifest must be named `acme.sampletool.addin`. This file must be located at the root of the package file. Any references to files by the manifest must relative to the root of the package Zip file.




##Package Library##

A package library stores a collection of packages in a shared directory.

###LibraryPackages.xml###

The root of a library contains a `LibraryPackages.xml` file which keeps a record of all the packages that have been added to the library.

    %LibraryRoot%\LibraryPackages.xml

This file has the following format:

	<LibraryPackages xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
		<LibraryPackage>
			<Package xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
				<PackageName>Test Package</PackageName>
				<PackageCode>com.acme.testpackage</PackageCode>
				<PackageNotes>This is a test package</PackageNotes>
				<Author>John Smith</Author>
				<Email>jsmith@acme.com</Email>
				<Link>http://www.acme.com/</Link>
    	    	<RevitRelease>2013</RevitRelease>
				<VersionNumber>1.0.0</VersionNumber>
				<VersionCode>1</VersionCode>
				<VersionNotes>Test version</VersionNotes>
			</Package>
			<AddedOnUTC>2013-10-23T09:15:07.8536568Z</AddedOnUTC>
			<AddedBy>user.name</AddedBy>
			<FileSize>487863</FileSize>
			<Status>Published</Status>
		</LibraryPackage>

		<!-- Additional LibraryPackage Elements-->

	</LibraryPackages>
	
Package files are stored in sub-directories of the library directory in the following format:


	%LibraryRoot%\<packagecode>\<packagecode>_<versionnumber>.rpk

For example:

	%LibraryRoot%\acme.sampletool\acme.sampletool_1.0.0.rpk
	%LibraryRoot%\acme.sampletool\acme.sampletool_2.0.0.rpk
	%LibraryRoot%\acme.testtool\acme.testtool_2.0.0.rpk
	

	

##Package Installation##

Revit Add-In Library installs package files to the current user's `%AppData%` directory.

###InstalledPackages.xml###

A record of the installed packages for each user is stored in a `InstalledPackages.xml` file located at the following path.

	%AppData%\Roaming\Revit Add-In Library\InstalledPackages.xml

`InstalledPackages.xml` has the following format:


	<InstalledPackages xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
		<InstalledPackage>
			<Package xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
				<PackageName>Test Package</PackageName>
				<PackageCode>com.acme.testpackage</PackageCode>
				<PackageNotes>This is a test package</PackageNotes>
				<Author>John Smith</Author>
				<Email>jsmith@acme.com</Email>
				<Link>http://www.acme.com/</Link>
    	    	<RevitRelease>2013</RevitRelease>
				<VersionNumber>1.0.0</VersionNumber>
				<VersionCode>1</VersionCode>
				<VersionNotes>Test version</VersionNotes>
			</Package>
			<InstalledOnUTC>2013-10-23T09:30:54.4576568Z</InstalledOnUTC>
			<InstallDirectory>C:\Users\Eric.Anastas\AppData\Roaming\Revit Add-In Library\2013\som.activitylog</InstallDirectory>
		</InstalledPackage>

		<!-- Additional InstalledPackage elements -->

	</InstalledPackages>


###Installation Directory###

Durring installation a package's files, excluding `Package.xml` and the add-in manifest, are copied to an installation directory according to the following format.


	%AppData%\Roaming\Revit Add-In Library\<RevitRelease>\<PackageCode>

For example:

	%AppData%\Roaming\Revit Add-In Library\2013\acme.sampletool\
	%AppData%\Roaming\Revit Add-In Library\2014\acme.sampletool\
	%AppData%\Roaming\Revit Add-In Library\2014\acme.testtool\


###Revit Add-In Manifest Files###

 During installation the Revit add-in manifest file in the package is copied to the standard user specific Autodesk add-in directory. 

	%AppData%\Roaming\Autodesk\Revit\Addins\<RevitRelease>\<PackageCode>.addin

For example:

	%AppData%\Roaming\Autodesk\Revit\Addins\2013\acme.sampletool.addin
	%AppData%\Roaming\Autodesk\Revit\Addins\2014\acme.sampletool.addin
	%AppData%\Roaming\Autodesk\Revit\Addins\2014\acme.testtool.addin

Any file references in the add-in manifest are repathed to the installation directory described above after it is copied.