﻿using System;
namespace RevitAddInLibrary
{
    public interface IPackageInstaller
    {
        InstalledPackage GetInstalledPackage(string packageCode, int revitRelease);
        System.Collections.Generic.IList<InstalledPackage> GetInstalledPackages();
        InstallResult Install(IPackageLibrary library, int revitRelease, string packageCode);
        InstallResult Install(IPackageLibrary library, int revitRelease, string packageCode, string versionName);
        InstallResult Install(int revitRelease, System.IO.Stream packageData);
        InstallResult Install(int revitRelease, string fileName);
        InstallResult Uninstall(int release, string packageCode);
        System.Collections.Generic.IList<InstallResult> UninstallAll();
        System.Collections.Generic.IList<InstallResult> UninstallAll(int revitRelease);
        UpdateResult Update(IPackageLibrary library, int revitRelease, string packageCode);
        System.Collections.Generic.IList<UpdateResult> UpdateAll(IPackageLibrary library, int? revitRelease, System.Collections.Generic.IList<RequiredPackage> requiredPackages);
    }
}
