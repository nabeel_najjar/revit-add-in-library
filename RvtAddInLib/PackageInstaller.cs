﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RevitAddInLibrary
{


    /// <summary>
    /// PackageInstaller is used to install, uninstall, and update Revit add-in packages
    /// </summary>
    public class PackageInstaller : RevitAddInLibrary.IPackageInstaller
    {

        /// <summary>
        /// This is the file that the PackageInstaller reads and writes to and from to determine and record which packages are installed
        /// </summary>
        private string _installedPackagesFile;

        /// <summary>
        /// The root directory which package files are installed to
        /// </summary>
        /// <remarks>Sub directories will be created for each release and package code 
        /// 
        /// [RootInstallDirectory]\2013\som.activitylog, [RootInstallDirectory]\2012\som.viewowner
        /// </remarks>
        private string _appDataDirectory;


        /// <summary>
        /// Utility class to determine what versions of Revit are installed and if they are running
        /// </summary>
        private IRevitApplicationUtil _revitAppUtil;


        public PackageInstaller(IRevitApplicationUtil revitAppUtil)
        {
            //sets Revit App Util
            if (revitAppUtil == null) throw new ArgumentNullException("revitAppUtil");
            _revitAppUtil = revitAppUtil;

            //Guaranteed to be writable
            var roamingAppData = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            this._appDataDirectory = Path.Combine(roamingAppData, "Revit Add-In Library");
            this._installedPackagesFile = Path.Combine(_appDataDirectory, "InstalledPackages.xml");
        }

        public PackageInstaller():this(new RevitApplicationUtil())
        {
            
        }




        #region Install/Uninstall Methods



        /// <summary>
        /// Installs a package from a library
        /// </summary>
        /// <remarks>If an existing package is already installed with the same package code for the same release it will first be uninstalled before upgrading, downgrading, or reinstalling the provided package data </remarks>
        /// <param name="library">The library to install the package from</param>
        /// <param name="packageCode">The package code of the package to install</param>
        /// <param name="versionNumber">The version of the package to install</param>
        /// <param name="release">The release of Revit to install the package into</param>
        /// <returns>An InstallerResult which identifies the package that is installed currently and previously, and the type of installation of that occurred.
        /// The InstallerResultType may be Install, Reinstall, Upgrade, or Downgrade.
        /// </returns>
        /// <exception cref="ArgumentNullException">library is null</exception>
        /// <exception cref="ArgumentNullException">versionNumber is null</exception>
        /// <exception cref="ArgumentNullException">packageCode is null</exception>
        public InstallResult Install(IPackageLibrary library, int revitRelease, string packageCode, string versionNumber)
        {

            if (library == null) throw new ArgumentNullException("library");
            if (string.IsNullOrEmpty(packageCode)) throw new ArgumentNullException("packageCode");
            if (string.IsNullOrEmpty(versionNumber)) throw new ArgumentNullException("versionNumber");


            //string tempFile = string.Empty;
            Stream data = null;

            try
            {
                data = library.GetPackageData(packageCode, versionNumber);
                return Install(revitRelease, data);
            }
            finally
            {
                if (data != null)
                {
                    data.Dispose();
                }
            }
        }





        /// <summary>
        /// Installs a package from a file
        /// </summary>
        /// <remarks>If an existing package is already installed with the same package code for the same release it will first be uninstalled before upgrading, downgrading, or reinstalling the provided package data </remarks>
        /// <param name="fileName">Path to the package file</param>
        /// <returns>An InstallerResult which identifies the package that is installed currently and previously, and the type of installation of that occurred.
        /// The InstallerResultType may be Install, Reinstall, Upgrade, or Downgrade.
        /// </returns>
        public InstallResult Install(int revitRelease, string fileName)
        {
            using (System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read))
            {
                return Install(revitRelease, fs);
            }
        }


        /// <summary>
        /// Installs a package from a data stream
        /// </summary>
        /// <remarks>If an existing package is already installed with the same package code for the same release it will first be uninstalled before upgrading, downgrading, or reinstalling the provided package data </remarks>
        /// <param name="packageData">Stream of the package data</param>
        /// <returns>An InstallerResult which identifies the package that is installed currently and previously, and the type of installation of that occurred.
        /// The InstallerResultType may be Install, Reinstall, Upgrade, or Downgrade.
        /// </returns>
        /// <exception cref="InvalidOperationException">If release is less than Package.RevitRelease or  release is greater then Package.MaxRevitRelease</exception>
        public InstallResult Install(int revitRelease, System.IO.Stream packageData)
        {
            using (var packageMemStream = new MemoryStream())
            {
                packageData.CopyTo(packageMemStream);

                packageMemStream.Position = 0;
                var package = Package.ReadPackage(packageMemStream);


                //checks the specified release is greater then equal to MinRevitRelease
                if (revitRelease < package.MinRevitRelease)
                {
                    throw new InvalidOperationException("Can not install " + package.PackageCode + " version " + package.VersionName + " for Revit " + revitRelease.ToString() + ". Package.MinRevitRelease ==" + package.MinRevitRelease.ToString());
                }


                //checks the specified release is less than or equal to MaxRevitRelease
                if (package.MaxRevitRelease.HasValue && revitRelease > package.MaxRevitRelease.Value)
                {
                    throw new InvalidOperationException("Can not install " + package.PackageCode + " version " + package.VersionName + " for Revit " + revitRelease.ToString() + ". Package.MaxRevitRelease ==" + package.MaxRevitRelease.ToString());
                }

                //checks if existing package is installed for target release
                var existingPackage = GetInstalledPackage(package.PackageCode, revitRelease);


                if (existingPackage != null)
                {
                    Uninstall(revitRelease, package.PackageCode);
                }



                //Determines the new add-in manifest path
                var newAddInFilePath = GetAddInManifestFilePath(revitRelease, package.PackageCode);


                //checks if add-in file already exists
                
                if (System.IO.File.Exists(newAddInFilePath))
                {
                    //Delete any existing add-in file
                    System.IO.File.Delete(newAddInFilePath);
                }



                //Determines the the install directory
                string installDirectory = GetInstallDirectory(revitRelease, package.PackageCode);


                //Deletes any existing install directory
                if (System.IO.Directory.Exists(installDirectory))
                {
                    System.IO.Directory.Delete(installDirectory, true);
                }



                //new package is added to Installed packages file
                InstalledPackage installedPackage = new InstalledPackage(package, revitRelease, DateTime.Now.ToUniversalTime());
                AddInstalledPackage(installedPackage);

                try
                {
                    //Creates the install directory
                    System.IO.Directory.CreateDirectory(installDirectory);

                    //Extracts all files from package except package file and *.addin files
                    packageMemStream.Position = 0;

                    string addInManifestFileName = package.PackageCode + ".addin";


                    using (Ionic.Zip.ZipFile packageZip = Ionic.Zip.ZipFile.Read(packageMemStream))
                    {
                        foreach (var entry in packageZip)
                        {
                            //skip Package.xml file
                            if (entry.FileName == Package.PACKAGE_FILE_NAME) continue;

                            //skip add-in manifest file
                            if (entry.FileName == addInManifestFileName) continue;

                            entry.Extract(installDirectory);
                        }





                        string tempManifestPath = Path.Combine(System.IO.Path.GetTempPath(), addInManifestFileName);

                        try
                        {
                            //extracts add-in manifest file to addin folder
                            
                            //extracts addin manifest to temp file

                            packageZip[addInManifestFileName].Extract(System.IO.Path.GetTempPath());

                            
                            

                            
                            //opens temp addin manifest
                            var addInManifest = Autodesk.RevitAddIns.AddInManifestUtility.GetRevitAddInManifest(tempManifestPath);



                            foreach (var app in addInManifest.AddInApplications)
                            {
                                app.Assembly = Path.Combine(installDirectory, app.Assembly);
                            }

                            foreach (var dbApp in addInManifest.AddInDBApplications)
                            {
                                dbApp.Assembly = Path.Combine(installDirectory, dbApp.Assembly);
                            }

                            foreach (var cmd in addInManifest.AddInCommands)
                            {
                                cmd.Assembly = Path.Combine(installDirectory, cmd.Assembly);

                                if (!string.IsNullOrEmpty(cmd.LargeImage))
                                {
                                    cmd.LargeImage = Path.Combine(installDirectory, cmd.LargeImage);
                                }

                                if (!string.IsNullOrEmpty(cmd.TooltipImage))
                                {
                                    cmd.TooltipImage = Path.Combine(installDirectory, cmd.TooltipImage);
                                }
                            }

                            addInManifest.Save();

                            //creates the add-in directory if missing
                            var newAddinFileDir = Path.GetDirectoryName(newAddInFilePath);
                            if (!Directory.Exists(newAddinFileDir)) Directory.CreateDirectory(newAddinFileDir);

                            //Copies the temp manifest
                            File.Copy(tempManifestPath, newAddInFilePath);
                        }
                        finally
                        {
                            if (File.Exists(tempManifestPath)) File.Delete(tempManifestPath);
                        }
                    }

                    //returns result
                    if (existingPackage != null)
                    {
                        return new InstallResult(existingPackage.Package, package);
                    }
                    else
                    {
                        return new InstallResult(null, package);
                    }
                }
                catch (Exception)
                {
                    //if there is any problem installing the package uninstall it
                    Uninstall(revitRelease, package.PackageCode);

                    throw;
                }


            }
        }

        private string GetInstallDirectory(int revitRelease, string packageCode)
        {
            return System.IO.Path.Combine(_appDataDirectory, revitRelease.ToString(), packageCode);
        }



        /// <summary>
        /// Installs the latest published version of the specified package for the specified release.
        /// </summary>
        /// <param name="lib">The library to install from</param>
        /// <param name="packageCode">The package code of the package to install</param>
        /// <param name="release">The target Release of Revit</param>
        /// <exception cref="InvalidOperationException">No versions are published for the specified release</exception>
        /// <exception cref="ArgumentNullException">library or packageCode is null or empty</exception>
        public InstallResult Install(IPackageLibrary library, int revitRelease, string packageCode)
        {
            if (library == null) throw new ArgumentNullException("library");
            if (String.IsNullOrEmpty(packageCode)) throw new ArgumentNullException("packageCode");

            var installedVersion = GetInstalledPackage(packageCode, revitRelease);

            if (installedVersion != null)
            {
                throw new NotImplementedException();
                //return new InstallResult(installedVersion.Package, installedVersion.Package, InstallerResultType.AlreadyInstalled);
            }
            else
            {
                //package is not installed
                var latestPublishedVersion = library.GetLatestPublishedVersion(packageCode, revitRelease);

                if (latestPublishedVersion != null)
                {
                    return Install(library, revitRelease, packageCode, latestPublishedVersion.Package.VersionName);
                }
                else
                {
                    throw new InvalidOperationException("No published versions of " + packageCode + " for Revit " + revitRelease.ToString());
                }
            }
        }



        /// <summary>
        /// Uninstalls any installed package with the specified package code from the specified release of Revit.
        /// </summary>
        /// <param name="release">The release of Revit to uninstall the package from</param>
        /// <param name="packageCode">The package code of the package to uninstall</param>
        /// <returns>An InstallerResult. If previously installed the InstallerResult.Previous will identify the version that was installed and the ResultType will be Uninstall. If the package
        /// was not installed then the ResultType will be NoChange and both Previous and Current will equal null.</returns>
        /// <exception cref="InvalidOperationException">No package is installed with the specified package code, for the specified release</exception>
        public InstallResult Uninstall(int release, string packageCode)
        {

            //this should only return one packages but this code in included to ensure that 
            var installedPackage = GetInstalledPackage(packageCode, release);

            if (installedPackage != null)
            {


                //Prevent uninstalling package while Revit is runnning
                if (_revitAppUtil != null && _revitAppUtil.IsRevitRunning(release))
                {
                    throw new InvalidOperationException("Can not uninstall " + installedPackage.Package.PackageName + " " + installedPackage.Package.VersionName + ". Revit " + release.ToString() + " is running.");
                }

                //deletes add-in manifest
                var addInManifestFilePath = GetAddInManifestFilePath(release, installedPackage.Package.PackageCode);
                if (System.IO.File.Exists(addInManifestFilePath)) System.IO.File.Delete(addInManifestFilePath);


                string installDirectory = GetInstallDirectory(release, packageCode);


                //deletes install directory
                if (System.IO.Directory.Exists(installDirectory))
                {
                    System.IO.Directory.Delete(installDirectory, true);
                }

                //Removes the package from the installed packages file
                RemoveInstalledPackage(packageCode, release);

                return new InstallResult(installedPackage.Package, null);
            }
            else
            {
                throw new InvalidOperationException("Can not uninstall! No version of " + packageCode + " is installed for Revit " + release.ToString());
            }
        }


        public IList<InstallResult> UninstallAll()
        {
            List<InstallResult> results = new List<InstallResult>();

            IList<InstalledPackage> installed = GetInstalledPackages();

            foreach (var i in installed)
            {
                var r = Uninstall(i.RevitRelease, i.Package.PackageCode);
                results.Add(r);
            }

            return results;
        }

        public IList<InstallResult> UninstallAll(int revitRelease)
        {
            List<InstallResult> results = new List<InstallResult>();

            IList<InstalledPackage> installed = GetInstalledPackages().Where(p => p.RevitRelease == revitRelease).ToList();

            foreach (var i in installed)
            {
                var r = Uninstall(revitRelease, i.Package.PackageCode);
                results.Add(r);
            }

            return results;
        }

        #endregion




        #region Update Methods

        /// <summary>
        /// Updates an existing installed package to the latest version, or downgrades if the installed version has been suspended
        /// </summary>
        /// <remarks></remarks>
        /// <param name="library">The library to update from</param>
        /// <param name="revitRelease">The release of Revit to target</param>
        /// <param name="packageCode">Package code of the package to update</param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">When an existing version of the specified package is not installed for the specified release of Revit</exception>
        public UpdateResult Update(IPackageLibrary library, int revitRelease, string packageCode)
        {
            if (library == null) throw new ArgumentNullException("library");
            if (String.IsNullOrEmpty(packageCode)) throw new ArgumentNullException("packageCode");

            var installed = GetInstalledPackage(packageCode, revitRelease);

            if (installed == null)
            {
                throw new InvalidOperationException("Package " + packageCode + " is not installed for Revit " + revitRelease.ToString());
            }
            else
            {
                //Package is installed
                //updating an existing package


                LibraryPackage installedLibPackage = library.GetPackage(packageCode, installed.Package.VersionName);
                LibraryPackage officalVersion = library.GetLatestPublishedVersion(packageCode, revitRelease);


                if (officalVersion != null)
                {
                    //latest published version found in library

                    if (officalVersion.Status != VersionStatus.Published) throw new InvalidOperationException("GetLatestPublishedVersion() returned a non-published package");

                    //installed version not found in library
                    if (installed.Package.VersionNumber < officalVersion.Package.VersionNumber)
                    {
                        //published library version is newer then installed version
                        //upgrade to this version
                        Install(library, revitRelease, packageCode, officalVersion.Package.VersionName);
                        return new UpdateResult(installed.Package, officalVersion.Package, UpdateResultType.Upgrade);

                    }
                    else if (installed.Package.VersionNumber > officalVersion.Package.VersionNumber)
                    {
                        //install version is newer then published version

                        if (installedLibPackage != null && installedLibPackage.Status == VersionStatus.Suspended)
                        {
                            //Installed version is suspended, downgrade to latest published version

                            var res = Install(library, revitRelease, packageCode, officalVersion.Package.VersionName);
                            return new UpdateResult(res.Previous, res.Current, UpdateResultType.DowngradeFromSuspended);

                        }
                        else
                        {
                            //User has a newer version than published version installed, which is not in the library
                            //Leave this newer unofficial version installed
                            return new UpdateResult(installed.Package, installed.Package, UpdateResultType.NewerThanOfficalInstalled);
                        }
                    }
                    else
                    {
                        //user has the official version installed
                        return new UpdateResult(installed.Package, installed.Package, UpdateResultType.OfficalInstalled);
                    }

                }
                else
                {
                    //Latest Published package not found

                    if (installedLibPackage != null && installedLibPackage.Status == VersionStatus.Suspended)
                    {
                        //Uninstall if installed package is suspended
                        var res = Uninstall(revitRelease, packageCode);

                        return new UpdateResult(res.Previous, res.Current, UpdateResultType.UninstallSuspended);
                    }

                    //Can't determine a latest published version, so no upgrade/downgrade can be determined

                    return new UpdateResult(installed.Package, installed.Package, UpdateResultType.UnknownOfficalVersion);
                }
            }
        }









        /// <summary>
        /// Updates installed packages, and potentially installs missing required packages
        /// </summary>
        /// <remarks></remarks>
        /// <param name="library">The library to update and install packages from.</param>
        /// <param name="revitRelease">The target release of Revit, all installed release of Revit will be updated if this value is null.</param>
        /// <param name="requiredPackages">The required packages that should be installed if missing. Only installed packages will be updated if this value is empty or null.</param>
        public IList<UpdateResult> UpdateAll(IPackageLibrary library, int? revitRelease, IList<RequiredPackage> requiredPackages)
        {
            if (library == null) throw new ArgumentNullException("library");

            List<UpdateResult> results = new List<UpdateResult>();

            var allInstalled = GetInstalledPackages();

            
            IList<InstalledPackage> installedToUpdate = null;

            //Gets installed releases
            var installedReleases = _revitAppUtil.GetInstalledRevitReleases();

            if (revitRelease.HasValue)
            {
                if (!installedReleases.Contains(revitRelease.Value))
                {
                    throw new InvalidOperationException("Can not run UpdateAll for Revit " + revitRelease + ". This release is not installed.");
                }

                //Updates packages installed for the specified release of Revit
                installedToUpdate = allInstalled.Where(p => p.RevitRelease == revitRelease.Value).ToList();
            }
            else
            {
                //only updates the packages for installed releases
                installedToUpdate = allInstalled.Where(p => installedReleases.Contains(p.RevitRelease)).ToList();
            }


            //Updates installed packages
            foreach (var ins in installedToUpdate)
            {
                var r = Update(library, ins.RevitRelease, ins.Package.PackageCode);
                results.Add(r);
            }

            //checks if required packages were specified
            if (requiredPackages != null)
            {
                List<RequiredPackage> reqToInstall = null;

                //checks if a release was specified and if so filter the requires list
                if (revitRelease.HasValue)
                {
                    //Only installs packages from specified installed release
                    reqToInstall = requiredPackages.Where(p => p.RevitRelease == revitRelease.Value).ToList();
                }
                else
                {
                    //Selects packages from all installed releases
                    reqToInstall = requiredPackages.Where(p => installedReleases.Contains(p.RevitRelease)).ToList();
                }

                //installs missing required packages
                foreach (var reqP in reqToInstall)
                {
                    var curIns = installedToUpdate.Where(i => i.Package.PackageCode == reqP.PackageCode & i.RevitRelease == reqP.RevitRelease).FirstOrDefault();

                    if (curIns == null)
                    {
                        var insRes = Install(library, reqP.RevitRelease, reqP.PackageCode);

                        results.Add(new UpdateResult(null, insRes.Current, UpdateResultType.InstalledRequired));
                    }
                }
            }

            //returns the list of results
            return results;

        }



        #endregion

        #region Installed Package File I/O Methods


        /// <summary>
        /// Provides the installed package information for the provided package code and Revit release
        /// </summary>
        /// <param name="packageCode">The package code. Example: com.acme.customtool</param>
        /// <param name="revitRelease">The target release of revit (2013, 2014, 2015 etc...)</param>
        /// <returns>Returns the installed package that matches the provided package code and release. Returns null if the specified package is not installed for the release of Revit.</returns>
        public InstalledPackage GetInstalledPackage(string packageCode, int revitRelease)
        {
            return GetInstalledPackages().Where(p => p.Package.PackageCode == packageCode && p.RevitRelease == revitRelease).FirstOrDefault();
        }


        /// <summary>
        /// This method is used to determine the currently installed packages.
        /// </summary>
        /// <returns>A list of all the currently installed packages</returns>
        public IList<InstalledPackage> GetInstalledPackages()
        {
            if (!System.IO.File.Exists(_installedPackagesFile)) return new List<InstalledPackage>();

            List<InstalledPackage> packages = null;

            using (System.IO.FileStream fs = new System.IO.FileStream(_installedPackagesFile, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.None))
            {
                System.Runtime.Serialization.DataContractSerializer ser = GetInstalledPackagesSerializer();
                packages = (List<InstalledPackage>)ser.ReadObject(fs);
            }

            return packages;
        }


        /// <summary>
        /// Removes any packages from the installed packages file with the provided packageCode and release
        /// </summary>
        /// <param name="packageCode"></param>
        /// <param name="release"></param>
        private void RemoveInstalledPackage(string packageCode, int release)
        {
            if (!System.IO.File.Exists(_installedPackagesFile)) return;

            using (System.IO.FileStream fs = new System.IO.FileStream(_installedPackagesFile, System.IO.FileMode.Open, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None))
            {

                List<InstalledPackage> packages = null;

                System.Runtime.Serialization.DataContractSerializer ser = GetInstalledPackagesSerializer();
                packages = (List<InstalledPackage>)ser.ReadObject(fs);

                var packageToRemove = packages.Where(p => p.Package.PackageCode == packageCode && p.RevitRelease == release).ToList();

                foreach (var remPkg in packageToRemove)
                {
                    packages.Remove(remPkg);
                }

                fs.SetLength(0);

                ser.WriteObject(fs, packages);
            }
        }


        /// <summary>
        /// Adds a new installed package record to the installed package file
        /// </summary>
        /// <remarks>If the file or directory does not exist then it will be created by this method</remarks>
        private void AddInstalledPackage(InstalledPackage installedPackage)
        {
            if (!File.Exists(_installedPackagesFile))
            {

                if (!Directory.Exists(_appDataDirectory))
                {
                    Directory.CreateDirectory(_appDataDirectory);
                }

                using (System.IO.FileStream fs = new System.IO.FileStream(_installedPackagesFile, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write, System.IO.FileShare.None))
                {
                    System.Runtime.Serialization.DataContractSerializer ser = GetInstalledPackagesSerializer();
                    var list = new List<InstalledPackage>();

                    list.Add(installedPackage);
                    ser.WriteObject(fs, list);
                }
            }
            else
            {
                using (System.IO.FileStream fs = new System.IO.FileStream(_installedPackagesFile, System.IO.FileMode.Open, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None))
                {

                    //Reads the existing packages
                    List<InstalledPackage> packages = null;
                    System.Runtime.Serialization.DataContractSerializer ser = GetInstalledPackagesSerializer();
                    packages = (List<InstalledPackage>)ser.ReadObject(fs);

                    //writes the file
                    packages.Add(installedPackage);
                    fs.SetLength(0);
                    ser.WriteObject(fs, packages);
                }
            }
        }

        /// <summary>
        /// Generates a DataContract serialized for reading and writing Installed Packages files
        /// </summary>
        private DataContractSerializer GetInstalledPackagesSerializer()
        {
            return new System.Runtime.Serialization.DataContractSerializer(typeof(List<InstalledPackage>), "InstalledPackages", string.Empty);
        }

        #endregion


        #region File Path Utility Methods

        /// <summary>
        /// Returns the folder to store revit add-in manifest files for all users for the specified release
        /// </summary>
        private static string GetAddInManifestFolder(int release)
        {
            var appData = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);


            string[] pathParts = new string[] { appData, "Autodesk", "Revit", "Addins", release.ToString() };

            var path = System.IO.Path.Combine(pathParts);

            return path;
        }


        /// <summary>
        /// Returns the absolute path to the revit add-in manifest file for the provided package
        /// </summary>
        private static string GetAddInManifestFilePath(int revitRelease, string packageCode)
        {
            return System.IO.Path.Combine(GetAddInManifestFolder(revitRelease), packageCode + ".addin");
        }

        #endregion











    }
}
