﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.IO;
using NLog;

namespace RevitAddInLibrary
{

    /// <summary>
    /// The Package class represents a single installable Revit add in package, which is uniquely identified by the VersionName and PackageCode properties. In addition 
    /// the static BuildingPackage, and ReadPackage methods can be used to create and read add-in package files.
    /// </summary>
    /// <remarks>This class is immutable</remarks>
    [DataContract(Namespace = "")]
    public sealed class Package
    {
        internal readonly static string PACKAGE_FILE_NAME = "Package.xml";
        internal readonly static string PACKAGE_EXTENSION = ".rpk";



        public override string ToString()
        {
            return PackageCode + " (" + VersionName + ")";
        }



        /// <summary>
        /// Creates a new Package instance
        /// </summary>
        /// <param name="packageCode">Sets the PackageCode property</param>
        /// <param name="packageName">Sets the PackageName property</param>
        /// <param name="packageNotes">Sets the PackageNotes property</param>
        /// <param name="versionName">Sets the VersionNumber property</param>
        /// <param name="versionNumber">Sets the VersionCode property</param>
        /// <param name="versionNotes">Sets the VersionNotes property</param>
        /// <param name="minRevitRelease">Sets the RevitRelease property</param>
        /// <param name="customInstallDirectory">Sets the CustomInstallDirectory property</param>
        /// <param name="author">Sets the Author property</param>
        /// <param name="email">Sets the Email property</param>
        public Package(string packageCode, string packageName, string packageNotes, string versionName, int versionNumber, string versionNotes, int minRevitRelease, int? maxRevitRelease, string author, string email, Uri link)
        {
            if (string.IsNullOrEmpty(packageCode)) throw new ArgumentNullException("packageCode");

            if (!System.Text.RegularExpressions.Regex.IsMatch(packageCode, @"^[a-z\d\.-]*$")) throw new ArgumentException("Invalid package code. Package codes may only contain lower case letters, numbers, periods, and hyphens.", "packageCode");
            this.PackageCode = packageCode;


            if (string.IsNullOrEmpty(packageName) || string.IsNullOrWhiteSpace(packageName)) throw new ArgumentNullException("packageName can not be null, empty, or only contain whitespace", "packageName");
            if (packageName.Contains("\n") | packageName.Contains("\r")) throw new ArgumentException("Package name must not contain new line characters", "packageName");
            this.PackageName = packageName;

            if (string.IsNullOrEmpty(versionName) || string.IsNullOrWhiteSpace(versionName))
            {
                throw new ArgumentNullException("versionNumber can not be empty, null, or only whitespace", "versionNumber");
            }
            else if (versionName != versionName.Trim())
            {
                throw new ArgumentException("Version number can not start or end with whitespace", "versionNumber");
            }
            else if (versionName.Contains("\n") | versionName.Contains("\r"))
            {
                throw new ArgumentException("Version number can not include line breaks", "versionNumber");
            }

            this.VersionName = versionName;


            if (versionNumber < 0) throw new ArgumentException("versionCode can not be negative", "versionCode");
            this.VersionNumber = versionNumber;

            if (minRevitRelease < 0) throw new ArgumentException("revitRelease can not be negative", "revitRelease");
            this.MinRevitRelease = minRevitRelease;


            if (maxRevitRelease.HasValue && maxRevitRelease < minRevitRelease) throw new ArgumentException("maxRevitRelease must be greater then revitRelease", "maxRevitRelease");
            this.MaxRevitRelease = maxRevitRelease;


            this.Author = author;
            this.Email = email;
            this.Link = link;
            this.VersionNotes = versionNotes;
            this.PackageNotes = packageNotes;
        }


        /// <summary>
        /// A human readable name for the package. Example "Activity Log"
        /// </summary>
        [DataMember(Name = "PackageName", IsRequired = true, Order = 1)]
        private string _packageName;
        public string PackageName
        {
            get
            {
                return _packageName;

            }

            set
            {

                if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value)) throw new InvalidOperationException("packageName can not be null, empty, or only contain whitespace");
                if (value.Contains("\n") | value.Contains("\r")) throw new InvalidOperationException("Package name must not contain new line characters");

                this._packageName = value;


            }

        }


        /// <summary>
        /// A string which identifies a package. This is recommended to be lowercase and in the format [Company].[ApplicationName]. For example:
        /// som.activitylog, ideate.bimlink, or case-inc.parametertool.
        /// </summary>
        /// <remarks>The PackageInstaller ensures that only one version of each package is installed for each release of Revit.</remarks>
        [DataMember(IsRequired = true, Order = 2)]
        public string PackageCode { get; private set; }

        /// <summary>
        ///Short description of the Revit add-in and its functionality.
        /// </summary>
        [DataMember(IsRequired = false, Order = 3)]
        public string PackageNotes { get; set; }




        /// <summary>
        /// Author of the package
        /// </summary>
        [DataMember(IsRequired = false, Order = 4)]
        public string Author { get; set; }

        /// <summary>
        /// Email address
        /// </summary>
        [DataMember(IsRequired = false, Order = 5)]
        public string Email { get; set; }


        /// <summary>
        /// Hyper-link to more information
        /// </summary>
        [DataMember(IsRequired = false, Order = 6)]
        public Uri Link { get; set; }



        /// <summary>
        /// Release of Revit the package supports (2013, 2014, 2015 etc..)
        /// </summary>
        [DataMember(Name = "MinRevitRelease", IsRequired = true, Order = 7)]
        int _minRevitRelease;
        public int MinRevitRelease
        {
            get
            {
                return _minRevitRelease;
            }

            set
            {
                if (_minRevitRelease != value)
                {
                    if (value < 0) throw new InvalidOperationException("MinRevitRelease must be greater than 0");

                    if (MaxRevitRelease.HasValue && value > MaxRevitRelease.Value) throw new InvalidOperationException("MinRevitRelease must be less than or equal to MaxRevitRelease");
                    _minRevitRelease = value;
                }


            }


        }



        /// <summary>
        /// The 
        /// </summary>
        [DataMember(Name = "MaxRevitRelease", IsRequired = false, Order = 8)]
        private int? _maxRevitRelease;
        public int? MaxRevitRelease
        {
            get
            {
                return _maxRevitRelease;
            }
            set
            {

                if (value.HasValue)
                {
                    if (value >= MinRevitRelease)
                    {
                        _maxRevitRelease = value.Value;
                    }
                    else
                    {
                        throw new InvalidOperationException("MaxRevitRelease must be greater than or equal to RevitRelease");
                    }
                }
                else
                {
                    _maxRevitRelease = null;
                }

            }
        }


        
        public string SupportedRevitReleases
        {
            get
            {
                if (MaxRevitRelease.HasValue)
                {
                    return MinRevitRelease.ToString() + "-" + MaxRevitRelease.ToString();
                }
                else
                {
                    return MinRevitRelease.ToString() + "+";
                }
            }
        }


        /// <summary>
        /// Human readable version number which is used to identify the version of the package to users.
        /// </summary>
        /// <remarks>This value must be unique for all packages with the same package code in a library</remarks>
        [DataMember(Name = "VersionName", IsRequired = true, Order = 9)]
        public string VersionName { get; private set; }

        /// <summary>
        /// Integer code used to compare two versions. This value is used by the PackageInstaller to compare two package versions.
        /// </summary>
        /// <remarks>This value must be unique for all packages with the same package code in a library</remarks>
        [DataMember(Name = "VersionNumber", IsRequired = true, Order = 10)]
        public int VersionNumber { get; private set; }

        /// <summary>
        /// Notes on changes and updates made in the current version of the package.
        /// </summary>
        [DataMember(IsRequired = false, Order = 11)]
        public string VersionNotes { get; set; }






        /// <summary>
        /// Extracts the package meta data from a package file
        /// </summary>
        /// <param name="packageFilePath">The file path to the package file</param>
        public static Package ReadPackage(string packageFilePath)
        {
            using (var fs = new System.IO.FileStream(packageFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read))
            {
                return ReadPackage(fs);
            }
        }


        /// <summary>
        /// Extracts the package meta data from a package data stream
        /// </summary>
        /// <param name="packageData">The package data stream</param>
        public static Package ReadPackage(System.IO.Stream packageData)
        {
            Ionic.Zip.ZipFile zipFile = Ionic.Zip.ZipFile.Read(packageData);

            var packageEntry = zipFile[PACKAGE_FILE_NAME];


            Package readPackage = null;


            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {

                packageEntry.Extract(ms);

                DataContractSerializer ser = GetPackageXmlSerializer();

                ms.Position = 0;

                readPackage = (Package)ser.ReadObject(ms);
            }

            return readPackage;
        }


        private static DataContractSerializer GetPackageXmlSerializer()
        {
            return new DataContractSerializer(typeof(Package));

        }



        /// <summary>
        /// Builds and validates a package directly from the files in the sourceFolder
        /// </summary>
        /// <remarks>
        /// This folder must contain a valid Package.xml file, and a corresponding adding manifest named with the package code specified in Package.xml
        /// </remarks>
        /// <param name="destinationStream">The stream to write the package data to</param>
        /// <param name="sourceFolder">Directory which contains the source files</param>
        public static void BuildPackage(System.IO.Stream destinationStream, string sourceFolder)
        {

            if (destinationStream == null)
            {
                throw new ArgumentNullException("destinationStream", "destinationStream can not be null");
            }


            //check that source folder Exists
            if (!System.IO.Directory.Exists(sourceFolder))
            {
                throw new DirectoryNotFoundException("Source folder does not exist: " + sourceFolder);
            }

            var packageXMLPath = Path.Combine(sourceFolder, PACKAGE_FILE_NAME);

            //check that Package.xml does not already exist
            if (!System.IO.File.Exists(packageXMLPath))
            {
                throw new FileNotFoundException("Could not find " + PACKAGE_FILE_NAME, packageXMLPath);
            }

            Package package;

            using (var fs = new FileStream(packageXMLPath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var ser = GetPackageXmlSerializer();
                package = (Package)ser.ReadObject(fs);
            }

            string addInManifestFileName = package.PackageCode + ".addin";

            string addInManifestpath = Path.Combine(sourceFolder, addInManifestFileName);

            //check that addin.xml does not already exist
            if (!System.IO.File.Exists(addInManifestpath))
            {
                throw new FileNotFoundException("Could not find Revit add-in manifest" + addInManifestFileName, addInManifestpath);
            }


            //Check that all paths referenced in add-in file are relative and that soureFolder contains matching files 
            var addInManifest = Autodesk.RevitAddIns.AddInManifestUtility.GetRevitAddInManifest(addInManifestpath);

            List<string> manifestFilePaths = new List<string>();

            //collects all file references
            foreach (var app in addInManifest.AddInApplications)
            {
                manifestFilePaths.Add(app.Assembly);
            }

            foreach (var dbApp in addInManifest.AddInDBApplications)
            {
                manifestFilePaths.Add(dbApp.Assembly);
            }

            foreach (var cmd in addInManifest.AddInCommands)
            {
                manifestFilePaths.Add(cmd.Assembly);

                if (!String.IsNullOrEmpty(cmd.LargeImage))
                {
                    manifestFilePaths.Add(cmd.LargeImage);
                }

                if (!String.IsNullOrEmpty(cmd.TooltipImage))
                {
                    manifestFilePaths.Add(cmd.TooltipImage);
                }
            }

            //checks that all file references are relative paths and reference files that exist
            foreach (var manifestFilePath in manifestFilePaths)
            {
                if (Path.IsPathRooted(manifestFilePath)) throw new ArgumentException("Add-in manifest file" + addInManifestFileName + " refers to a rooted path. File references must be relative.");

                string fileLoc = Path.Combine(sourceFolder, manifestFilePath);

                if (!File.Exists(fileLoc)) throw new FileNotFoundException("Add-in manifest file" + addInManifestFileName + " refers to a file that does not exists.", fileLoc);
            }



            //create package zip
            using (Ionic.Zip.ZipFile zipFile = new Ionic.Zip.ZipFile())
            {
                //Saves files to zip file
                zipFile.AddItem(sourceFolder);

                //sets file attributes to normal
                //this prevents files from being read-only and thus not able to be deleted later but uninstall
                foreach (var e in zipFile.Entries)
                {
                    e.Attributes = FileAttributes.Normal;
                }

                //Writes zip to stream
                zipFile.Save(destinationStream);
            }
        }





        /// <summary>
        /// Updates the metadata of the package
        /// </summary>
        /// <remarks>This only includes the the package metadata. This method does not affect the files in the package. 
        /// Properties that identify the package can not be changed including the PackageCode, VersionCode, and VersionNumber</remarks>
        /// <param name="packageData"></param>
        /// <param name="updatedPackage"></param>
        public static void UpdatePackage(System.IO.Stream packageData, Package updatedPackage)
        {
            //creates a memory stream to read the package data
            using (System.IO.MemoryStream packageDataMS = new System.IO.MemoryStream())
            {
                //copy the package data to memory stream
                packageData.CopyTo(packageDataMS);
                packageDataMS.Position = 0;


                //reads package file into ms
                using (Ionic.Zip.ZipFile zipFile = Ionic.Zip.ZipFile.Read(packageDataMS))
                {
                   
                    DataContractSerializer ser = GetPackageXmlSerializer();
                    Package package;

                    //reads existing package
                    using (System.IO.MemoryStream packageManifestMS = new MemoryStream())
                    {
                        var packageEntry = zipFile[PACKAGE_FILE_NAME];
                        packageEntry.Extract(packageManifestMS);
                        packageManifestMS.Position = 0;
                        package = (Package)ser.ReadObject(packageManifestMS);
                    }
                    

                    //reads the old package from the zip file
                    
                    

                    //checks that identifying properties are not different
                    if (package.VersionNumber != updatedPackage.VersionNumber) throw new ArgumentException("package.VersionNumber does not match existing package", "package");
                    if (package.VersionName != updatedPackage.VersionName) throw new ArgumentException("package.VersionNumber does not match existing package", "package");
                    if (package.PackageCode != updatedPackage.PackageCode) throw new ArgumentException("package.VersionNumber does not match existing package", "package");


                    //updates package data
                    package.PackageName = updatedPackage.PackageName;
                    package.Author = updatedPackage.Author;
                    package.Email = updatedPackage.Email;
                    package.Link = updatedPackage.Link;
                    package.PackageNotes = updatedPackage.PackageNotes;
                    package.VersionNotes = updatedPackage.VersionNotes;


                    //need to set this to null first in case the new minrelease is greater then the old max
                    package.MaxRevitRelease = null;
                    package.MinRevitRelease = updatedPackage.MinRevitRelease;
                    package.MaxRevitRelease = updatedPackage.MaxRevitRelease;



                    using (System.IO.MemoryStream newPackageManifestData = new MemoryStream())
                    {
                        ser.WriteObject(newPackageManifestData, package);
                        newPackageManifestData.Position = 0;

                        //clears the existing stream
                        packageData.SetLength(0);



                        zipFile.RemoveEntry(PACKAGE_FILE_NAME);
                        zipFile.AddEntry(PACKAGE_FILE_NAME, newPackageManifestData);

                        //saves the new package
                        zipFile.Save(packageData);
                    }
                }





            }



        }


        /// <summary>
        /// Generates a package file from files and directories located at "sourceFolder" with the package details of the 'package' parameter.
        /// </summary>
        /// <remarks>
        /// 'sourceFolder' must contain a revit add-in manifest that matches the package code in 'package'
        /// All file paths in add-in manifest file to assemblies or image file must be relative to sourceFolder
        /// 'sourveFolder' must not contain a Package.xml file
        /// </remarks>
        /// <param name="destinationStream">The stream to write to package file data to</param>
        /// <param name="sourceFolder">The source folder to read the revit add-in files from. This folder must contain a Revit add-in manifest file with the same name as package.PackageCode.</param>
        public static void BuildPackage(System.IO.Stream destinationStream, string sourceFolder, Package package)
        {
            if (destinationStream == null)
            {
                throw new ArgumentNullException("destinationStream", "destinationStream can not be null");
            }

            if (package == null)
            {
                throw new ArgumentNullException("package", "package can not be null");
            }

            //check that source folder Exists
            if (!System.IO.Directory.Exists(sourceFolder))
            {
                throw new DirectoryNotFoundException("Source folder does not exist: " + sourceFolder);
            }


            //check that sourceFolder folder does not already contain Package.xml file
            if (System.IO.File.Exists(System.IO.Path.Combine(sourceFolder, PACKAGE_FILE_NAME))) throw new ArgumentException("Source folder contains " + PACKAGE_FILE_NAME, "sourceFolder");


            //check packagecode.addin file exists
            string addInManifestFileName = package.PackageCode + ".addin";
            string addInManifestPath = Path.Combine(sourceFolder, addInManifestFileName);
            if (!File.Exists(addInManifestPath))
            {
                throw new FileNotFoundException("Source folder does not contain a matching package manifest file: " + addInManifestFileName, addInManifestPath);
            }

            //Check that all paths referenced in add-in file are relative and that soureFolder contains matching files 
            var addInManifest = Autodesk.RevitAddIns.AddInManifestUtility.GetRevitAddInManifest(addInManifestPath);

            List<string> manifestFilePaths = new List<string>();

            //collects all file references
            foreach (var app in addInManifest.AddInApplications)
            {
                manifestFilePaths.Add(app.Assembly);
            }

            foreach (var dbApp in addInManifest.AddInDBApplications)
            {
                manifestFilePaths.Add(dbApp.Assembly);
            }

            foreach (var cmd in addInManifest.AddInCommands)
            {
                manifestFilePaths.Add(cmd.Assembly);

                if (!String.IsNullOrEmpty(cmd.LargeImage))
                {
                    manifestFilePaths.Add(cmd.LargeImage);
                }

                if (!String.IsNullOrEmpty(cmd.TooltipImage))
                {
                    manifestFilePaths.Add(cmd.TooltipImage);
                }
            }

            //checks that all file references are relative paths and reference files that exist
            foreach (var manifestFilePath in manifestFilePaths)
            {
                if (Path.IsPathRooted(manifestFilePath)) throw new ArgumentException("Add-in manifest file" + addInManifestFileName + " refers to a rooted path. File references must be relative.");

                string fileLoc = Path.Combine(sourceFolder, manifestFilePath);

                if (!File.Exists(fileLoc)) throw new FileNotFoundException("Add-in manifest file" + addInManifestFileName + " refers to a file that does not exists.", fileLoc);
            }



            using (Ionic.Zip.ZipFile zipFile = new Ionic.Zip.ZipFile())
            {
                //Saves files to zip file
                zipFile.AddItem(sourceFolder);

                //sets file attributes to normal
                //this prevents files from being read-only and thus not able to be deleted later but uninstall
                foreach (var e in zipFile.Entries)
                {
                    e.Attributes = FileAttributes.Normal;
                }

                //Writes Package.xml file to zip
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                {
                    DataContractSerializer ser = GetPackageXmlSerializer();
                    ser.WriteObject(ms, package);

                    ms.Position = 0;
                    zipFile.AddEntry(PACKAGE_FILE_NAME, ms);
                    zipFile.Save(destinationStream);
                }


            }
        }
    }





}



