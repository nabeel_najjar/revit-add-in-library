﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RevitAddInLibrary
{
    /// <summary>
    /// A required package identifies a specific packages that must be installed for users
    /// </summary>
    ///<remarks>A list of RequiredPackage instances can be provided to the PackageInstaller.UpdateAll() method. This will cause the installed</remarks>
    public class RequiredPackage
    {
        public string PackageCode { get; private set; }
        public int RevitRelease { get; private set; }


        public RequiredPackage(int release, string packageCode)
        {
            if (string.IsNullOrEmpty(packageCode)) throw new ArgumentNullException("packageCode");
            if (release < 0) throw new ArgumentException("Release must be greater then zero", "release");

            this.RevitRelease = release;
            this.PackageCode = packageCode;
        }


        public static IList<RequiredPackage> ReadRequiredPackageList(string filePath)
        {


            IList<RequiredPackage> list;

            using (var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                list = ReadRequiredPackageList(fs);
            }

            return list;

        }

        public static IList<RequiredPackage> ReadRequiredPackageList(System.IO.Stream fileStream)
        {


            List<RequiredPackage> list = new List<RequiredPackage>();

            using (StreamReader sr = new StreamReader(fileStream))
            {
                while (!sr.EndOfStream)
                {

                    var line = sr.ReadLine();

                    var match = System.Text.RegularExpressions.Regex.Match(line, @"^(\d+)\s([a-z\.\d-]+)$");

                    if (match.Success)
                    {
                        int release = System.Convert.ToInt32(match.Groups[1].Value);
                        string packageCode = match.Groups[2].Value;

                        list.Add(new RequiredPackage(release, packageCode));
                    }
                }
            }

            return list;

        }

        public static void WriteRequiredPackageList(string filePath, IEnumerable<RequiredPackage> reqPackages)
        {



            using (var fs = new FileStream(filePath, FileMode.Create, FileAccess.Write))
            {
                WriteRequiredPackageList(fs, reqPackages);
            }

        }



        public static void WriteRequiredPackageList(System.IO.Stream fileStream, IEnumerable<RequiredPackage> reqPackages)
        {

            using (StreamWriter sw = new StreamWriter(fileStream))
            {
                foreach (var reqP in reqPackages)
                {
                    sw.WriteLine(reqP.RevitRelease.ToString() + " " + reqP.PackageCode);
                }
            }

        }
    }
}
