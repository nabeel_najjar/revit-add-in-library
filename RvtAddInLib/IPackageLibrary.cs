﻿using System;
using System.Collections.Generic;
using System.IO;

namespace RevitAddInLibrary
{

    /// <summary>
    /// Interface to a library of Revit add-in packages
    /// </summary>
    /// <seealso cref="LibraryPackage"/>
    public interface IPackageLibrary
    {

        /// <summary>
        /// String which identifies the location of the library
        /// </summary>
        string Location { get; }


        /// <summary>
        /// Adds a package to the library
        /// </summary>
        /// <remarks>IPackageLibrary implementations must prevent packages from being added which have duplicate Version Number or VersionCode values</remarks>
        /// <param name="packageData">Package data stream</param>
        void AddPackage(System.IO.Stream packageData);

        /// <summary>
        /// Removes a package from the library
        /// </summary>
        /// <param name="packageCode">The package code</param>
        /// <param name="versionNumber">The package version</param>
        void DeletePackage(string packageCode, string versionNumber);

        /// <summary>
        /// Return the data stream of the package file
        /// </summary>
        Stream GetPackageData(string packageCode, string versionNumber);

        /// <summary>
        /// Updates the
        /// </summary>
        void UpdatePackage(LibraryPackage updatedPackage);

        /// <summary>
        /// Returns all the package codes used by packages in the library.
        /// </summary>
        /// <returns></returns>
        IEnumerable<string> GetPackageCodes();

        /// <summary>
        /// Returns all packages in the library
        /// </summary>
        /// <returns></returns>
        IEnumerable<LibraryPackage> GetPackages();

        /// <summary>
        /// Returns all package versions with the provided package code
        /// </summary>
        IEnumerable<LibraryPackage> GetPackages(int? release, string packageCode);

        /// <summary>
        /// Returns the package from the library with the matching package code and version number
        /// </summary>
        /// <remarks>Returns null if the library does not contain a package with the provided values</remarks>
        LibraryPackage GetPackage(string packageCode, string versionNumber);

        /// <summary>
        /// Returns the latest published package for the specified packageCode and Revit release
        /// </summary>
        /// <remarks>Returns NULL is there are no published </remarks>
        /// <param name="packageCode"></param>
        /// <param name="revitRelease"></param>
        /// <returns></returns>
        LibraryPackage GetLatestPublishedVersion(string packageCode, int revitRelease);

    }
}
