﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RevitAddInLibrary
{

    /// <summary>
    /// LibraryPackage represents an instance of a Package in an IPackageLibrary
    /// </summary>
    /// <seealso cref="IPackageLibrary"/>
    [DataContract(Namespace = "")]
    public class LibraryPackage
    {
        /// <summary>
        /// The Package
        /// </summary>
        [DataMember(IsRequired=true,Order=1)]
        public Package Package { get; private set; }

        /// <summary>
        /// The date/time when the package was added to the library in UTC
        /// </summary>
        [DataMember(IsRequired = true, Order = 2)]
        public DateTime AddedOnUTC { get; private set; }



        /// <summary>
        /// The date/time when the package was added to the library in UTC
        /// </summary>
        [DataMember(IsRequired = true, Order = 3)]
        public string AddedBy { get; private set; }


        /// <summary>
        /// The size of the package data in bytes
        /// </summary>
        [DataMember(IsRequired = true, Order = 4)]
        public long FileSize { get; private set; }

        /// <summary>
        /// The status of the package in the library
        /// </summary>
        /// <seealso cref="VersionStatus"/>
        /// <remarks>
        /// This is the only value of a LibrayPackage which can be changed after a Package is added to the library. Changing the status of a package version 
        /// changes which users see the version in the installer tool, and which version the update process upgrades too.
        /// </remarks>
        [DataMember(IsRequired = true, Order = 5)]
        public VersionStatus Status { get; set; }


        public override string ToString()
        {
            return Package.ToString() + " [" + Status.ToString() + "]";
        }

        public LibraryPackage(Package package, VersionStatus status, string addedBy, DateTime addedOnUTC, long fileSize)
        {
            if (package == null) throw new ArgumentNullException("package");
            this.Package = package;

            if (String.IsNullOrEmpty(addedBy)) throw new ArgumentNullException("addedBy");
            this.AddedBy = addedBy;

            this.Status = status;

            FileSize = fileSize;

            if (addedOnUTC.Kind != DateTimeKind.Utc)
            {
                throw new ArgumentException("addedOnUTC.Kind must be UTC", "addedOnUTC");
            }
            this.AddedOnUTC = addedOnUTC;
        }
    }
}
