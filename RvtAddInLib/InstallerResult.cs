﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RevitAddInLibrary
{
    /// <summary>
    /// 
    /// </summary>
    public class PackageInstallerResult
    {
        public Package Previous { get; private set; }
        public Package Current { get; private set; }


        public PackageInstallerResult(Package previous, Package current)
        {
            if (previous != null && current != null)
            {
                if (previous.PackageCode != current.PackageCode) throw new ArgumentException("previous", "previous.PackageCode != current.PackageCode");
            
            }

            Previous = previous;
            Current = current;
        }
    }


    public class InstallResult : PackageInstallerResult
    {
        public InstallationType Type { get; private set; }

        public InstallResult(Package previous, Package current)
            : base(previous, current)
        {

            if (previous != null & current != null)
            {
                if (previous.VersionName == current.VersionName)
                {
                    Type = InstallationType.Reinstall;
                }
                else if (previous.VersionNumber > current.VersionNumber)
                {
                    Type = InstallationType.Downgrade;
                }
                else if (previous.VersionNumber < current.VersionNumber)
                {
                    Type = InstallationType.Upgrade;
                }
            }
            else if (previous == null & current != null)
            {
                Type = InstallationType.Install;
            }

            else if (previous != null & current == null)
            {
                Type = InstallationType.Uninstall;
            }
            else
            {
                throw new ArgumentNullException("Both previous and current can not be null", "current");
            }
        }
    }



    public class UpdateResult : PackageInstallerResult
    {
        public UpdateResultType Type { get; private set; }

        public UpdateResult(Package previous, Package current, UpdateResultType resultType)
            : base(previous, current)
        {
            this.Type = resultType;
        }
    }


    public enum UpdateResultType
    {
        /// <summary>
        /// Upgrade to latest published
        /// </summary>
        Upgrade,

        /// <summary>
        /// Downgrade because installed suspended
        /// </summary>
        DowngradeFromSuspended,

        /// <summary>
        /// Uninstalled suspended, no official versions
        /// </summary>
        UninstallSuspended,

        /// <summary>
        /// The official version is installed
        /// </summary>
        OfficalInstalled,

        /// <summary>
        /// Installed version is newer then official
        /// </summary>
        NewerThanOfficalInstalled,

        /// <summary>
        /// Official version can not be determined
        /// </summary>
        UnknownOfficalVersion,

        /// <summary>
        /// The package was installed because it was specified as a required package
        /// </summary>
        InstalledRequired
    }



    public enum InstallationType
    {
        /// <summary>
        /// Packages was installed
        /// </summary>
        Install,

        /// <summary>
        /// Package was reinstalled
        /// </summary>
        Reinstall,

        /// <summary>
        /// Package was uninstalled
        /// </summary>
        Uninstall,

        /// <summary>
        /// Package was upgraded
        /// </summary>
        Upgrade,

        /// <summary>
        ///  Packages was downgraded
        /// </summary>
        Downgrade
    }
}
