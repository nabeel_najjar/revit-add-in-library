﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RevitAddInLibrary
{
    /// <summary>
    /// Identifies the status of LibraryPackage version in an IPackageLibrary
    /// </summary>
    public enum VersionStatus
    {
        /// <summary>
        /// All users will see published packages, update process will automatically upgrade to the latest published package.
        /// </summary>
        Published =0, 

        /// <summary>
        /// By default users can not see unpublished packages, and version will not automatically be installed. However, users can enable visibility of unpublished packages and install them manually.   
        /// </summary>
        Unpublished =1, 

        /// <summary>
        /// Suspended packages will be hidden to users, and installation is not allowed. Update process will roll back users to last published version, or uninstall package entirely if no version is published.
        /// </summary>
        Suspended = 3, 
    }
}
