﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RevitAddInLibrary
{
    public class RevitApplicationUtil : IRevitApplicationUtil
    {
        public bool IsRevitRunning(int revitRelease)
        {
            var revProc = System.Diagnostics.Process.GetProcessesByName("Revit");
            if (revProc.Length > 0) return true;
            else return false;
        }

        public List<int> GetInstalledRevitReleases()
        {
            List<int> releases = new List<int>();

            var products = Autodesk.RevitAddIns.RevitProductUtility.GetAllInstalledRevitProducts();

            foreach (var product in products)
            {
                int release = 0;

                switch (product.Version)
                {
                    case Autodesk.RevitAddIns.RevitVersion.Revit2011:
                        release = 2011;
                        break;

                    case Autodesk.RevitAddIns.RevitVersion.Revit2012:
                        release = 2012;
                        break;

                    case Autodesk.RevitAddIns.RevitVersion.Revit2013:
                        release = 2013;
                        break;

                    case Autodesk.RevitAddIns.RevitVersion.Revit2014:
                        release = 2014;
                        break;

                    case Autodesk.RevitAddIns.RevitVersion.Revit2015:
                        release = 2015;
                        break;

                    case Autodesk.RevitAddIns.RevitVersion.Revit2016:
                        release = 2016;
                        break;

                    case Autodesk.RevitAddIns.RevitVersion.Unknown:
                        continue;
                }


                if (!releases.Contains(release)) releases.Add(release);




            }

            return releases;


            ////  HKEY_LOCAL_MACHINE\SOFTWARE\Autodesk\Revit\[RELEASEYEAR]\REVIT-[PRODID]:[LANGID]

            //List<int> revitReleases = new List<int>();

            //var revitKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Autodesk\Revit");



            //if (revitKey != null)
            //{

            //    var subKeys = revitKey.GetSubKeyNames();


            //    foreach (string key in subKeys)
            //    {

            //        if (System.Text.RegularExpressions.Regex.IsMatch(key, @"^\d{4}$"))
            //        {
            //            revitReleases.Add(System.Convert.ToInt32(key));
            //        }
            //    }
            //}

            //return revitReleases;
        }
    }
}
