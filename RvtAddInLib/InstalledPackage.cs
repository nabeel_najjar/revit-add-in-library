﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RevitAddInLibrary
{
    
    /// <summary>
    /// A record of an installed package.
    /// </summary>
    /// <remarks>This class is immutable</remarks>
    [DataContract(Namespace = "")]
    public class InstalledPackage
    {

        /// <summary>
        /// The installed package
        /// </summary>
        [DataMember(IsRequired = true, Order = 1)]
        public Package Package { get; private set; }

        /// <summary>
        /// The release of Revit the package is installed for
        /// </summary>
        [DataMember(IsRequired = true, Order = 1)]
        public int RevitRelease { get; private set; }
      
        /// <summary>
        /// The date/time when the package was installed
        /// </summary>
        [DataMember(IsRequired = true, Order = 2)]
        public DateTime InstalledOnUTC { get; private set; }


       
        /// <summary>
        /// Creates a new instance of InstalledPackage
        /// </summary>
        /// <param name="package">The installed package</param>
        /// <param name="installDirectory">The root installation directory</param>
        /// <param name="installedOnUtc">The date and time when the package was installed</param>
        public InstalledPackage(Package package,int revitRelease,DateTime installedOnUtc)
        {
            if (package == null) throw new ArgumentNullException("package");
            this.Package = package;

            if (revitRelease < 0) throw new ArgumentException("revit release should be a positive number", "revitRelease");
            this.RevitRelease = revitRelease;

            if (installedOnUtc.Kind != DateTimeKind.Utc) throw new ArgumentException("installedOnUtc.Kind must be UTC", "installedOnUtc");
            InstalledOnUTC = installedOnUtc;
        }




    }
}
