﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RevitAddInLibrary
{
    public interface IRevitApplicationUtil
    {
        bool IsRevitRunning(int release);
        List<int> GetInstalledRevitReleases();
    }
}
