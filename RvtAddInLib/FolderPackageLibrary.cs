﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Security.Cryptography;

namespace RevitAddInLibrary
{


    /// <summary>
    /// File system based implementation of IPackageLibrary. Reads and writes to and from a library of packages stored in a standard file system directory
    /// </summary>
    public sealed class FolderPackageLibrary : RevitAddInLibrary.IPackageLibrary
    {
        /// <summary>
        /// The root directory of the library
        /// </summary>
        public string Location { get; private set; }

        /// <summary>
        /// Specifies the the 
        /// </summary>
        public bool ReadOnly { get; private set; }

        /// <summary>
        /// Creates a new FolderPackageLibrary instance
        /// </summary>
        /// <param name="rootDirectory">The root directory of the library to reference </param>
        /// <param name="readOnly">Determines if the FolderPackageLibrary should be allowed to modify the library or not</param>
        public FolderPackageLibrary(string rootDirectory, bool readOnly)
        {
            this.ReadOnly = readOnly;
            this.Location = rootDirectory;

            if (!Directory.Exists(rootDirectory))
            {
                throw new System.IO.DirectoryNotFoundException("Could not find specify library root folder: " + rootDirectory);
            }
            if (!File.Exists(GetLibraryDataFilePath())) throw new FileNotFoundException("Could not find library data file", GetLibraryDataFilePath());
        }

        private string GetPackageDataDirectory(string packageCode)
        {
            return System.IO.Path.Combine(Location, packageCode);
        }


        private string GetPackageDataFilePath(string packageCode, string versionName)
        {
            return System.IO.Path.Combine(GetPackageDataDirectory(packageCode), packageCode + "_" + versionName + Package.PACKAGE_EXTENSION);
        }

        public void AddPackage(Stream packageData)
        {

            if (ReadOnly) throw new InvalidOperationException("Library is Read Only");

            using (var ms = new MemoryStream())
            {
                packageData.CopyTo(ms);

                ms.Position = 0;

                var package = Package.ReadPackage(ms);

                using (var libraryDataFS = new FileStream(GetLibraryDataFilePath(), FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    //Reads package list from file
                    //GetPackages() is not use here so reading and writing to file can happen in same file stream session
                    List<LibraryPackage> packages;
                    var ser = GetSerializer();
                    packages = (List<LibraryPackage>)ser.ReadObject(libraryDataFS);


                    //Checks if there are existing package(s) in the library with the same package code and version number or version code
                    var existingPackages = packages.Where(
                        p => p.Package.PackageCode == package.PackageCode &
                            (p.Package.VersionName == package.VersionName | p.Package.VersionNumber == package.VersionNumber));

                    if (existingPackages.Count() > 0) throw new InvalidOperationException("Packages already exists." +
                               "\nPackage Name:" + package.PackageName +
                               "\nPackage Code:" + package.PackageCode +
                               "\nVersion Name:" + package.VersionName +
                               "\nVersion Number:" + package.VersionNumber);

                    try
                    {
                        //Writes the package data the library
                        var packageDataFilePath = GetPackageDataFilePath(package.PackageCode, package.VersionName);
                        var packageDir = GetPackageDataDirectory(package.PackageCode);

                        if (File.Exists(packageDataFilePath)) throw new InvalidOperationException("Package file already exists in library storage:" + packageDataFilePath);

                        if (!Directory.Exists(packageDir)) Directory.CreateDirectory(packageDir);

                        ms.Position = 0;

                        long fileSize = 0;

                        using (var fs = new FileStream(packageDataFilePath, FileMode.CreateNew, FileAccess.Write, FileShare.None))
                        {
                            ms.CopyTo(fs);

                            fileSize = fs.Length;
                        }

                        string user = System.Environment.UserName;

                        //creates the new library package
                        LibraryPackage libPack = new LibraryPackage(package, VersionStatus.Unpublished, user, DateTime.Now.ToUniversalTime(), fileSize);

                        //adds new library package to library data file
                        packages.Add(libPack);
                        libraryDataFS.SetLength(0);
                        ser.WriteObject(libraryDataFS, packages);

                    }
                    catch
                    {
                        DeletePackage(package.PackageCode, package.VersionName);
                        throw;
                    }
                }

            }


            //Updates the atom feed
            UpdateAtomFeed();
        }


        private static readonly string LIBRARYDATAFILENAME = "LibraryPackages.xml";


        private string GetLibraryDataFilePath()
        {
            return Path.Combine(Location, LIBRARYDATAFILENAME);
        }


        private static DataContractSerializer GetSerializer()
        {
            return new System.Runtime.Serialization.DataContractSerializer(typeof(List<LibraryPackage>), "LibraryPackages", string.Empty);
        }


        /// <summary>
        /// Creates new new file system package library at the specified root directory
        /// </summary>
        /// <param name="rootDirectory">The root directory of the new library. This directory path must either not exist, or be empty.</param>
        /// <exception cref="InvalidOperationException">Directory already contains LibraryPackages.xml</exception>
        public static void CreateNew(string rootDirectory)
        {
            if (!Directory.Exists(rootDirectory))
            {
                Directory.CreateDirectory(rootDirectory);
            }

            var dataFilePath = Path.Combine(rootDirectory, LIBRARYDATAFILENAME);

            if (File.Exists(dataFilePath))
            {

                throw new InvalidOperationException("Selected directory already contains a " + LIBRARYDATAFILENAME + " file.");

            }


            using (var fs = new FileStream(dataFilePath, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.None))
            {
                var list = new List<LibraryPackage>();
                var ser = GetSerializer();
                ser.WriteObject(fs, list);
            }
        }

        public void DeletePackage(string packageCode, string versionName)
        {
            if (ReadOnly) throw new InvalidOperationException("Library is Read Only");

            using (var libraryDataFS = new FileStream(GetLibraryDataFilePath(), FileMode.Open, FileAccess.ReadWrite, FileShare.None))
            {
                //Reads package list from file
                //GetPackages() is not use here so reading and writing to file can happen in same file stream session
                List<LibraryPackage> packages;
                var ser = GetSerializer();
                packages = (List<LibraryPackage>)ser.ReadObject(libraryDataFS);


                //Checks if there are existing package(s) in the library with the same package code and version number or version code
                var packageToDelete = packages.Where(
                    p => p.Package.PackageCode == packageCode &
                        p.Package.VersionName == versionName).ToList();


                foreach (var del in packageToDelete)
                {
                    packages.Remove(del);
                }

                //Re Writes the package file
                libraryDataFS.SetLength(0);
                ser.WriteObject(libraryDataFS, packages);
            }


            var packageDataFilePath = GetPackageDataFilePath(packageCode, versionName);

            if (File.Exists(packageDataFilePath)) File.Delete(packageDataFilePath);

            var packageDir = GetPackageDataDirectory(packageCode);

            var dirFiles = Directory.GetFileSystemEntries(packageDir, "*", SearchOption.TopDirectoryOnly);

            if (dirFiles.Length == 0) Directory.Delete(packageDir);

            //Updates the atom feed
            UpdateAtomFeed();
        }

        public Stream GetPackageData(string packageCode, string versionName)
        {
            MemoryStream ms = new MemoryStream();

            using (var fs = new FileStream(GetPackageDataFilePath(packageCode, versionName), FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                fs.CopyTo(ms);
            }

            ms.Position = 0;

            return ms;
        }

        public void UpdatePackage(LibraryPackage updatedPackage)
        {
            using (var libraryDataFS = new FileStream(GetLibraryDataFilePath(), FileMode.Open, FileAccess.ReadWrite, FileShare.None))
            {
                //Reads package list from file
                //GetPackages() is not use here so reading and writing to file can happen in same file stream session
                List<LibraryPackage> packages;
                var ser = GetSerializer();
                packages = (List<LibraryPackage>)ser.ReadObject(libraryDataFS);


                //Checks if there are existing package(s) in the library with the same package code and version number or version code
                var libPackage = packages.Where(
                    p => p.Package.PackageCode == updatedPackage.Package.PackageCode &
                        p.Package.VersionName == updatedPackage.Package.VersionName).FirstOrDefault();

                if (libPackage == null) throw new InvalidOperationException("Packages not found");

                //updates package properties(s)
                libPackage.Status = updatedPackage.Status;
                libPackage.Package.MaxRevitRelease = updatedPackage.Package.MaxRevitRelease;

                //Gets the Data file paths
                var dataFilePath = GetPackageDataFilePath(updatedPackage.Package.PackageCode, updatedPackage.Package.VersionName);
                var temp_DataFilePath = dataFilePath + ".temp";
                var dataFileName = Path.GetFileName(dataFilePath);

                //removes any existing temp data file
                if (File.Exists(temp_DataFilePath)) File.Delete(temp_DataFilePath);

                //Copies the current package data to a temp file
                File.Copy(dataFilePath, temp_DataFilePath);

                //Tries to update the temp package data
                using (var temp_packageDataFS = new FileStream(temp_DataFilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    Package.UpdatePackage(temp_packageDataFS, updatedPackage.Package);
                }


                File.Delete(dataFilePath); //Delete old file
                File.Move(temp_DataFilePath, dataFilePath); //rename temp file to correct name



                //Writes The updated LibraryPackages.xml file
                libraryDataFS.SetLength(0);
                ser.WriteObject(libraryDataFS, packages);
            }


            //Update the atom feed
            UpdateAtomFeed();
        }

        public IEnumerable<string> GetPackageCodes()
        {
            return GetPackages().Select(p => p.Package.PackageCode).Distinct();
        }

        public IEnumerable<LibraryPackage> GetPackages()
        {
            List<LibraryPackage> packages;

            using (var fs = new FileStream(GetLibraryDataFilePath(), FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var ser = GetSerializer();
                packages = (List<LibraryPackage>)ser.ReadObject(fs);
            }

            return packages.OrderBy(p => p.Package.PackageCode).ThenBy(p => p.Package.VersionNumber);
        }

        public IEnumerable<LibraryPackage> GetPackages(int? release, string packageCode)
        {

            if (string.IsNullOrEmpty(packageCode) & release.HasValue)
            {
                //only release
                return GetPackages().Where(p => p.Package.MinRevitRelease <= release & (p.Package.MaxRevitRelease.HasValue && p.Package.MaxRevitRelease.Value > -release.Value));


            }
            else if (!string.IsNullOrEmpty(packageCode) & release.HasValue)
            {
                //release and package code
                return GetPackages().Where(p => p.Package.PackageCode == packageCode && p.Package.MinRevitRelease <= release & (p.Package.MaxRevitRelease.HasValue && p.Package.MaxRevitRelease.Value > -release.Value));
            }

            else if (!string.IsNullOrEmpty(packageCode) & !release.HasValue)
            {
                //only package code
                return GetPackages().Where(p => p.Package.PackageCode == packageCode);
            }
            else
            {
                //neither package code or release
                return GetPackages();
            }
        }

        public LibraryPackage GetPackage(string packageCode, string versionName)
        {
            return GetPackages().Where(p => p.Package.PackageCode == packageCode & p.Package.VersionName == versionName).FirstOrDefault();
        }

        public LibraryPackage GetLatestPublishedVersion(string packageCode, int revitRelease)
        {
            var packages = GetPackages();


            var published = from p in packages
                            where
                            p.Status == VersionStatus.Published
                            && p.Package.PackageCode == packageCode
                            &&
                            p.Package.MinRevitRelease <= revitRelease
                            &&
                            (!p.Package.MaxRevitRelease.HasValue | (p.Package.MaxRevitRelease.HasValue & p.Package.MaxRevitRelease >= revitRelease))
                            orderby p.Package.VersionName
                            descending
                            select p;



            return published.FirstOrDefault();
        }

        #region Atom Feed

        //only include published
        private static XmlDocument GenerateAtomFile(IEnumerable<LibraryPackage> libPackages, Guid feedId)
        {
            List<LibraryPackage> publishedReleasePackages = new List<LibraryPackage>();

            foreach (var libPack in libPackages)
            {
                if (libPack.Status == VersionStatus.Published)
                {
                    publishedReleasePackages.Add(libPack);
                }
            }



            string dateTimeFormat = "yyyy-MM-ddTHH:mm:ssZ";

            XmlDocument xmlDoc = new XmlDocument();
            var feedElement = xmlDoc.CreateElement("feed", "http://www.w3.org/2005/Atom");
            xmlDoc.AppendChild(feedElement);


            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
            xmlDoc.InsertBefore(xmlDeclaration, feedElement);






            //Feed Title
            var feedTitleElement = xmlDoc.CreateElement("title");
            feedTitleElement.InnerText = "Revit Add-In Library";
            feedElement.AppendChild(feedTitleElement);


            //Feed Id
            var feedIdElement = xmlDoc.CreateElement("id");
            string feedGuidString = feedId.ToString("D");
            string idStr = "urn:uuid:" + feedGuidString;
            feedIdElement.InnerText = idStr;
            feedElement.AppendChild(feedIdElement);

            //Feed Updated
            var feedUpdatedElement = xmlDoc.CreateElement("updated");
            var newestPackage = publishedReleasePackages.OrderByDescending(p => p.AddedOnUTC).FirstOrDefault();

            if (newestPackage == null)
            {
                //if no library packages then return
                feedUpdatedElement.InnerText = DateTime.Now.ToString(dateTimeFormat);
                return xmlDoc;
            }
            else
            {
                feedUpdatedElement.InnerText = newestPackage.AddedOnUTC.ToString(dateTimeFormat);
            }


            feedElement.AppendChild(feedUpdatedElement);


            //Entries
            foreach (var package in libPackages.OrderByDescending(p => p.AddedOnUTC))
            {

                var entryElement = xmlDoc.CreateElement("entry");


                var entryTitleElement = xmlDoc.CreateElement("title");
                entryTitleElement.InnerText = package.Package.PackageName + " (" + package.Package.VersionName + ")";
                entryElement.AppendChild(entryTitleElement);


                if (package.Package.Link != null && package.Package.Link.OriginalString != String.Empty)
                {
                    var linkElement = xmlDoc.CreateElement("link");
                    linkElement.InnerText = package.Package.Link.ToString();
                    entryElement.AppendChild(linkElement);
                }



                var packageString = package.Package.PackageCode + package.Package.VersionName;
                Guid packageEntryid = StringToGuid(packageString);


                var entryIdElemetn = xmlDoc.CreateElement("id");
                string entryIdStr = "urn:uuid:" + packageEntryid.ToString("D");
                entryIdElemetn.InnerText = entryIdStr;
                entryElement.AppendChild(entryIdElemetn);


                var entryUpdatedElement = xmlDoc.CreateElement("updated");
                entryUpdatedElement.InnerText = package.AddedOnUTC.ToString(dateTimeFormat);
                entryElement.AppendChild(entryUpdatedElement);




                try
                {
                    var summaryElement = xmlDoc.CreateElement("summary");
                    summaryElement.InnerText = package.Package.VersionNotes;
                    entryElement.AppendChild(summaryElement);
                }
                catch (Exception)
                {

                    throw;
                }

                //Author
                var authorElement = xmlDoc.CreateElement("author");


                var nameElement = xmlDoc.CreateElement("name");
                nameElement.InnerText = package.Package.Author;
                authorElement.AppendChild(nameElement);


                var emailElement = xmlDoc.CreateElement("email");
                emailElement.InnerText = package.Package.Email;
                authorElement.AppendChild(emailElement);

                entryElement.AppendChild(authorElement);



                //appends Entry element to feed
                feedElement.AppendChild(entryElement);

            }


            return xmlDoc;
        }

        private static Guid StringToGuid(string packageString)
        {
            Guid packageEntryid;
            using (MD5 md5 = MD5.Create())
            {
                byte[] hash = md5.ComputeHash(Encoding.Default.GetBytes(packageString));
                packageEntryid = new Guid(hash);
            }
            return packageEntryid;
        }


        private void UpdateAtomFeed()
        {
            var allPublishedPackages = GetPackages().Where(p => p.Status == VersionStatus.Published);

            Guid feedId = StringToGuid(this.Location);

            var feedXmlDoc = GenerateAtomFile(allPublishedPackages, feedId);

            string feedFileName = "AtomFeed.xml";

            string feedFilePath = Path.Combine(this.Location, feedFileName);

            if (File.Exists(feedFilePath)) File.Delete(feedFilePath);

            feedXmlDoc.Save(feedFilePath);
        }


        #endregion

    }
}
