﻿using RevitAddInLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RevitAddInLibrary.Manager
{
    public class NewPackageCodeListPageViewModel : ValidatedViewModel, IWizardPage
    {
        private NewPackageInfoPageViewModel _infoPageViewModel;
        private IEnumerable<Package> templatePackages;

        public NewPackageCodeListPageViewModel(IEnumerable<Package> templatePackages, NewPackageInfoPageViewModel infoPageViewModel)
        {
            // TODO: Complete member initialization
            this.templatePackages = templatePackages;
            this._infoPageViewModel = infoPageViewModel;



            this.TemplatePackages = new ObservableCollection<object>();

            var newProjectViewModel = new NewPackageViewModel();

            this.TemplatePackages.Add(newProjectViewModel);

            this.SelectedTemplatePackage = newProjectViewModel;

            foreach (var p in templatePackages)
            {
                this.TemplatePackages.Add(p);
            }




        }


        public class NewPackageViewModel
        {
            public string PackageCode
            {
                get
                { return "<No Template>"; }
            }
        
        }





        private object _selectedTemplatePackage;

        public object SelectedTemplatePackage
        {

            get
            {
                return _selectedTemplatePackage;
            }




            set
            {
                this._selectedTemplatePackage = value;
                UpdateInfoPage();
            }
        }

        private void UpdateInfoPage()
        {
            if (_selectedTemplatePackage is NewPackageViewModel | _selectedTemplatePackage == null)
            {

                _infoPageViewModel.PackageCode = string.Empty;
                _infoPageViewModel.PackageName = string.Empty;
                _infoPageViewModel.PackageNotes = string.Empty;

                _infoPageViewModel.Author = string.Empty;
                _infoPageViewModel.Email = string.Empty;

                _infoPageViewModel.VersionNumber = 0;
                _infoPageViewModel.VersionName = string.Empty;
                _infoPageViewModel.VersionNotes= string.Empty;
                
            }
            else
            {
                Package p = SelectedTemplatePackage as Package;

                _infoPageViewModel.PackageCode = p.PackageCode;
                _infoPageViewModel.PackageName = p.PackageName;
                _infoPageViewModel.PackageNotes = p.PackageNotes;


                _infoPageViewModel.MinRevitRelease = p.MinRevitRelease;

                _infoPageViewModel.Author = p.Author;
                _infoPageViewModel.Email = p.Email;
                _infoPageViewModel.Link = p.Link;

                _infoPageViewModel.VersionNumber = p.VersionNumber + 1;
                _infoPageViewModel.VersionName = string.Empty;
                _infoPageViewModel.VersionNotes = string.Empty;

            
            }
           
        }



        public ObservableCollection<object> TemplatePackages { get; private set; }




        public override string[] ValidatedPropertyNames
        {
            get { return new string[] { }; }
        }

        public override string this[string columnName]
        {
            get { return string.Empty; }
        }

        public string PageName
        {
            get { return "Package Information Template"; }
        }

        public void ShowingPage()
        {
            return;
        }
    }
}

