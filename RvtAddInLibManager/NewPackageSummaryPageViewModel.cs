﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevitAddInLibrary.Manager
{
    public class NewPackageSummaryPageViewModel : IWizardPage
    {
        public NewPackageInfoPageViewModel InfoPage { get; private set; }
        public NewPackageFilesPageViewModel FilesPage { get; private set; }

        

        public NewPackageSummaryPageViewModel(NewPackageInfoPageViewModel infoPage, NewPackageFilesPageViewModel filesPage, bool libAvailble)
        {
            this.InfoPage = infoPage;
            this.FilesPage = filesPage;

            if (libAvailble)
            {
                this.DestinationFile = false;
                this.DestinationLibrary = true;
                LibraryDestinationEnabled = true;
            }
            else
            {
                this.DestinationFile = true;
                this.DestinationLibrary = false;
                LibraryDestinationEnabled = false;
            }
            
        }

        public string PageName
        {
            get { return "Package Summary"; }
        }


        public bool LibraryDestinationEnabled
        {
            get;
            private set;
        }

        public bool DestinationLibrary
        {
            get;
             set;
        }

        public bool DestinationFile
        {
            get;
             set;
        }


        public bool IsValid
        {
            get { return true; }
        }


        public void ShowingPage()
        {
         

        }
    }
}
