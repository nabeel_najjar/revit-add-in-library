﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RevitAddInLibrary;

namespace RevitAddInLibrary.Manager
{
    /// <summary>
    /// Interaction logic for NewReleaseWizardWindow.xaml
    /// </summary>
    public partial class NewPackageWizardWindow : Window
    {
        public NewPackageWizardWindowViewModel ViewModel { get; private set; }



        public NewPackageWizardWindow(IEnumerable<Package> templatePackages, IPackageLibrary library)
        {
            InitializeComponent();
            // TODO: Complete member initialization

            this.ViewModel = new NewPackageWizardWindowViewModel(templatePackages, new Action<bool>(this.CloseWindow), library);
            this.DataContext = ViewModel;

        }

        private void CloseWindow(bool result)
        {
            this.DialogResult = result;
            this.Close();
        }
    }
}
