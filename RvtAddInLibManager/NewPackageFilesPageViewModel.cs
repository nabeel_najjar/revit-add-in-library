﻿using Mono.Cecil;
using Ookii.Dialogs.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.IO;

namespace RevitAddInLibrary.Manager
{
    public class NewPackageFilesPageViewModel : ValidatedViewModel, IWizardPage
    {

        private string[] imageExtensions = new string[] { ".png", ".gif", ".bmp", ".jpg", ".jpeg", ".ico" };


        public string PageName
        {
            get { return "Package Files"; }
        }







        private string _filesSourceFolder;
        public string FilesSourceFolder
        {
            get
            {
                return _filesSourceFolder;
            }
            set
            {
                if (value != _filesSourceFolder)
                {

                    _filesSourceFolder = value;
                    UpdateFileList();
                    OnPropertyChanged("FilesSourceFolder");
                }

            }
        }


        private string _addInText;
        public string AddInText
        {
            get
            {
                return _addInText;
            }
            set
            {
                if (value != _addInText)
                {

                    _addInText = value;
                    OnPropertyChanged("AddInText");
                }

            }
        }





        private void ClearSourceFolder()
        {
            _filesSourceFolder = null;
            this.FileNames.Clear();
            OnPropertyChanged("FilesSourceFolder");
        }








        private void UpdateFileList()
        {
            this.FileNames.Clear();
            this.ImageFilesNames.Clear();

            if (System.IO.Directory.Exists(_filesSourceFolder))
            {
                string[] files = new string[] { };


                files = System.IO.Directory.GetFiles(_filesSourceFolder, "*", System.IO.SearchOption.AllDirectories);


                //if the directory contains more then 100 files warn the user
                int NewPackageFileCountThreshold = 100;

                int threshold = System.Convert.ToInt32(NewPackageFileCountThreshold);

                if (files.Count() > threshold)
                {

                    Ookii.Dialogs.Wpf.TaskDialog diag = new Ookii.Dialogs.Wpf.TaskDialog();

                    TaskDialogButton yesButton = new TaskDialogButton(ButtonType.Yes);
                    TaskDialogButton noButton = new TaskDialogButton(ButtonType.No);

                    diag.Buttons.Add(yesButton);
                    diag.Buttons.Add(noButton);

                    diag.WindowTitle = "Warning";

                    diag.MainInstruction = "You have selected a folder which contains a large number of files. Are you sure you want to select this folder?";

                    var res = diag.ShowDialog();

                    if (res == noButton)
                    {
                        ClearSourceFolder();
                        return;
                    }
                }


                foreach (var fileName in files)
                {
                    Uri fullPath = new Uri(fileName, UriKind.Absolute);
                    Uri relRoot = new Uri(_filesSourceFolder, UriKind.Absolute);

                    Uri relUri = relRoot.MakeRelativeUri(fullPath);

                    string relativeFileName = relUri.ToString();



                    this.FileNames.Add(relativeFileName);

                    var ext = System.IO.Path.GetExtension(relativeFileName);

                    if (imageExtensions.Contains(ext)) ImageFilesNames.Add(relativeFileName);
                }



            }


            //update file info text
            if (this.FileNames.Count == 0)
            {
                this.FilesInfoText = "No files found";
            }
            else if (this.FileNames.Count == 1)
            {
                this.FilesInfoText = "1 file found";
            }
            else
            {
                this.FilesInfoText = this.FileNames.Count.ToString() + " files found";
            }


        }



        private string _filesInfoText;
        public string FilesInfoText
        {
            get
            {
                return _filesInfoText;
            }
            set
            {
                if (value != _filesInfoText)
                {
                    _filesInfoText = value;
                    OnPropertyChanged("FilesInfoText");
                }
            }
        }


        public ObservableCollection<string> ImageFilesNames { get; private set; }

        public ObservableCollection<string> FileNames
        {
            get;
            private set;
        }


        private NewPackageInfoPageViewModel _infoPage;

        public NewPackageFilesPageViewModel(NewPackageInfoPageViewModel infoPage)
        {
            this._infoPage = infoPage;


            FileNames = new ObservableCollection<string>();
            ImageFilesNames = new ObservableCollection<string>();


      


            this.SelectFilesSourceFolderCommand = new RelayCommand(new Action<object>(this.SelectFilesSourceFolder));


        }




        public ICommand SelectFilesSourceFolderCommand { get; private set; }

        private void SelectFilesSourceFolder(object o)
        {
            Ookii.Dialogs.Wpf.VistaFolderBrowserDialog diag = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();

            diag.Description = "Select Files Source Folder";
            diag.UseDescriptionForTitle = true;
            diag.ShowNewFolderButton = true;

            bool? res = diag.ShowDialog();

            if (res.HasValue && res.Value)
            {
                this.FilesSourceFolder = diag.SelectedPath;
            }
        }







        public override string this[string columnName]
        {
            get
            {
                if (columnName == "FilesSourceFolder")
                {
                    if (string.IsNullOrEmpty(FilesSourceFolder))
                    {
                        return "Source folder is not specified";
                    }

                    if (!System.IO.Directory.Exists(FilesSourceFolder))
                    {
                        return "Source folder does not exist.";
                    }

                    string addInFileName = _infoPage.PackageCode + ".addin";

                    if (!File.Exists(Path.Combine(FilesSourceFolder, addInFileName)))
                    {
                        return "Source folder does not contain a matching add-in manifest file: " + addInFileName;
                    }

                    if (File.Exists(Path.Combine(FilesSourceFolder, "Package.xml")))
                    {
                        return "Source folder contains a Package.xml file";
                    }
                }

                return String.Empty;
            }
        }

        public override string[] ValidatedPropertyNames
        {
            get
            {
                return new string[] { "FilesSourceFolder", "InstallFolder", "AddInText" };
            }
        }


        public void ShowingPage()
        {
            //does nothing
        }
    }
}
