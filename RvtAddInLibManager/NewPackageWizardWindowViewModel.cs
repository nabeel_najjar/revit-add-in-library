﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Mono.Cecil;
using Mono.Cecil.Cil;


namespace RevitAddInLibrary.Manager
{
    public class NewPackageWizardWindowViewModel : INotifyPropertyChanged
    {

        public NewPackageCodeListPageViewModel NewPackageCodeListPageViewModel { get; private set; }
        public NewPackageInfoPageViewModel NewReleaseInfoPageViewModel { get; private set; }
        public NewPackageFilesPageViewModel NewReleaseFilesPageViewModel { get; private set; }
        public NewPackageSummaryPageViewModel NewReleaseSummaryPageViewModel { get; private set; }


        public Action<bool> _closeWindowAction;





        public NewPackageWizardWindowViewModel(IEnumerable<Package> templatePackages, Action<bool> closeWindowAction,IPackageLibrary library)
        {

            _closeWindowAction = closeWindowAction;



            //inits page view models

            this.NewReleaseInfoPageViewModel = new NewPackageInfoPageViewModel(library);
            this.NewPackageCodeListPageViewModel = new NewPackageCodeListPageViewModel(templatePackages, NewReleaseInfoPageViewModel);
            this.NewReleaseFilesPageViewModel = new NewPackageFilesPageViewModel(this.NewReleaseInfoPageViewModel);

            bool libAvailble = false;
            if (library != null) libAvailble = true;

            this.NewReleaseSummaryPageViewModel = new NewPackageSummaryPageViewModel(this.NewReleaseInfoPageViewModel, this.NewReleaseFilesPageViewModel, libAvailble);



            Pages = new ObservableCollection<IWizardPage>();


       
            Pages.Add(this.NewPackageCodeListPageViewModel);
            Pages.Add(this.NewReleaseInfoPageViewModel);
            Pages.Add(this.NewReleaseFilesPageViewModel);
            Pages.Add(this.NewReleaseSummaryPageViewModel);



            //inits current page
            this.CurrentPage = Pages[0];


            //wires commands
            this.NextPageCommand = new RelayCommand(new Action<object>(this.NextPage), new Predicate<object>(this.CanNextPage));
            this.PreviousPageCommand = new RelayCommand(new Action<object>(this.PreviousPage), new Predicate<object>(this.CanPreviousPage));

            this.SubmitCommand = new RelayCommand(new Action<object>(this.Submit), new Predicate<object>(this.CanSubmit));
            this.CancelCommand = new RelayCommand(new Action<object>(this.Cancel));
        }


        public ICommand NextPageCommand { get; private set; }
        public ICommand PreviousPageCommand { get; private set; }
        public ICommand SubmitCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public ObservableCollection<IWizardPage> Pages { get; set; }

        private IWizardPage _currentPage;
        public IWizardPage CurrentPage
        {
            get
            {
                return _currentPage;
            }
            set
            {
                if (_currentPage != value)
                {
                    _currentPage = value;
                    OnPropertyChanged("CurrentPage");
                }
            }
        }

        public int CurrentPageIndex
        {
            get
            {
                return Pages.IndexOf(CurrentPage);
            }
        }

        private void NextPage(object o)
        {
            CurrentPage = Pages[CurrentPageIndex + 1];
            CurrentPage.ShowingPage();
        }

        private bool CanNextPage(object o)
        {
            if (CurrentPage.IsValid && CurrentPageIndex < Pages.Count - 1)
            {
                return true;
            }
            else return false;
        }


        private void PreviousPage(object o)
        {
            CurrentPage = Pages[CurrentPageIndex - 1];
            CurrentPage.ShowingPage();
        }


        private bool CanPreviousPage(object o)
        {
            if (CurrentPageIndex == 0) return false;
            else return true;
        }






        private void Submit(object o)
        {
            _closeWindowAction.Invoke(true);
        }

        private void Cancel(object o)
        {
            _closeWindowAction.Invoke(false);
        }

        private bool CanSubmit(object o)
        {

            if (this.NewReleaseInfoPageViewModel.IsValid &&
                this.NewReleaseFilesPageViewModel.IsValid &&
                CurrentPage == this.NewReleaseSummaryPageViewModel)
            {
                return true;
            }
            else return false;

        }




        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;



        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion



    }
}
