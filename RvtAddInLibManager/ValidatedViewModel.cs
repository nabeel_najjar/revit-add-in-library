﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevitAddInLibrary.Manager
{
    public abstract class ValidatedViewModel : INotifyPropertyChanged, IDataErrorInfo
    {

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;


        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion



        public bool IsValid
        {
            get
            {
                if (string.IsNullOrEmpty(Error)) return true;
                else return false;
            }
        }


        public abstract string[] ValidatedPropertyNames
        {
            get;
        }

        public string Error
        {
            get
            {
                foreach (string pName in ValidatedPropertyNames)
                {
                    string currentError = this[pName];

                    if (string.IsNullOrEmpty(currentError)) continue;
                    else return currentError;
                }

                return String.Empty;
            }
        }

        public abstract string this[string columnName]
        {
            get;
        }
    }
}
