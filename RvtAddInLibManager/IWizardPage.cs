﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevitAddInLibrary.Manager
{
    public interface IWizardPage
    {
        string PageName { get; }
        bool IsValid { get; }
        void ShowingPage();
    }
}
