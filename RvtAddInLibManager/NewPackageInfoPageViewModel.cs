﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevitAddInLibrary.Manager
{
    public class NewPackageInfoPageViewModel : ValidatedViewModel, IWizardPage
    {




        public string PageName
        {
            get { return "Package Information"; }
        }








        private List<Package> existingPackages;

        public NewPackageInfoPageViewModel(IPackageLibrary library)
        {

            if (library != null)
            {
                existingPackages = library.GetPackages().Select(l => l.Package).ToList();
            }
            else
            {
                existingPackages = new List<Package>();
            }
        }






        private string _packageName;
        public string PackageName
        {
            get
            {
                return _packageName;
            }
            set
            {
                if (value != _packageName)
                {
                    _packageName = value;
                    OnPropertyChanged("PackageName");
                }
            }
        }


        private string _author;
        public string Author
        {
            get
            {
                return _author;
            }
            set
            {
                if (value != _author)
                {
                    _author = value;
                    OnPropertyChanged("Author");
                }
            }
        }


        private string _email;
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                if (value != _email)
                {
                    _email = value;
                    OnPropertyChanged("Email");
                }
            }
        }



        private Uri _link;
        public Uri Link
        {
            get
            {
                return _link;
            }
            set
            {
                if (value != _link)
                {
                    _link = value;
                    OnPropertyChanged("Link");
                }
            }
        }




        private string _packageNotes;
        public string PackageNotes
        {
            get
            {
                return _packageNotes;
            }
            set
            {
                if (value != _packageNotes)
                {
                    _packageNotes = value;
                    OnPropertyChanged("PackageNotes");
                }
            }
        }



        private string _packageCode;
        public string PackageCode
        {
            get
            {
                return _packageCode;
            }
            set
            {
                if (value != _packageCode)
                {
                    _packageCode = value;
                    OnPropertyChanged("PackageCode");
                }
            }
        }










        private int _minRevitRelease;
        public int MinRevitRelease
        {
            get
            {
                return _minRevitRelease;
            }
            set
            {
                if (value != _minRevitRelease)
                {
                    _minRevitRelease = value;
                    OnPropertyChanged("MinRevitRelease");
                }
            }
        }




        public int? MaxRevitRelease
        {
            get
            {
                try
                {
                    if (String.IsNullOrEmpty(_maxReleaseStr))
                    {
                        return null;
                    }
                    else
                    {
                        var release = System.Convert.ToInt32(_maxReleaseStr);
                        return release;
                    }
                }
                catch
                {
                    return null;
                }
            }
        }




        private string _maxReleaseStr;
        public string MaxRevitReleaseStr
        {
            get
            {
                return _maxReleaseStr;
            }
            set
            {
                if (value != _maxReleaseStr)
                {



                    _maxReleaseStr = value;


                    OnPropertyChanged("MaxRevitReleaseStr");
                    OnPropertyChanged("MaxRevitRelease");

                }
            }
        }



        public string TargetRelease
        {
            get
            {
                if (MaxRevitRelease.HasValue)
                {
                    return MinRevitRelease.ToString() + "-" + MaxRevitRelease.Value.ToString();
                }
                else
                {
                    return MinRevitRelease.ToString() + "+";

                }
            }

        }









        private string _versionNumber;
        public string VersionName
        {
            get
            {
                return _versionNumber;
            }
            set
            {
                if (value != _versionNumber)
                {
                    _versionNumber = value;
                    OnPropertyChanged("VersionNumber");
                }
            }
        }


        private int _versionCode;
        public int VersionNumber
        {
            get
            {
                return _versionCode;
            }
            set
            {
                if (value != _versionCode)
                {
                    _versionCode = value;
                    OnPropertyChanged("VersionCode");
                }
            }
        }



        private string _versionNotes;
        public string VersionNotes
        {
            get
            {
                return _versionNotes;
            }
            set
            {
                if (value != _versionNotes)
                {
                    _versionNotes = value;
                    OnPropertyChanged("VersionNotes");
                }
            }
        }


        public override string this[string columnName]
        {
            get
            {

                if (columnName == "PackageName")
                {
                    if (string.IsNullOrEmpty(PackageName)) return "Package name required";

                    if (PackageName.Contains("\n") | PackageName.Contains("\r")) return "Package name can not include line breaks";

                }
                else if (columnName == "PackageCode")
                {
                    if (string.IsNullOrEmpty(PackageCode)) return "Package code required";

                    if (!System.Text.RegularExpressions.Regex.IsMatch(PackageCode, @"^[a-z\.\d-]*$"))
                    {
                        return "Invalid package code. Package codes may only contain lower case letters, numbers, periods, and hyphens.";
                    }
                }
                else if (columnName == "MinRevitRelease")
                {
                    if (MinRevitRelease <= 0)
                    {
                        return "Min release must be a positive integer (2013, 2014, 2015 etc..)";
                    }

                }
                else if (columnName == "MaxRevitReleaseStr")
                {
                    if (MaxRevitRelease.HasValue && MaxRevitRelease.Value <= 0)
                    {
                        return "Max release must be a positive integer (2013, 2014, 2015 etc..)";
                    }
                    else if (MaxRevitRelease.HasValue && MaxRevitRelease.Value < MinRevitRelease)
                    {
                        return "Max release must greater or equal to Target Release (2013, 2014, 2015 etc..)";
                    }


                    if (!String.IsNullOrEmpty(MaxRevitReleaseStr))
                    {
                        try
                        {
                            System.Convert.ToInt32(MaxRevitReleaseStr);
                        }
                        catch
                        {
                            return "Max release must be a positive integer (2013, 2014, 2015 etc..)";

                        }
                    }
                }
                else if (columnName == "VersionName")
                {

                    //checks that version number is not blank
                    if (string.IsNullOrEmpty(VersionName) || string.IsNullOrWhiteSpace(VersionName))
                    {
                        return "Version number can not be blank or only white space";
                    }
                    else if (VersionName != VersionName.Trim())
                    {
                        return "Version number can not start or end with whitespace";
                    }
                    else if (VersionName.Contains("\n") | VersionName.Contains("\r"))
                    {
                        return "Version number can not include line breaks";
                    }

                    if (!String.IsNullOrEmpty(PackageCode) && !String.IsNullOrEmpty(VersionName))
                    {
                        int exCount = existingPackages.Where(ex => ex.PackageCode == this.PackageCode && ex.VersionName == this.VersionName).Count();

                        if (exCount > 0)
                        {
                            return "Version " + VersionName + " already exists in the library for " + PackageCode;
                        }
                    }


                }
                else if (columnName == "VersionNumber")
                {
                    if (VersionNumber <= 0) return "Version code must be greater then zero";



                    if (!String.IsNullOrEmpty(PackageCode))
                    {
                        int exCount = existingPackages.Where(ex => ex.PackageCode == this.PackageCode && ex.VersionNumber == this.VersionNumber).Count();

                        if (exCount > 0)
                        {
                            return "The library already contains a " + PackageCode + " package with version number " + VersionNumber.ToString();
                        }
                    }

                }


                return string.Empty;
            }
        }

        public override string[] ValidatedPropertyNames
        {
            get
            {
                return new string[] { "PackageName", "PackageCode", "MinRevitRelease", "MaxRevitReleaseStr", "VersionName" };
            }
        }


        public void ShowingPage()
        {
            //does nothing
        }
    }
}
