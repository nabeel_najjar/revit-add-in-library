﻿using RevitAddInLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevitAddInLibrary.Manager
{
    public class PackageViewModel : INotifyPropertyChanged
    {
        public LibraryPackage Package { get; private set; }


        public string PackageName { get; private set; }
        public string PackageCode { get; private set; }
        public string PackageNotes { get; private set; }

        public string AddedBy { get; private set; }


        public string VersionName { get; private set; }
        public int VersionNumber { get; private set; }
        public string VersionNotes { get; private set; }

        public string Author { get; private set; }
        public string Email { get; private set; }

        public Uri Link { get; private set; }



        public DateTime AddedOn { get; private set; }

        public string FileSize { get; private set; }

        private int _minRevitRelease;
        public int MinRevitRelease
        {
            get
            {
                return _minRevitRelease;
            }
            set
            {
                if (_minRevitRelease != value)
                {
                    _minRevitRelease = value;
                    OnPropertyChanged("MinRevitRelease");
                    OnPropertyChanged("SupportedRevitReleases");
                }
            }
        }




        private int? _maxRevitRelease;
        public int? MaxRevitRelease 
        {
            get
            {
                return _maxRevitRelease;
            }
            set
            {
                if (_maxRevitRelease != value)
                {
                    _maxRevitRelease = value;
                    OnPropertyChanged("MaxRevitRelease");
                    OnPropertyChanged("SupportedRevitReleases");

                    
                }
            }
        }

        public string SupportedRevitReleases
        {
            get
            {
                if (MaxRevitRelease.HasValue)
                {
                    return MinRevitRelease.ToString() + "-" + MaxRevitRelease.ToString();
                }
                else
                {
                    return MinRevitRelease.ToString() + "+";
                }
            }
        }
        

        

        

        private VersionStatus _status;
        public VersionStatus Status
        {
            get { return _status; }
            set
            {
                if (_status != value)
                {
                    _status = value;
                    OnPropertyChanged("Status");
                    OnPropertyChanged("SupportedRevitReleases");
                }
            }
        }




        public PackageViewModel(LibraryPackage package)
        {
            this.Package = package;

            this.PackageName = package.Package.PackageName;
            this.PackageCode = package.Package.PackageCode;
            this.PackageNotes = package.Package.PackageNotes;

            this.VersionName = package.Package.VersionName;
            this.VersionNumber = package.Package.VersionNumber;
            this.VersionNotes = package.Package.VersionNotes;

            

            this.MinRevitRelease = package.Package.MinRevitRelease;
            this.MaxRevitRelease = package.Package.MaxRevitRelease;
            //this.SupportedRevitReleases = Package.Package.SupportedRevitReleases;

            this.Author = package.Package.Author;
            this.Email = package.Package.Email;
            this.Link = package.Package.Link;

            this.Status = package.Status;


            this.AddedBy = package.AddedBy;
            this.AddedOn = package.AddedOnUTC.ToLocalTime();

            this.FileSize = BytesToString(package.FileSize);
        }


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion



        static String BytesToString(long byteCount)
        {
            string[] suf = { "B", "KB", "MB", "GB", "TB", "PB", "EB" }; //Longs run out around EB
            if (byteCount == 0)
                return "0" + suf[0];
            long bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return (Math.Sign(byteCount) * num).ToString() +" "+ suf[place];
        }

    }
}


