﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RevitAddInLibrary.Manager
{
    /// <summary>
    /// Interaction logic for SetMaxReleaseWindow.xaml
    /// </summary>
    public partial class SetMaxReleaseWindow : Window
    {


        public int MinRevitRelease { get; private set; }

        public int? MaxRevitRelease { get; private set; }


        public SetMaxReleaseWindow(PackageViewModel p)
        {

            InitializeComponent();

            ContentLabel.Content = "Package Name: " + p.PackageName +
                "\nPackage Code: " + p.PackageCode +
                "\nVersion Name: " + p.VersionName +
                "\nVersion Code: " + p.VersionNumber.ToString();


            this.MinRevitRelease = p.MinRevitRelease;

            maxReleaseTextBox.Text = p.MaxRevitRelease.ToString();


            this.DataContext = this;
        }

        public string ContentText { get; set; }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            int maxR;
            
            var x = int.TryParse(maxReleaseTextBox.Text, out maxR);

            if (x)
            {
                this.MaxRevitRelease = maxR;
            }
            else
            {
                this.MaxRevitRelease = null;
                maxReleaseTextBox.Text = string.Empty;
            }
        }

   
    }
}

