﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevitAddInLibrary.Manager
{
    public class NewPackageDestinationPageViewModel : ValidatedViewModel, IWizardPage
    {
        public override string[] ValidatedPropertyNames
        {
            get
            {
                return new string[] { };
            }
        }

        public override string this[string columnName]
        {
            get { return string.Empty; }
        }

        public string PageName
        {
            get { return "Package Destination"; }
        }

        public void ShowingPage()
        {
            
        }
    }
}
