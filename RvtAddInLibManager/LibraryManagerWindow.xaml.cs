﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RevitAddInLibrary.Manager
{
    /// <summary>
    /// Interaction logic for ComponentDetailsWindow.xaml
    /// </summary>
    public partial class LibraryManagerWindow : Window
    {
        private LibraryManagerWindowViewModel vm;

        public LibraryManagerWindow()
        {
            InitializeComponent();

            vm = new LibraryManagerWindowViewModel();

            this.DataContext = vm;
        }

 
    }
}
