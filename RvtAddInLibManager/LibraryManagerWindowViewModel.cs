﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Ookii.Dialogs.Wpf;
using RevitAddInLibrary;
using System.IO;
using NLog;
using System.Configuration;

namespace RevitAddInLibrary.Manager
{
    public class LibraryManagerWindowViewModel : INotifyPropertyChanged
    {
        public IPackageLibrary CurrentLibrary { get; private set; }
        public ObservableCollection<PackageViewModel> Packages { get; private set; }
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private static readonly string windowTitle = "Revit Add-In Library Manager";



        /// <summary>
        /// Loads the latest components from the file and refilters
        /// </summary>
        private void RefreshPackageList()
        {
            this.Packages.Clear();

            if (CurrentLibrary != null)
            {


                foreach (var p in CurrentLibrary.GetPackages())
                {
                    this.Packages.Add(new PackageViewModel(p));
                }
            }
        }

        private string _windowTitle;
        public string WindowTitle
        {
            get { return _windowTitle; }
            set
            {
                if (value != _windowTitle)
                {
                    _windowTitle = value;
                    OnPropertyChanged("WindowTitle");
                }
            }
        }


        private PackageViewModel _selectedPackage;
        public PackageViewModel SelectedPackage
        {
            get { return _selectedPackage; }
            set
            {
                if (value != _selectedPackage)
                {
                    _selectedPackage = value;
                    OnPropertyChanged("SelectedPackage");
                }
            }
        }

        public ICommand DownloadFileCommand { get; private set; }

        public ICommand NewLibraryCommand { get; private set; }
        public ICommand OpenLibraryCommand { get; private set; }
        public ICommand NewPackageCommand { get; private set; }
        public ICommand AddPackageFileCommand { get; private set; }
        public ICommand SetVersionStatusCommand { get; private set; }
        public ICommand DeletedPackageCommand { get; private set; }
        public ICommand SetMaxReleaseCommand { get; private set; }


        public LibraryManagerWindowViewModel()
        {
            Packages = new ObservableCollection<PackageViewModel>();


            this.OpenLibraryCommand = new RelayCommand(new Action<object>(this.OpenLibrary));
            this.NewPackageCommand = new RelayCommand(new Action<object>(this.NewPackage));
            this.AddPackageFileCommand = new RelayCommand(new Action<object>(this.AddPackageFile), new Predicate<object>(CanAddPackageFile));

            this.SetVersionStatusCommand = new RelayCommand(new Action<object>(SetVersionStatus), new Predicate<object>(PackageIsSelected));
            this.DownloadFileCommand = new RelayCommand(new Action<object>(DownloadFile), new Predicate<object>(PackageIsSelected));
            this.DeletedPackageCommand = new RelayCommand(new Action<object>(DeletePackage), new Predicate<object>(PackageIsSelected));

            this.SetMaxReleaseCommand = new RelayCommand(new Action<object>(SetMaxRelease), new Predicate<object>(PackageIsSelected));

            //Set
            WindowTitle = windowTitle;


            string libraryLocation = ConfigurationManager.AppSettings["Library"];

            if (String.IsNullOrEmpty(libraryLocation))
            {


                Ookii.Dialogs.Wpf.TaskDialog diag = new Ookii.Dialogs.Wpf.TaskDialog();

                TaskDialogButton okButton = new TaskDialogButton(ButtonType.Ok);

                diag.Buttons.Add(okButton);



                diag.MainInstruction = "The library location has not been set in the configuration file.";

                diag.MainIcon = TaskDialogIcon.Warning;


                var res = diag.ShowDialog();




            }
            else
            {
                TryOpenLibrary(ConfigurationManager.AppSettings["Library"]);
            }
        }

        private void SetMaxRelease(object obj)
        {
            SetMaxReleaseWindow setMaxRElDiag = new SetMaxReleaseWindow(SelectedPackage);

            int? oldMaxRelease = SelectedPackage.Package.Package.MaxRevitRelease;

            var res = setMaxRElDiag.ShowDialog();



            int? newMaxRelease = setMaxRElDiag.MaxRevitRelease;

            if (res.HasValue&&res.Value&&newMaxRelease != oldMaxRelease)
            {


                try
                {
                    SelectedPackage.MaxRevitRelease = newMaxRelease;
                    SelectedPackage.Package.Package.MaxRevitRelease = newMaxRelease;
                    CurrentLibrary.UpdatePackage(SelectedPackage.Package);
                }
                catch (Exception exp)
                {
                    SelectedPackage.MaxRevitRelease = oldMaxRelease;

                    logger.Error(exp);

                    ShowErrorDialog("Could not change package status", exp);
                }

            }



        }


        private void DownloadFile(object o)
        {
            VistaSaveFileDialog sDiag = new VistaSaveFileDialog();
            sDiag.Filter = "Revit Add-In Package (*.rpk)|*.rpk";
            sDiag.Title = "Save Package File";
            sDiag.FileName = SelectedPackage.PackageCode + "_" + SelectedPackage.VersionName + ".rpk";

            var res = sDiag.ShowDialog();

            if (res.HasValue && res.Value)
            {


                Stream packageData = null;

                try
                {
                    packageData = CurrentLibrary.GetPackageData(SelectedPackage.PackageCode, SelectedPackage.VersionName);

                    using (var fs = new FileStream(sDiag.FileName, FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        packageData.CopyTo(fs);
                    }
                }
                catch (Exception exp)
                {

                    ShowErrorDialog("There was a problem creating the new library", exp);
                    logger.Error(exp);

                    return;
                }
                finally
                {
                    if (packageData != null)
                    {
                        packageData.Dispose();
                    }

                }

            }


        }


        private bool PackageIsSelected(object o)
        {
            if (SelectedPackage != null) return true;
            else return false;
        }




        private void SetVersionStatus(object o)
        {


            TaskDialog diag = new TaskDialog();


            diag.ButtonStyle = TaskDialogButtonStyle.CommandLinks;

            diag.ExpandedByDefault = false;
            diag.ExpandFooterArea = true;
            diag.MinimizeBox = false;
            diag.AllowDialogCancellation = true;
            diag.WindowTitle = "Set Package Version Status";
            //diag.MainIcon = TaskDialogIcon.Information;
            diag.CustomMainIcon = new System.Drawing.Icon("RvtAddInLib.ico");


            diag.MainInstruction = "Choose a new status for " + SelectedPackage.PackageName + " " + SelectedPackage.VersionName;
            diag.ExpandedControlText = "Hide Package Details";
            diag.CollapsedControlText = "Show Package Details";
            diag.Content =

                "Package Name: " + SelectedPackage.PackageName +
                "\nPackage Code: " + SelectedPackage.PackageCode +
            "\nVersion Number: " + SelectedPackage.VersionName +
            "\nVersion Code: " + SelectedPackage.VersionNumber;




            TaskDialogButton publishedBtn = new TaskDialogButton("Published");
            publishedBtn.CommandLinkNote = "Package version will be visible to all users. Users that have package installed will automatically be upgraded to the latest published version of the package.";

            TaskDialogButton unpublishedBtn = new TaskDialogButton("Unpublished");
            unpublishedBtn.CommandLinkNote = "Package version will not be visible to users by default, but can be seen by enabling the display of unpublished versions. Users that do not already have the package installed will need to manually install or upgrade to an unpublished package version.";

            TaskDialogButton suspendedBtn = new TaskDialogButton("Suspended");
            suspendedBtn.CommandLinkNote = "Package version will not be available for installation or upgrade. Users with suspended package versions installed will automatically be upgraded or downgraded to the latest published version. If no versions are published the suspended package will be uninstalled.";


            if (SelectedPackage.Status == VersionStatus.Unpublished)
            {
                unpublishedBtn.Enabled = false;
                unpublishedBtn.Text = unpublishedBtn.Text + " (Current)";
            }
            else if (SelectedPackage.Status == VersionStatus.Published)
            {
                publishedBtn.Enabled = false;
                publishedBtn.Text = publishedBtn.Text + " (Current)";
            }
            else if (SelectedPackage.Status == VersionStatus.Suspended)
            {
                suspendedBtn.Enabled = false;
                suspendedBtn.Text = suspendedBtn.Text + " (Current)";
            }


            TaskDialogButton cancelBtn = new TaskDialogButton("Cancel");
            cancelBtn.ButtonType = ButtonType.Cancel;

            diag.Buttons.Add(publishedBtn);
            diag.Buttons.Add(unpublishedBtn);
            diag.Buttons.Add(suspendedBtn);
            diag.Buttons.Add(cancelBtn);



            var resultButton = diag.ShowDialog(Application.Current.MainWindow);


            if (resultButton.ButtonType == ButtonType.Cancel) return;



            VersionStatus newStatus = VersionStatus.Unpublished;
            VersionStatus oldStatus = SelectedPackage.Status;


            if (resultButton == publishedBtn)
            {
                newStatus = VersionStatus.Published;
            }
            else if (resultButton == unpublishedBtn)
            {
                newStatus = VersionStatus.Unpublished;
            }
            else if (resultButton == suspendedBtn)
            {
                newStatus = VersionStatus.Suspended;
            }




            try
            {
                SelectedPackage.Status = newStatus;
                SelectedPackage.Package.Status = newStatus;
                CurrentLibrary.UpdatePackage(SelectedPackage.Package);
            }
            catch (Exception exp)
            {
                SelectedPackage.Package.Status = oldStatus;

                logger.Error(exp);

                ShowErrorDialog("Could not change package status", exp);
            }


        }






        private bool CanAddPackageFile(object o)
        {
            if (CurrentLibrary != null) return true;
            else return false;
        }


        private void AddPackageFile(object o)
        {
            try
            {
                VistaOpenFileDialog oDiag = new VistaOpenFileDialog();
                oDiag.Filter = "Revit Add-In Package (*.rpk)|*.rpk";
                oDiag.Title = "Add Package File To Library";

                var res = oDiag.ShowDialog();

                if (res.HasValue && res.Value)
                {
                    try
                    {
                        using (var fs = new FileStream(oDiag.FileName, FileMode.Open, FileAccess.Read, FileShare.None))
                        {
                            CurrentLibrary.AddPackage(fs);
                        }
                    }
                    catch (Exception exp)
                    {
                        logger.Error(exp);
                        ShowErrorDialog("Could not add package file to library", exp);
                    }

                    RefreshPackageList();
                }

            }
            catch (Exception exp)
            {

                ShowErrorDialog("There was a problem adding the package file", exp);

                logger.Error(exp);

                return;
            }
        }



        private void NewPackage(object o)
        {



            try
            {
                IEnumerable<Package> templatePackages;


                bool libAvailble;

                if (CurrentLibrary != null)
                {

                    libAvailble = true;
                    var allPackages = this.CurrentLibrary.GetPackages();

                    templatePackages = from p in allPackages
                                       group p by p.Package.PackageCode into grp
                                       let latestVersionCode = grp.Max(g => g.Package.VersionName)
                                       from p in grp
                                       where p.Package.VersionName == latestVersionCode
                                       select p.Package;
                }
                else
                {
                    libAvailble = false;
                    templatePackages = null;
                    templatePackages = new List<Package>();
                }




                NewPackageWizardWindow wizardWindow = new NewPackageWizardWindow(templatePackages, this.CurrentLibrary);


                wizardWindow.Owner = Application.Current.MainWindow;
                var res = wizardWindow.ShowDialog();


                if (!(res.HasValue && res.Value)) return;


                string packageCode = wizardWindow.ViewModel.NewReleaseInfoPageViewModel.PackageCode;
                string versionNumber = wizardWindow.ViewModel.NewReleaseInfoPageViewModel.VersionName;
                int versionCode = wizardWindow.ViewModel.NewReleaseInfoPageViewModel.VersionNumber;
                string packageName = wizardWindow.ViewModel.NewReleaseInfoPageViewModel.PackageName;
                int minRelease = wizardWindow.ViewModel.NewReleaseInfoPageViewModel.MinRevitRelease;
                int? maxRelease = wizardWindow.ViewModel.NewReleaseInfoPageViewModel.MaxRevitRelease;
                string notes = wizardWindow.ViewModel.NewReleaseInfoPageViewModel.VersionNotes;


                string sourceFolder = wizardWindow.ViewModel.NewReleaseFilesPageViewModel.FilesSourceFolder;





                string packageNotes = wizardWindow.ViewModel.NewReleaseInfoPageViewModel.PackageNotes;
                string versionNotes = wizardWindow.ViewModel.NewReleaseInfoPageViewModel.VersionNotes;
                string author = wizardWindow.ViewModel.NewReleaseInfoPageViewModel.Author;
                string email = wizardWindow.ViewModel.NewReleaseInfoPageViewModel.Email;
                Uri link = wizardWindow.ViewModel.NewReleaseInfoPageViewModel.Link;


                Package newPackage = new Package(packageCode, packageName, packageNotes, versionNumber, versionCode, versionNotes, minRelease, maxRelease, author, email, link);


                if (wizardWindow.ViewModel.NewReleaseSummaryPageViewModel.DestinationFile)
                {

                    VistaSaveFileDialog sDiag = new VistaSaveFileDialog();
                    sDiag.Title = "Save Package File";
                    sDiag.Filter = "Revit Add-In Package (*.rpk)|*.rpk";
                    sDiag.FileName = packageCode + "_" + versionNumber + ".rpk";
                    sDiag.OverwritePrompt = true;

                    var sDiagRes = sDiag.ShowDialog();

                    if (!(sDiagRes.HasValue && sDiagRes.Value)) return;



                    string packageFileName = sDiag.FileName;

                    using (var fs = new FileStream(packageFileName, FileMode.Create, FileAccess.ReadWrite, FileShare.None))
                    {
                        Package.BuildPackage(fs, sourceFolder, newPackage);
                    }
                }
                else if (wizardWindow.ViewModel.NewReleaseSummaryPageViewModel.DestinationLibrary)
                {
                    using (var ms = new MemoryStream())
                    {
                        Package.BuildPackage(ms, sourceFolder, newPackage);

                        ms.Position = 0;

                        CurrentLibrary.AddPackage(ms);
                    }

                    RefreshPackageList();
                }







            }
            catch (Exception exp)
            {

                ShowErrorDialog("There was a problem creating the new package", exp);
                logger.Error(exp);

                return;
            }

        }




        private void DeletePackage(object o)
        {

            Ookii.Dialogs.Wpf.TaskDialog diag = new Ookii.Dialogs.Wpf.TaskDialog();

            TaskDialogButton yesButton = new TaskDialogButton(ButtonType.Yes);
            TaskDialogButton cancelButton = new TaskDialogButton(ButtonType.Cancel);

            diag.Buttons.Add(yesButton);
            diag.Buttons.Add(cancelButton);

            cancelButton.Default = true;
            diag.WindowTitle = "Package Delete Confirmation";
            diag.MainInstruction = "Are you sure you want to permanently delete the package from the library?";

            diag.MainIcon = TaskDialogIcon.Warning;


            diag.Content = "Package Name: " + SelectedPackage.PackageName +
            "\nPackage Code: " + SelectedPackage.PackageCode +
            "\nVersion Number: " + SelectedPackage.VersionName +
            "\nVersion Code: " + SelectedPackage.VersionNumber;


            var res = diag.ShowDialog();

            try
            {
                if (res == yesButton)
                {

                    CurrentLibrary.DeletePackage(SelectedPackage.PackageCode, SelectedPackage.VersionName);
                    RefreshPackageList();
                }
            }
            catch (Exception exp)
            {
                ShowErrorDialog("There was a problem creating the new library", exp);
                logger.Error(exp);

                return;
            }



        }

        private void SetConfigLibrary(string library)
        {
            ConfigurationManager.AppSettings.Remove("Library");
            ConfigurationManager.AppSettings.Add("Library", library);
        }

        private void NewLibrary(object o)
        {
            try
            {

                Ookii.Dialogs.Wpf.VistaFolderBrowserDialog pathDiag = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();

                pathDiag.Description = "Select New Library Folder";
                pathDiag.UseDescriptionForTitle = true;
                pathDiag.ShowNewFolderButton = true;

                bool? res = pathDiag.ShowDialog();

                if (!(res.HasValue && res.Value)) return;





                RevitAddInLibrary.FolderPackageLibrary.CreateNew(pathDiag.SelectedPath);



            }
            catch (Exception exp)
            {

                ShowErrorDialog("There was a problem creating the new library", exp);
                logger.Error(exp);

                return;
            }


        }


        private void OpenLibrary(object o)
        {
            try
            {
                Ookii.Dialogs.Wpf.VistaFolderBrowserDialog pathDiag = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();

                pathDiag.Description = "Select Library Folder";
                pathDiag.UseDescriptionForTitle = true;
                pathDiag.ShowNewFolderButton = true;

                bool? res = pathDiag.ShowDialog();

                if (!(res.HasValue && res.Value)) return;




                TryOpenLibrary(pathDiag.SelectedPath);


                if (CurrentLibrary != null)
                {

                    Ookii.Dialogs.Wpf.TaskDialog d = new Ookii.Dialogs.Wpf.TaskDialog();

                    d.WindowTitle = "Open Library";
                    d.MainInstruction = "Would you like to open this library by default when starting the Library Manager?";

                    TaskDialogButton yesButton = new TaskDialogButton(ButtonType.Yes);
                    TaskDialogButton noButton = new TaskDialogButton(ButtonType.No);

                    d.Buttons.Add(yesButton);
                    d.Buttons.Add(noButton);

                    d.MainIcon = TaskDialogIcon.Information;

                    d.ExpandedByDefault = true;
                    d.Content = pathDiag.SelectedPath;

                    var createLibDiagRes = d.ShowDialog();

                    if (createLibDiagRes == yesButton)
                    {
                        SetConfigLibrary(pathDiag.SelectedPath);
                    }

                }


            }
            catch (Exception exp)
            {

                ShowErrorDialog("There was a problem setting the library", exp);
                logger.Error(exp);

                return;
            }
        }



        //sets the current library to the specified path

        /// <summary>
        /// Attempts to open the specified library, shows an error dialog if there is a problem
        /// </summary>
        /// <param name="libraryDirectory"></param>
        /// <exception cref="FileNotFoundException">When library file is not found or valid</exception>
        private void TryOpenLibrary(string libraryDirectory)
        {
            try
            {
                CurrentLibrary = new FolderPackageLibrary(libraryDirectory, false);
                this.WindowTitle = windowTitle + " - " + libraryDirectory;

                RefreshPackageList();
            }
            catch (Exception exp)
            {
                this.CurrentLibrary = null;
                RefreshPackageList();

                logger.Error(exp);
                ShowErrorDialog("There was a problem opening a library from the folder\n\n" + libraryDirectory, exp);
            }
        }




        private static void ShowErrorDialog(string message, Exception exp)
        {
            Ookii.Dialogs.Wpf.TaskDialog diag = new Ookii.Dialogs.Wpf.TaskDialog();

            TaskDialogButton okButton = new TaskDialogButton(ButtonType.Ok);


            diag.MainIcon = TaskDialogIcon.Error;
            diag.Buttons.Add(okButton);

            diag.MainInstruction = message;

            diag.ExpandFooterArea = true;
            diag.ExpandedByDefault = false;

            diag.ExpandedInformation = exp.Message;


            diag.ShowDialog();
        }










        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion


    }
}


