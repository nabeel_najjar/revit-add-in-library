﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace RevitAddInLibrary.Tests
{
    [TestClass]
    public class RequiredPackageTests
    {
        [TestMethod]
        public void RequiredPackageConstructor()
        {
            //Assemble
            int release = 2013;
            string packageCode = "company.toolname";

            //act
            RevitAddInLibrary.RequiredPackage reqPack = new RequiredPackage(release, packageCode);


            //assert
            Assert.AreEqual(release, reqPack.RevitRelease);
            Assert.AreEqual(packageCode, reqPack.PackageCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RequiredPackageConstructor_ArgumentNullException()
        {
            //Assemble
            int release = 2013;

            //act
            RevitAddInLibrary.RequiredPackage reqPack = new RequiredPackage(release, null);
        }






        [TestMethod]
        public void RequiredPackageConstructor_WriteRead_ReqPackageList()
        {
            //Assemble
            RequiredPackage reqPackage1 = new RequiredPackage(2013, "som.activitylog");
            RequiredPackage reqPackage2 = new RequiredPackage(2014, "som.viewowner");
            RequiredPackage reqPackage3 = new RequiredPackage(2015, "som.viewowner");

            List<RequiredPackage> writeList = new List<RequiredPackage>();
            writeList.Add(reqPackage1);
            writeList.Add(reqPackage2);
            writeList.Add(reqPackage3);


            var tempFile = System.IO.Path.GetTempFileName();

            try
            {
                //act
                RequiredPackage.WriteRequiredPackageList(tempFile, writeList);

                var readList = RequiredPackage.ReadRequiredPackageList(tempFile);


                //assert
                Assert.AreEqual(readList.Count, writeList.Count);

                Assert.AreEqual(readList[0].PackageCode, writeList[0].PackageCode);
                Assert.AreEqual(readList[0].RevitRelease, writeList[0].RevitRelease);

                Assert.AreEqual(readList[1].PackageCode, writeList[1].PackageCode);
                Assert.AreEqual(readList[1].RevitRelease, writeList[1].RevitRelease);

                Assert.AreEqual(readList[2].PackageCode, writeList[2].PackageCode);
                Assert.AreEqual(readList[2].RevitRelease, writeList[2].RevitRelease);

            }
            finally
            {
                if (System.IO.File.Exists(tempFile)) System.IO.File.Delete(tempFile);
            }


        }




    }
}
