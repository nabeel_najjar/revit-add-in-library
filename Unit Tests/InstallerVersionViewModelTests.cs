﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RevitAddInLibrary.Installer;

namespace RevitAddInLibrary.Tests
{
    [TestClass]
    public class InstallerPackageVersionViewModelTests
    {


        [TestMethod]
        public void ConstructorTest()
        {
            string versionName = "1.0.0";
            string versionNotes = "some notes";
            string packageCode = "som.testpackage";
            string packageName = "Test Package";
            int versionNumber = 4;
            VMVersionStatus status = VMVersionStatus.Unpublished;
            int targetRelease = 2013;

            Package pack = new Package(packageCode, packageName, null, versionName, versionNumber, versionNotes, 2012, 2013, null, null, null);


            PackageVersionViewModel vm = new PackageVersionViewModel(pack, targetRelease, status);


            Assert.IsFalse(vm.IsInstalled);
           
            Assert.AreEqual(packageCode,vm.InstallParameter.Package.PackageCode);
            Assert.AreEqual(targetRelease, vm.InstallParameter.TargetRevitRelease);
            Assert.AreEqual(versionName, vm.InstallParameter.Package.VersionName);

            Assert.AreEqual(status, vm.Status);
            Assert.AreEqual(pack, vm.Package);
            

            //marks version as installed
            vm.IsInstalled = true;

            //Checks the IsInstalled and button text updates
            Assert.IsTrue(vm.IsInstalled);
      
        }


        [TestMethod]
    [ExpectedException(typeof(ArgumentNullException))]
        public void Ctor_NullPackage_ArgNullExp()
        {
    
            VMVersionStatus status = VMVersionStatus.Unpublished;
            int targetRelease = 2013;

       

            PackageVersionViewModel vm = new PackageVersionViewModel(null, targetRelease, status);


 
        }






        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TargetReleaseOutPackageRange_ArgumentOutOfRangeException()
        {

            string versionName = "1.0.0";
            string versionNotes = "some notes";
            string packageCode = "som.testpackage";
            string packageName = "Test Package";
            int versionNumber = 4;
            VMVersionStatus status = VMVersionStatus.Unpublished;
            int targetRelease = 2014;

            Package pack = new Package(packageCode, packageName, null, versionName, versionNumber, versionNotes, 2012, 2013, null, null, null);

            PackageVersionViewModel vm = new PackageVersionViewModel(pack, targetRelease, status);

            Assert.IsFalse(vm.IsInstalled);

            Assert.AreEqual(packageCode, vm.InstallParameter.Package.PackageCode);
            Assert.AreEqual(targetRelease, vm.InstallParameter.TargetRevitRelease);
            Assert.AreEqual(versionName, vm.InstallParameter.Package.VersionName);

            Assert.AreEqual(status, vm.Status);
            Assert.AreEqual(pack, vm.Package);

            //marks version as installed
            vm.IsInstalled = true;

            //Checks the IsInstalled and button text updates
            Assert.IsTrue(vm.IsInstalled);
 
        }








    }
}

