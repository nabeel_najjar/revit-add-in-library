﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RevitAddInLibrary.Tests
{
    [TestClass]
    public class GenerateTestPackageFiles
    {

        string packageCode = "com.acme.testpackage";
        string packageName = "Test Package";
        string packageNotes = "Package notes";
        string author = "Author";
        string email = "email@acme.com";
        Uri link = new Uri(@"http://www.acme.com");
        string versionNotes = "Version Notes";


        [TestMethod]
        public void GenerateFiles()
        {
            

            var executingFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string sourceFolder = System.IO.Path.Combine(executingFolder, "SamplePackageSource");
            string outputFolder = System.IO.Path.Combine(executingFolder, "Sample Packages");

            System.IO.Directory.CreateDirectory(outputFolder);


            

            CreateTestPackage(sourceFolder, outputFolder, "1.0.0", 1, 2013, 2015);
            CreateTestPackage(sourceFolder, outputFolder, "2.0.0", 2, 2013, 2015);
            CreateTestPackage(sourceFolder, outputFolder, "3.0.0", 3, 2013, null);
            CreateTestPackage(sourceFolder, outputFolder, "4.0.0", 4, 2013, null);








        }

        private void CreateTestPackage(string sourceFolder, string outputFolder, string versionName, int versionNumber, int minRel, int? maxRel)
        {

            var file1Path = System.IO.Path.Combine(outputFolder, "com.acme.testpackage_" + versionName + ".rpk");

            if (System.IO.File.Exists(file1Path)) System.IO.File.Delete(file1Path);
            using (var fs = new System.IO.FileStream(file1Path, System.IO.FileMode.CreateNew, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None))
            {
                Package p = new Package(packageCode, packageName, packageNotes, versionName, versionNumber, versionNotes, minRel, maxRel, author, email, link);
                Package.BuildPackage(fs, sourceFolder, p);
            }
        }
    }
}

