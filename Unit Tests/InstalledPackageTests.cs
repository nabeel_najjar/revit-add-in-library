﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RevitAddInLibrary.Tests
{
    [TestClass]
    public class InstalledPackageTests
    {

        
        Package package = new Package("company.toolname",
"Package Name",
"Package Note",
"1.0.0",
10,
"Version Notes",
2013,
null,
"Author",
"email@email.com",
new Uri("http://www.google.com"));


        

        DateTime installedON = DateTime.Now.ToUniversalTime();
        

        [TestMethod]
        public void Constructor()
        {

            

            InstalledPackage insPack = new InstalledPackage(package,2013, installedON);

            Assert.AreEqual(package, insPack.Package);
            Assert.AreEqual(installedON, insPack.InstalledOnUTC);
            Assert.AreEqual(2013, insPack.RevitRelease);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_NullPackage_ArgumentNullException()
        {
            InstalledPackage insPack = new InstalledPackage(null, 2013, installedON);
        }



        

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_NonUTCInstalledOn_ArgumentException()
        {
            var localTime = DateTime.Now.ToLocalTime();

            InstalledPackage insPack = new InstalledPackage(package, 2013, localTime);
        }



        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_NegRevitRelease_ArgumentException()
        {
            var localTime = DateTime.Now.ToLocalTime();

            InstalledPackage insPack = new InstalledPackage(package, -1, localTime);
        }

    }
}
