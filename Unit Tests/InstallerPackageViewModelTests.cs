﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RevitAddInLibrary.Installer;
using System.Collections.Generic;
using System.Linq;

namespace RevitAddInLibrary.Tests
{
    [TestClass]
    public class InstallerPackageViewModelTests
    {



        Uri link = new Uri(@"http://www.google.com");
        string packageCode = "som.testpackagecode";

        Package p1;
        Package p2;
        Package p3;
        Package p4;

        LibraryPackage libP1;
        LibraryPackage libP2;
        LibraryPackage libP3;
        LibraryPackage libP4;


        [TestInitialize]
        public void InitTest()
        {

            p1 = new Package(packageCode, "Package Name 1", "Package Notes 1", "1.0.0", 1, "Version Notes 1", 2012, 2013, "Author 1", "email1@acme.com", new Uri(@"http://www.link1.com"));
            p2 = new Package(packageCode, "Package Name 2", "Package Notes 2", "2.0.0", 2, "Version Notes 2", 2012, 2013, "Author 2", "email2@acme.com", new Uri(@"http://www.link2.com"));
            p3 = new Package(packageCode, "Package Name 3", "Package Notes 3", "3.0.0", 3, "Version Notes 3", 2012, null, "Author 3", "email3@acme.com", new Uri(@"http://www.link3.com"));
            p4 = new Package(packageCode, "Package Name 4", "Package Notes 4", "4.0.0", 3, "Version Notes 4", 2012, null, "Author 4", "email4@acme.com", new Uri(@"http://www.link4.com"));

            libP1 = new LibraryPackage(p1, VersionStatus.Published, "user 1", DateTime.Now.ToUniversalTime(), 123);
            libP2 = new LibraryPackage(p2, VersionStatus.Published, "user 2", DateTime.Now.ToUniversalTime(), 123);
            libP3 = new LibraryPackage(p3, VersionStatus.Unpublished, "user 3", DateTime.Now.ToUniversalTime(), 123);
            libP4 = new LibraryPackage(p4, VersionStatus.Suspended, "user 4", DateTime.Now.ToUniversalTime(), 123);


        }


        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void NotInLibrary_SetInstalledWrongPackageCode_InvalidOpExp()
        {
            //create package view model
            PackageViewModel packVM = new PackageViewModel(packageCode, 2013, p1, true);

            Package wrongPCode = new Package("wrong.code", "name", "notes", "1.0.0", 1, "notes", 2012, 2013, "auth", "mail@mail.com", new Uri(@"http://www.google.com"));
            packVM.SetInstalled(wrongPCode);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void SetInstalled_TargetLessThenSupported_ArgOutOfRangeExp()
        {
            //create package view model
            PackageViewModel packVM = new PackageViewModel(packageCode, 2013, p1, true);

            Assert.AreEqual(2013, packVM.TargetRelease);

            Package wrongPCode = new Package(packageCode, "name", "notes", "1.0.0", 1, "notes", 2014, 2015, "auth", "mail@mail.com", new Uri(@"http://www.google.com"));
            packVM.SetInstalled(wrongPCode);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void SetInstalled_TargetGreaterThanSupported_ArgOutOfRangeExp()
        {
            //create package view model
            PackageViewModel packVM = new PackageViewModel(packageCode, 2013, p1, true);

            Assert.AreEqual(2013, packVM.TargetRelease);

            Package wrongPCode = new Package(packageCode, "name", "notes", "1.0.0", 1, "notes", 2011, 2011, "auth", "mail@mail.com", new Uri(@"http://www.google.com"));
            packVM.SetInstalled(wrongPCode);
        }






        [TestMethod]
        public void SetInstalledInLibrary_InstallUnknownVersion()
        {

            //if there are no published versions do not show install, upgrade, or downgrade button

            List<LibraryPackage> libPacks = new List<LibraryPackage>();


            libPacks.Add(libP1);
            libPacks.Add(libP2);

            int targetRelease = 2013;



            //create package view model
            PackageViewModel packVM = new PackageViewModel(packageCode, targetRelease, libPacks, false);


            //assert Immutable properties
            Assert.AreEqual(packageCode, packVM.PackageCode);
            Assert.IsFalse(packVM.IsRequired);
            Assert.AreEqual(targetRelease, packVM.TargetRelease);
            Assert.AreEqual(p2, packVM.OfficialLibraryVersion.Package);
            Assert.AreEqual(VMVersionStatus.Published, packVM.PackageStatus);
            Assert.AreEqual(VMVersionStatus.Published, packVM.DisplayStatus);

            //check package versions view models
            Assert.AreEqual(2, packVM.PackageVersions.Count);

            var p1VM = packVM.PackageVersions.Where(p => p.Package.VersionName == p1.VersionName).First();
            var p2VM = packVM.PackageVersions.Where(p => p.Package.VersionName == p2.VersionName).First();


            Assert.AreEqual(targetRelease, packVM.PackageVersions[0].InstallParameter.TargetRevitRelease);
            Assert.AreEqual(targetRelease, packVM.PackageVersions[1].InstallParameter.TargetRevitRelease);
            Assert.AreEqual(packageCode, packVM.PackageVersions[0].InstallParameter.Package.PackageCode);
            Assert.AreEqual(packageCode, packVM.PackageVersions[1].InstallParameter.Package.PackageCode);

            Assert.AreEqual(p1.VersionName, p1VM.Package.VersionName);
            Assert.AreEqual(p2.VersionName, p2VM.Package.VersionName);

            Assert.AreEqual(VMVersionStatus.Published, p1VM.Status);
            Assert.AreEqual(VMVersionStatus.Published, p2VM.Status);

            Assert.IsFalse(p1VM.IsInstalled);
            Assert.IsFalse(p2VM.IsInstalled);


            //create new unknown package
            Package newPackage = new Package(packageCode, "New package Name,", "new package notes", "5.0.0", 5, "version notes", 2012, null, "auth", "email@email.com", new Uri(@"http://www.google.com"));




            //install the new package
            packVM.SetInstalled(newPackage);


            //Assert
            Assert.AreEqual(3, packVM.PackageVersions.Count);
            var p5VM = packVM.PackageVersions.Where(p => p.Package.VersionName == newPackage.VersionName).First();

            Assert.IsFalse(p1VM.IsInstalled);
            Assert.IsFalse(p2VM.IsInstalled);
            Assert.IsTrue(p5VM.IsInstalled);
            Assert.AreEqual(VMVersionStatus.Unknown, p5VM.Status);
            Assert.AreEqual(newPackage, p5VM.Package);
            Assert.AreEqual(packVM.InstalledVersion, p5VM);

            Assert.IsTrue(packVM.IsInstalled);
            Assert.IsTrue(packVM.ShowInstallButton);
            Assert.IsTrue(packVM.ShowUninstallButton);
            Assert.AreEqual(InstallButtonType.Downgrade, packVM.InstallButtonType);

            Assert.AreEqual(VMVersionStatus.Unknown, packVM.DisplayStatus);


            //Uninstall
            packVM.SetInstalled(null);

            Assert.AreEqual(2, packVM.PackageVersions.Count);
            Assert.IsNull(packVM.InstalledVersion);
            Assert.IsFalse(packVM.IsInstalled);
            Assert.AreEqual(VMVersionStatus.Published, packVM.DisplayStatus);

        }


        [TestMethod]
        public void SetInstalledInLibrary_UninstallSuspended()
        {
            //if there are no published versions do not show install, upgrade, or downgrade button

            List<LibraryPackage> libPacks = new List<LibraryPackage>();


            libPacks.Add(libP3); //unpublished
            libPacks.Add(libP4); //suspsendd


            int targetRelease = 2013;



            //create package view model
            PackageViewModel packVM = new PackageViewModel(packageCode, targetRelease, libPacks, false);


            //assert Immutable properties
            Assert.AreEqual(packageCode, packVM.PackageCode);
            Assert.IsFalse(packVM.IsRequired);
            Assert.AreEqual(targetRelease, packVM.TargetRelease);
            Assert.AreEqual(p3, packVM.OfficialLibraryVersion.Package);
            Assert.AreEqual(VMVersionStatus.Unpublished, packVM.PackageStatus);

            Assert.AreEqual(VMVersionStatus.Unpublished, packVM.DisplayStatus);



            Assert.AreEqual(VMVersionStatus.Unpublished, packVM.DisplayStatus);


            //Assert package versions view models
            Assert.AreEqual(2, packVM.PackageVersions.Count);

            var p3VM = packVM.PackageVersions.Where(p => p.Package.VersionName == p3.VersionName).First();
            var p4VM = packVM.PackageVersions.Where(p => p.Package.VersionName == p4.VersionName).First();


            Assert.AreEqual(targetRelease, packVM.PackageVersions[0].InstallParameter.TargetRevitRelease);
            Assert.AreEqual(targetRelease, packVM.PackageVersions[1].InstallParameter.TargetRevitRelease);
            Assert.AreEqual(packageCode, packVM.PackageVersions[0].InstallParameter.Package.PackageCode);
            Assert.AreEqual(packageCode, packVM.PackageVersions[1].InstallParameter.Package.PackageCode);
            
            Assert.AreEqual(p4.VersionName, p4VM.Package.VersionName);
            Assert.AreEqual(p3.VersionName, p3VM.Package.VersionName);

            Assert.AreEqual(VMVersionStatus.Unpublished, p3VM.Status);
            Assert.AreEqual(VMVersionStatus.Suspended, p4VM.Status);

            Assert.IsFalse(p3VM.IsInstalled);
            Assert.IsFalse(p4VM.IsInstalled);


            //installs v4
            packVM.SetInstalled(p4);

            Assert.IsTrue(packVM.ShowInstallButton);
            Assert.IsTrue(packVM.ShowUninstallButton);

            Assert.IsTrue(packVM.IsInstalled);
            Assert.AreEqual(p4VM, packVM.InstalledVersion);

            Assert.IsTrue(p4VM.IsInstalled);
            Assert.IsFalse(p3VM.IsInstalled);


            Assert.AreEqual(VMVersionStatus.Suspended, packVM.DisplayStatus);



            //installs v3
            packVM.SetInstalled(p3);

            Assert.IsFalse(packVM.ShowInstallButton);
            Assert.IsTrue(packVM.ShowUninstallButton);

            Assert.IsTrue(packVM.IsInstalled);
            Assert.AreEqual(p3VM, packVM.InstalledVersion);

            Assert.IsFalse(p4VM.IsInstalled);
            Assert.IsTrue(p3VM.IsInstalled);


            Assert.AreEqual(VMVersionStatus.Unpublished, packVM.DisplayStatus);




            //uninstall
            packVM.SetInstalled(null);

            Assert.IsTrue(packVM.ShowInstallButton);
            Assert.IsFalse(packVM.ShowUninstallButton);

            Assert.IsFalse(packVM.IsInstalled);
            Assert.IsNull(packVM.InstalledVersion);

            Assert.IsFalse(p4VM.IsInstalled);
            Assert.IsFalse(p3VM.IsInstalled);

            Assert.AreEqual(VMVersionStatus.Unpublished, packVM.DisplayStatus);

            

        }



        [TestMethod]
        public void SetInstalledInLibrary_InstallKnowVersion()
        {
            //test what happens when a known version is installed and uninstalled


            int targetRelease = 2013;

            List<LibraryPackage> libPacks = new List<LibraryPackage>();

            libPacks.Add(libP1);
            libPacks.Add(libP2);
            libPacks.Add(libP3);
            libPacks.Add(libP4);
            

            //create package view model
            PackageViewModel packVM = new PackageViewModel(packageCode, targetRelease, libPacks, false);



            //assert Immutable properties
            Assert.AreEqual(packageCode, packVM.PackageCode);
            Assert.IsFalse(packVM.IsRequired);
            Assert.AreEqual(targetRelease, packVM.TargetRelease);
            Assert.AreEqual(p2, packVM.OfficialLibraryVersion.Package);
            Assert.AreEqual(VMVersionStatus.Published, packVM.PackageStatus);

            //check meta data is set to official version
            Assert.AreEqual(packVM.OfficialLibraryVersion.Package.PackageName, packVM.PackageName);
            Assert.AreEqual(packVM.OfficialLibraryVersion.Package.Author, packVM.Author);
            Assert.AreEqual(packVM.OfficialLibraryVersion.Package.Email, packVM.Email);
            Assert.AreEqual(packVM.OfficialLibraryVersion.Package.PackageNotes, packVM.PackageNotes);
            Assert.AreEqual(packVM.OfficialLibraryVersion.Package.Link, packVM.Link);


            //check package versions view models
            Assert.AreEqual(4, packVM.PackageVersions.Count);
            var p1VM = packVM.PackageVersions.Where(p => p.Package.VersionName == p1.VersionName).First();
            var p2VM = packVM.PackageVersions.Where(p => p.Package.VersionName == p2.VersionName).First();
            var p3VM = packVM.PackageVersions.Where(p => p.Package.VersionName == p3.VersionName).First();
            var p4VM = packVM.PackageVersions.Where(p => p.Package.VersionName == p4.VersionName).First();


            Assert.AreEqual(targetRelease, packVM.PackageVersions[0].InstallParameter.TargetRevitRelease);
            Assert.AreEqual(targetRelease, packVM.PackageVersions[1].InstallParameter.TargetRevitRelease);
            Assert.AreEqual(targetRelease, packVM.PackageVersions[2].InstallParameter.TargetRevitRelease);
            Assert.AreEqual(targetRelease, packVM.PackageVersions[3].InstallParameter.TargetRevitRelease);


            Assert.AreEqual(packageCode, packVM.PackageVersions[0].InstallParameter.Package.PackageCode);
            Assert.AreEqual(packageCode, packVM.PackageVersions[1].InstallParameter.Package.PackageCode);
            Assert.AreEqual(packageCode, packVM.PackageVersions[2].InstallParameter.Package.PackageCode);
            Assert.AreEqual(packageCode, packVM.PackageVersions[3].InstallParameter.Package.PackageCode);


            Assert.AreEqual(p1.VersionName, p1VM.Package.VersionName);
            Assert.AreEqual(p2.VersionName, p2VM.Package.VersionName);
            Assert.AreEqual(p3.VersionName, p3VM.Package.VersionName);
            Assert.AreEqual(p4.VersionName, p4VM.Package.VersionName);

            Assert.AreEqual(VMVersionStatus.Published, p1VM.Status);
            Assert.AreEqual(VMVersionStatus.Published, p2VM.Status);
            Assert.AreEqual(VMVersionStatus.Unpublished, p3VM.Status);
            Assert.AreEqual(VMVersionStatus.Suspended, p4VM.Status);

            Assert.IsFalse(p1VM.IsInstalled);
            Assert.IsFalse(p2VM.IsInstalled);
            Assert.IsFalse(p3VM.IsInstalled);
            Assert.IsFalse(p4VM.IsInstalled);



            //assert installation properties
            Assert.IsNull(packVM.InstalledVersion);
            Assert.IsFalse(packVM.IsInstalled);
            Assert.AreEqual(VMVersionStatus.Published, packVM.DisplayStatus);




            //UI Properties
            Assert.IsTrue(packVM.ShowInstallButton);
            Assert.IsFalse(packVM.ShowUninstallButton);
            Assert.AreEqual(InstallButtonType.Install, packVM.InstallButtonType);



            //Installs Official version
            packVM.SetInstalled(p2);

            //Assert
            Assert.IsFalse(packVM.ShowInstallButton);
            Assert.IsTrue(packVM.ShowUninstallButton);
            
            Assert.IsTrue(packVM.IsInstalled);
            Assert.AreEqual(p2VM, packVM.InstalledVersion);
            Assert.IsTrue(p2VM.IsInstalled);
            Assert.AreEqual(VMVersionStatus.Published, packVM.DisplayStatus);



            //Installs Unpublished Version version
            packVM.SetInstalled(p3);

            //Assert
            Assert.IsTrue(packVM.ShowInstallButton);
            Assert.IsTrue(packVM.ShowUninstallButton);
            Assert.AreEqual(InstallButtonType.Downgrade, packVM.InstallButtonType);
            Assert.IsTrue(packVM.IsInstalled);
            Assert.AreEqual(p3VM, packVM.InstalledVersion);
            Assert.IsTrue(p3VM.IsInstalled);
            Assert.IsFalse(p2VM.IsInstalled);
            Assert.AreEqual(VMVersionStatus.Unpublished, packVM.DisplayStatus);
            



            //Installs Unpublished Version version
            packVM.SetInstalled(p1);

            //Assert
            Assert.IsTrue(packVM.ShowInstallButton);
            Assert.IsTrue(packVM.ShowUninstallButton);
            Assert.AreEqual(InstallButtonType.Update, packVM.InstallButtonType);
            Assert.IsTrue(packVM.IsInstalled);
            Assert.AreEqual(p1VM, packVM.InstalledVersion);
            Assert.IsTrue(p1VM.IsInstalled);
            Assert.IsFalse(p3VM.IsInstalled);
            Assert.AreEqual(VMVersionStatus.Published, packVM.DisplayStatus);




            //Uninstalled
            packVM.SetInstalled(null);



            //Assert
            Assert.IsTrue(packVM.ShowInstallButton);
            Assert.IsFalse(packVM.ShowUninstallButton);
            Assert.AreEqual(InstallButtonType.Install, packVM.InstallButtonType);
            Assert.IsFalse(packVM.IsInstalled);
            Assert.IsNull(packVM.InstalledVersion);
            Assert.IsFalse(p1VM.IsInstalled);
            Assert.IsFalse(p2VM.IsInstalled);
            Assert.IsFalse(p3VM.IsInstalled);
            Assert.IsFalse(p4VM.IsInstalled);
            Assert.AreEqual(VMVersionStatus.Published, packVM.DisplayStatus);







            

   


        }










        [TestMethod]
        public void SetInstalledNotInLibrary()
        {
            int targetRelease = 2013;

            //create package view model
            PackageViewModel packVM = new PackageViewModel(packageCode, targetRelease, p1, false);

            //assert Immutable properties
            Assert.AreEqual(p1.PackageCode, packVM.PackageCode);
            Assert.IsFalse(packVM.IsRequired);
            Assert.AreEqual(targetRelease, packVM.TargetRelease);
            Assert.IsNull(packVM.OfficialLibraryVersion);
            Assert.AreEqual(VMVersionStatus.Unknown, packVM.PackageStatus);
            Assert.AreEqual(VMVersionStatus.Unknown, packVM.DisplayStatus);


            //Assert Metadata properties
            Assert.AreEqual(p1.PackageName, packVM.PackageName);
            Assert.AreEqual(p1.Author, packVM.Author);
            Assert.AreEqual(p1.Email, packVM.Email);
            Assert.AreEqual(p1.PackageNotes, packVM.PackageNotes);
            Assert.AreEqual(p1.Link, packVM.Link);

            //assert installation properties
            Assert.AreEqual(p1, packVM.InstalledVersion.Package);
            Assert.AreEqual(true, packVM.IsInstalled);
            Assert.AreEqual(1, packVM.PackageVersions.Count);
            Assert.AreEqual(p1, packVM.PackageVersions[0].Package);
            Assert.IsTrue(packVM.PackageVersions[0].IsInstalled);
            Assert.AreEqual(VMVersionStatus.Unknown, packVM.PackageVersions[0].Status);

            //UI Properties
            Assert.IsFalse(packVM.ShowInstallButton);
            Assert.IsTrue(packVM.ShowUninstallButton);


            //set installed to p2
            packVM.SetInstalled(p2);


            //Assert Metadata properties
            Assert.AreEqual(p2.PackageName, packVM.PackageName);
            Assert.AreEqual(p2.Author, packVM.Author);
            Assert.AreEqual(p2.Email, packVM.Email);
            Assert.AreEqual(p2.PackageNotes, packVM.PackageNotes);
            Assert.AreEqual(p2.Link, packVM.Link);


            //assert installation properties
            Assert.AreEqual(p2, packVM.InstalledVersion.Package);
            Assert.AreEqual(VMVersionStatus.Unknown, packVM.PackageStatus);

            Assert.AreEqual(VMVersionStatus.Unknown, packVM.DisplayStatus);

            Assert.IsTrue(packVM.IsInstalled);
            Assert.AreEqual(1, packVM.PackageVersions.Count);
            Assert.AreEqual(p2, packVM.PackageVersions[0].Package);
            Assert.IsTrue(packVM.PackageVersions[0].IsInstalled);
            Assert.AreEqual(VMVersionStatus.Unknown, packVM.PackageVersions[0].Status);


            //UI Properties
            Assert.IsFalse(packVM.ShowInstallButton);
            Assert.IsTrue(packVM.ShowUninstallButton);


            //ACT: Set uninstalled
            packVM.SetInstalled(null);


            //ASSERT: Metadata properties are cleared
            Assert.IsNull(packVM.PackageName);
            Assert.IsNull(packVM.Author);
            Assert.IsNull(packVM.Email);
            Assert.IsNull(packVM.PackageNotes);
            Assert.IsNull(packVM.Link);

            //assert installation properties
            Assert.IsNull(packVM.InstalledVersion);
            Assert.IsFalse(packVM.IsInstalled);
            Assert.AreEqual(0, packVM.PackageVersions.Count);

            //UI Properties
            Assert.IsFalse(packVM.ShowInstallButton);
            Assert.IsFalse(packVM.ShowUninstallButton);

        }





        [TestMethod]
        public void InLibrary_OfficialLibraryVersionSetCorrectly()
        {
            int targetRelease = 2013;

            bool required = false;

            List<LibraryPackage> lib1 = new List<LibraryPackage>();
            lib1.Add(libP1);
            lib1.Add(libP2);
            lib1.Add(libP3);
            lib1.Add(libP4);

            List<LibraryPackage> lib2 = new List<LibraryPackage>();
            lib2.Add(libP3);
            lib2.Add(libP4);

            List<LibraryPackage> lib3 = new List<LibraryPackage>();
            lib3.Add(libP4);


            //create package view model
            PackageViewModel vm1 = new PackageViewModel(packageCode, targetRelease, lib1, required);
            PackageViewModel vm2 = new PackageViewModel(packageCode, targetRelease, lib2, required);
            PackageViewModel vm3 = new PackageViewModel(packageCode, targetRelease, lib3, required);

            Assert.IsNotNull(vm1.OfficialLibraryVersion);
            Assert.IsNotNull(vm2.OfficialLibraryVersion);
            Assert.IsNotNull(vm3.OfficialLibraryVersion);

            Assert.AreEqual(p2, vm1.OfficialLibraryVersion.Package);
            Assert.AreEqual(p3, vm2.OfficialLibraryVersion.Package);
            Assert.AreEqual(p4, vm3.OfficialLibraryVersion.Package);
        }










        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CtorInLibrary_nullpackageCode_ArgNullExp()
        {
            List<LibraryPackage> libPackages = new List<LibraryPackage>();
            libPackages.Add(new LibraryPackage(p1, VersionStatus.Unpublished, "added", DateTime.Now.ToUniversalTime(), 123));

            PackageViewModel vm = new PackageViewModel(null, 2013, libPackages, true);
        }



        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CtorInLibrary_NullLibPackages_ArgNullExp()
        {
            PackageViewModel vm = new PackageViewModel(packageCode, 2013, (List<LibraryPackage>)null, true);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CtorInLibrary_EmptyLibPackages_ArgNullExp()
        {
            List<LibraryPackage> lib = new List<LibraryPackage>();

            PackageViewModel vm = new PackageViewModel(packageCode, 2013, lib, true);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CtorInLibrary_LibPackReleaseOver_ArgRangeExp()
        {

            List<LibraryPackage> libPackages = new List<LibraryPackage>();
            libPackages.Add(new LibraryPackage(p1, VersionStatus.Unpublished, "added", DateTime.Now.ToUniversalTime(), 123));

            PackageViewModel vm = new PackageViewModel(packageCode, 2011, libPackages, true);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CtorInLibrary_LibPackReleaseUnder_ArgRangeExp()
        {
            List<LibraryPackage> libPackages = new List<LibraryPackage>();
            libPackages.Add(new LibraryPackage(p1, VersionStatus.Unpublished, "added", DateTime.Now.ToUniversalTime(), 123));

            PackageViewModel vm = new PackageViewModel(packageCode, 2014, libPackages, true);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CtorInLibrary_DuplicateVersions_ArgExp()
        {
            List<LibraryPackage> libPackages = new List<LibraryPackage>();
            libPackages.Add(new LibraryPackage(p1, VersionStatus.Unpublished, "added", DateTime.Now.ToUniversalTime(), 123));
            libPackages.Add(new LibraryPackage(p1, VersionStatus.Unpublished, "added", DateTime.Now.ToUniversalTime(), 123));

            PackageViewModel vm = new PackageViewModel(packageCode, 2013, libPackages, true);
        }




        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CtorInLibrary_LibPackCodeNotMatch_ArgExp()
        {
            List<LibraryPackage> libPackages = new List<LibraryPackage>();
            libPackages.Add(new LibraryPackage(p1, VersionStatus.Unpublished, "added", DateTime.Now.ToUniversalTime(), 123));

            PackageViewModel vm = new PackageViewModel("differentPackageCode", 2013, libPackages, true);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CtorNotInLibrary_NullpackageCode_ArgNullExp()
        {
            PackageViewModel vm = new PackageViewModel(null, 2012, p1, true);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CtorNotInLibrary_PackCodeNotMatch_ArgExp()
        {
            PackageViewModel vm = new PackageViewModel("differentPackageCode", 2013, p1, true);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CtorNotInLibrary_InsPackReleaseOVer_ArgRangeExp()
        {
            PackageViewModel vm = new PackageViewModel(packageCode, 2014, p1, true);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CtorNotInLibrary_InstPackReleaseUnder_ArgRangeExp()
        {
            PackageViewModel vm = new PackageViewModel(packageCode, 2011, p1, true);
        }



        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CtorNotInLibrary_NullInstPackage_ArgNullExp()
        {
            PackageViewModel vm = new PackageViewModel(packageCode, 2014, (Package)null, true);
        }











    }
}






