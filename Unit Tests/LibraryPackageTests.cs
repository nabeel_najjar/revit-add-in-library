﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RevitAddInLibrary.Tests
{
    [TestClass]
    public class LibraryPackageTests
    {


        Package package = new Package("company.toolname",
    "Package Name",
    "Package Note",
    "1.0.0",
    10,
    "Version Notes",
    2013,
    null,
    "Author",
    "email@email.com",
    new Uri("http://www.google.com"));


        VersionStatus status = VersionStatus.Published;


        DateTime addedOn = DateTime.Now.ToUniversalTime();
        long fileSize = 12345;

        string userName = "john.smith";



        [TestMethod]
        public void Constructor()
        {
            LibraryPackage libPack = new LibraryPackage(package, status, userName, addedOn, fileSize);


            Assert.AreEqual(libPack.Package, package);
            Assert.AreEqual(libPack.Status, status);
            Assert.AreEqual(libPack.AddedOnUTC, addedOn);
            Assert.AreEqual(libPack.FileSize, fileSize);
            Assert.AreEqual(libPack.AddedBy, userName);

        }



        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_NullPackageArgumentNullException()
        {
            LibraryPackage libPack = new LibraryPackage(null, status,userName, addedOn, fileSize);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_NullAddedByArgumentNullException()
        {
            LibraryPackage libPack = new LibraryPackage(package, status,null, addedOn, fileSize);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_NonUTCArgumentException()
        {

            DateTime notUTC = DateTime.Now.ToLocalTime();

            LibraryPackage libPack = new LibraryPackage(package, status, userName, notUTC, fileSize);
        }
    }
}
