﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RevitAddInLibrary;
using System.IO;

namespace RevitAddInLibrary.Tests
{
    [TestClass]
    public class PackageTests
    {




        string packageCode = "com.acme.testpackage";
        string packageName = "Test Package";

        string versionName = "1.2.3";

        int versionNumber = 10;

        int minRelease = 2014;
        int maxRelease = 2017;

        string packageNotes = "these are the package notes";
        string versionNotes = "these are the version notes";

        string author = "author value";
        string email = "user@email.com";






        Uri link = new Uri("http://www.google.com");












        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Construtor_MaxRevitReleaseLessThenRevitRelease_InvalidOperationException()
        {
            Package p = new Package(packageCode, packageName, packageNotes, versionName, versionNumber, versionNotes, 2014, 2012, author, email, link);

        }



        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void SetMaxRevitReleaseLessThenMinRevitRelease_InvalidOperationException()
        {
            Package p = new Package(packageCode, packageName, packageNotes, versionName, versionNumber, versionNotes, 2014, null, author, email, link);

            p.MaxRevitRelease = 2013;
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void SetMinRevitReleaseGreaterThenMaxRevitRelease_InvalidOperationException()
        {
            Package p = new Package(packageCode, packageName, packageNotes, versionName, versionNumber, versionNotes, 2014, 2015, author, email, link);

            p.MinRevitRelease = 2016;
        }


        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void SetNameNewLine_InvalidOperationException()
        {
            Package p = new Package(packageCode, packageName, packageNotes, versionName, versionNumber, versionNotes, 2014, null, author, email, link);

            p.PackageName = "name with\n new line";
        }



        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NegRelease_ArgumentException()
        {
            //act
            Package p = new Package(packageCode, packageName, packageNotes, versionName, versionNumber, versionNotes, -10, null, author, email, link);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NegVersionCode_ArgumentException()
        {
            //act
            Package p = new Package(packageCode, packageName, packageNotes, versionName, -10, versionNotes, minRelease, null, author, email, link);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void VersionNumberStartWithWhiteSpace_ArgumentException()
        {
            //act
            Package p = new Package(packageCode, packageName, packageNotes, " 1.0.0", versionNumber, versionNotes, minRelease, null, author, email, link);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void VersionNumberNull_ArgumentNullException()
        {
            //act
            Package p = new Package(packageCode, packageName, packageNotes, null, versionNumber, versionNotes, minRelease, null, author, email, link);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PackageNameNull_ArgumentNullException()
        {
            //act
            Package p = new Package(packageCode, "", packageNotes, versionName, versionNumber, versionNotes, minRelease, null, author, email, link);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PackageCodeCapLetters_ArgumentException()
        {
            //act
            Package p = new Package("CAPITALLETTERS", packageName, packageNotes, versionName, versionNumber, versionNotes, minRelease, null, author, email, link);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PackageCodeSpaces_ArgumentException()
        {
            //act
            Package p = new Package("there are spaces", packageName, packageNotes, versionName, versionNumber, versionNotes, minRelease, null, author, email, link);
        }



        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PackageNameLineBreaks_ArgumentException()
        {
            //act
            Package p = new Package(packageCode, "Package \n Name", packageNotes, versionName, versionNumber, versionNotes, minRelease, null, author, email, link);
        }



        [TestMethod]
        [ExpectedException(typeof(System.IO.DirectoryNotFoundException))]
        public void BuildPackage_SourceFolderMissing_DirectoryNotFoundException()
        {
            //arrange
            var executingFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            string sourceFolder = System.IO.Path.Combine(executingFolder, "THIS FOLDER DOES NOT EXIST");


            Package package = new Package(packageCode, packageName, packageNotes, versionName, versionNumber, versionNotes, minRelease, null, author, email, link);


            var tempPackageFile = System.IO.Path.GetTempFileName();





            try
            {
                //act
                using (System.IO.FileStream fs = new System.IO.FileStream(tempPackageFile, System.IO.FileMode.Open))
                {
                    Package.BuildPackage(fs, sourceFolder, package);
                }
            }
            finally
            {
                if (System.IO.File.Exists(tempPackageFile)) System.IO.File.Delete(tempPackageFile);
            }
        }







        [TestMethod]
        [ExpectedException(typeof(System.IO.FileNotFoundException))]
        public void BuildPackage_AddinFileMIssing_FileNotFoundException()
        {
            //arrange
            var executingFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            string sourceFolder = System.IO.Path.Combine(executingFolder, "SamplePackageSource");

            Package package = new Package("com.doesnotmatchaddin", packageName, packageNotes, versionName, versionNumber, versionNotes, minRelease, null, author, email, link);

            var tempPackageFile = System.IO.Path.GetTempFileName();

            try
            {
                //act
                using (System.IO.FileStream fs = new System.IO.FileStream(tempPackageFile, System.IO.FileMode.Open))
                {
                    Package.BuildPackage(fs, sourceFolder, package);
                }

            }
            finally
            {
                if (System.IO.File.Exists(tempPackageFile)) System.IO.File.Delete(tempPackageFile);
            }
        }


        [TestMethod]
        public void ReadPackage()
        {

            Package readPackage;

            using(var fs = new FileStream("com.acme.testpackage_1.0.0.rpk",FileMode.Open,FileAccess.Read,FileShare.None))
            {
                readPackage = Package.ReadPackage(fs);
            }


            Assert.IsNotNull(readPackage);


            Assert.AreEqual("Test Package", readPackage.PackageName);
            Assert.AreEqual("com.acme.testpackage", readPackage.PackageCode);
            Assert.AreEqual("Package notes", readPackage.PackageNotes);
            Assert.AreEqual("Author", readPackage.Author);
            Assert.AreEqual("email@acme.com", readPackage.Email);
            Assert.AreEqual(new Uri(@"http://www.acme.com/"), readPackage.Link);

            Assert.AreEqual(2013, readPackage.MinRevitRelease);
                        Assert.AreEqual(2015, readPackage.MaxRevitRelease);
            Assert.AreEqual("1.0.0", readPackage.VersionName);
            Assert.AreEqual(1, readPackage.VersionNumber);
            Assert.AreEqual("Version Notes", readPackage.VersionNotes);
        }




        [TestMethod]
        public void BuildPackage_ReadPackage()
        {
            //arrange
            var executingFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            string sourceFolder = System.IO.Path.Combine(executingFolder, "SamplePackageSource");


            Package package = new Package(packageCode, packageName, packageNotes, versionName, versionNumber, versionNotes, minRelease, maxRelease, author, email, link);


            var tempPackageFile = System.IO.Path.GetTempFileName();



            try
            {
                //act
                using (System.IO.FileStream fs = new System.IO.FileStream(tempPackageFile, System.IO.FileMode.Open))
                {
                    Package.BuildPackage(fs, sourceFolder, package);
                }




                Package readPackage = null;

                using (System.IO.FileStream fs = new System.IO.FileStream(tempPackageFile, System.IO.FileMode.Open))
                {
                    readPackage = Package.ReadPackage(fs);
                }


                //assert
                Assert.AreEqual(readPackage.PackageCode, packageCode);
                Assert.AreEqual(readPackage.PackageName, packageName);
                Assert.AreEqual(readPackage.MinRevitRelease, minRelease);
                Assert.AreEqual(readPackage.MaxRevitRelease, maxRelease);
                Assert.AreEqual(readPackage.VersionName, versionName);
                Assert.AreEqual(readPackage.VersionNumber, versionNumber);
                Assert.AreEqual(readPackage.PackageNotes, packageNotes);
                Assert.AreEqual(readPackage.VersionNotes, versionNotes);
                Assert.AreEqual(readPackage.Author, author);
                Assert.AreEqual(readPackage.Email, email);
            }
            finally
            {
                if (System.IO.File.Exists(tempPackageFile)) System.IO.File.Delete(tempPackageFile);
            }

        }





        [TestMethod]
        public void BuildPackageRaw_ReadPackage()
        {
            var executingFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            string sourceFolder = System.IO.Path.Combine(executingFolder, "SampleRawPackageSource");

            using (var ms = new System.IO.MemoryStream())
            {

                //act
                Package.BuildPackage(ms, sourceFolder);



                //assert
                ms.Position = 0;


                var package = Package.ReadPackage(ms);


                Assert.AreEqual(package.PackageName, "Test Package");
                Assert.AreEqual(package.PackageCode, "com.acme.testpackage");
                Assert.AreEqual(package.Author, "Author");
                Assert.AreEqual(package.Email, "email@acme.com");
                Assert.AreEqual(package.Link, "http://www.acme.com/");
                Assert.AreEqual(package.MinRevitRelease, 2013);
                Assert.AreEqual(package.MaxRevitRelease, 2017);
                Assert.AreEqual(package.VersionName, "1.0.0");
                Assert.AreEqual(package.VersionNumber, 1);




            }
        }






        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void BuildPackageRaw_NullDestStream_ArgumentNullException()
        {

            var executingFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            string sourceFolder = System.IO.Path.Combine(executingFolder, "SamplePackageSource");

            Package.BuildPackage(null, sourceFolder);

        }




        [TestMethod]
        [ExpectedException(typeof(System.IO.DirectoryNotFoundException))]
        public void BuildPackageRaw_MissingSourceFolder_DirectoryNotFoundException()
        {

            var executingFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            string sourceFolder = System.IO.Path.Combine(executingFolder, "SamplePackageSource");

            using (var ms = new System.IO.MemoryStream())
            {
                //act
                Package.BuildPackage(ms, @"C:\THIS\FOLDER\DOES\NOT\EXISTS");
            }


        }


        [TestMethod]
        [ExpectedException(typeof(System.IO.FileNotFoundException))]
        public void BuildPackageRaw_MissingManifest_FileNotFoundException()
        {

            var tempPath = System.IO.Path.Combine(System.IO.Path.GetTempPath(), "SampleRawPackageSource");

            try
            {
                //copy
                if (Directory.Exists(tempPath)) Directory.Delete(tempPath, true);

                var executingFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                string sourceFolder = System.IO.Path.Combine(executingFolder, "SampleRawPackageSource");



                //Now Create all of the directories
                foreach (string dirPath in Directory.GetDirectories(sourceFolder, "*", SearchOption.AllDirectories))
                    Directory.CreateDirectory(dirPath.Replace(sourceFolder, tempPath));

                //Copy all the files
                foreach (string newPath in Directory.GetFiles(sourceFolder, "*.*", SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(sourceFolder, tempPath));


                File.Delete(Path.Combine(tempPath, "com.acme.testpackage.addin"));


                using (var ms = new System.IO.MemoryStream())
                {
                    //act
                    Package.BuildPackage(ms, tempPath);
                }



            }
            finally
            {
                if (Directory.Exists(tempPath)) Directory.Delete(tempPath, true);

            }


        }



        [TestMethod]
        [ExpectedException(typeof(System.IO.FileNotFoundException))]
        public void BuildPackageRaw_MissingManifestReferencedFile_FileNotFoundException()
        {

            var tempPath = System.IO.Path.Combine(System.IO.Path.GetTempPath(), "SampleRawPackageSource");

            try
            {
                //copy
                if (Directory.Exists(tempPath)) Directory.Delete(tempPath, true);

                var executingFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                string sourceFolder = System.IO.Path.Combine(executingFolder, "SampleRawPackageSource");



                //Now Create all of the directories
                foreach (string dirPath in Directory.GetDirectories(sourceFolder, "*", SearchOption.AllDirectories))
                    Directory.CreateDirectory(dirPath.Replace(sourceFolder, tempPath));

                //Copy all the files
                foreach (string newPath in Directory.GetFiles(sourceFolder, "*.*", SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(sourceFolder, tempPath));


                File.Delete(Path.Combine(tempPath, "TestPackage.dll"));


                using (var ms = new System.IO.MemoryStream())
                {
                    //act
                    Package.BuildPackage(ms, tempPath);
                }
            }
            finally
            {
                if (Directory.Exists(tempPath)) Directory.Delete(tempPath, true);
            }
        }



        [TestMethod]
        [ExpectedException(typeof(System.IO.FileNotFoundException))]
        public void BuildPackageRaw_NoPackageXML_FileNotFoundException()
        {

            var executingFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            string sourceFolder = System.IO.Path.Combine(executingFolder, "SamplePackageSource");

            using (var ms = new System.IO.MemoryStream())
            {
                //act
                Package.BuildPackage(ms, sourceFolder);
            }
        }

        [TestMethod]
        public void UpdatePackage()
        {
            //test Packkage.Update to update an existing package file
            var tempPackageFilePath = System.IO.Path.GetTempFileName();

            string packageNotesUpdated = "updated package notes";
            string packageNameUpdated = "updated package name";
            Uri packagelinkUpdated = new Uri(@"http://www.updatedlink.com");
            string packageAuthorUpdated = "updated package author";
            string packageEmailUpdated = "updated@packageeamil.com";
            int packageMaxReleaseUpdated = 2017;
            int packageMinReleaseUpdated = 2011;
            string updatedVersionNotes = "updated version notes";

            try
            {
                File.Copy("com.acme.testpackage_1.0.0.rpk", tempPackageFilePath, true);

                Package package;

                using (var fs = new FileStream(tempPackageFilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    package = Package.ReadPackage(fs);
                }

                package.PackageNotes = packageNotesUpdated;
                package.PackageName = packageNameUpdated;
                package.Link = packagelinkUpdated;
                package.Author = packageAuthorUpdated;
                package.Email = packageEmailUpdated;
                package.MaxRevitRelease = packageMaxReleaseUpdated;
                package.MinRevitRelease = packageMinReleaseUpdated;
                package.VersionNotes = updatedVersionNotes;

                using (var fs = new FileStream(tempPackageFilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    Package.UpdatePackage(fs, package);
                }

                //assert that package was updated
                Package readUpdatedPackage;
                using (var fs = new FileStream(tempPackageFilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    readUpdatedPackage = Package.ReadPackage(fs);
                }

                Assert.IsNotNull(readUpdatedPackage);
                Assert.AreEqual(packageNotesUpdated, readUpdatedPackage.PackageNotes);
                Assert.AreEqual(packageNameUpdated, readUpdatedPackage.PackageName);
                Assert.AreEqual(packagelinkUpdated, readUpdatedPackage.Link);
                Assert.AreEqual(packageAuthorUpdated, readUpdatedPackage.Author);
                Assert.AreEqual(updatedVersionNotes, readUpdatedPackage.VersionNotes);
                Assert.AreEqual(packageEmailUpdated, readUpdatedPackage.Email);
                Assert.AreEqual(packageMaxReleaseUpdated, readUpdatedPackage.MaxRevitRelease);
                Assert.AreEqual(packageMinReleaseUpdated, readUpdatedPackage.MinRevitRelease);

            }
            finally
            {
                if (File.Exists(tempPackageFilePath)) File.Delete(tempPackageFilePath);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void UpdatePackage_DifferentVersionNumber_ArgumentException()
        {
            //test Packkage.Update to update an existing package file
            var tempPackageFilePath = System.IO.Path.GetTempFileName();

            try
            {
                File.Copy("com.acme.testpackage_1.0.0.rpk", tempPackageFilePath, true);

                Package p;

                using (var fs = new FileStream(tempPackageFilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    p = Package.ReadPackage(fs);
                }


                Package updatedPackage = new Package(p.PackageCode, p.PackageName, p.PackageNotes, p.VersionName, 1234, p.VersionNotes, p.MinRevitRelease, p.MaxRevitRelease, p.Author, p.Email, p.Link);

                using (var fs = new FileStream(tempPackageFilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    Package.UpdatePackage(fs, updatedPackage);
                }
            }
            finally
            {
                if (File.Exists(tempPackageFilePath)) File.Delete(tempPackageFilePath);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void UpdatePackage_DifferentVersionName_ArgumentException()
        {
            //test Packkage.Update to update an existing package file
            var tempPackageFilePath = System.IO.Path.GetTempFileName();

            try
            {
                File.Copy("com.acme.testpackage_1.0.0.rpk", tempPackageFilePath, true);

                Package p;

                using (var fs = new FileStream(tempPackageFilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    p = Package.ReadPackage(fs);
                }


                Package updatedPackage = new Package(p.PackageCode, p.PackageName, p.PackageNotes, "DIFFERENT VERSION NAME", p.VersionNumber, p.VersionNotes, p.MinRevitRelease, p.MaxRevitRelease, p.Author, p.Email, p.Link);

                using (var fs = new FileStream(tempPackageFilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    Package.UpdatePackage(fs, updatedPackage);
                }
            }
            finally
            {
                if (File.Exists(tempPackageFilePath)) File.Delete(tempPackageFilePath);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void UpdatePackage_DifferentPackageCode_ArgumentException()
        {
            //test Packkage.Update to update an existing package file
            var tempPackageFilePath = System.IO.Path.GetTempFileName();

            try
            {
                File.Copy("com.acme.testpackage_1.0.0.rpk", tempPackageFilePath, true);

                Package p;

                using (var fs = new FileStream(tempPackageFilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    p = Package.ReadPackage(fs);
                }

                Package updatedPackage = new Package("different.packagecode", p.PackageName, p.PackageNotes, p.VersionName, p.VersionNumber, p.VersionNotes, p.MinRevitRelease, p.MaxRevitRelease, p.Author, p.Email, p.Link);

                using (var fs = new FileStream(tempPackageFilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    Package.UpdatePackage(fs, updatedPackage);
                }
            }
            finally
            {
                if (File.Exists(tempPackageFilePath)) File.Delete(tempPackageFilePath);
            }
        }
    }
}






