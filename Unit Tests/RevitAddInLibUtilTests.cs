﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Diagnostics;
using System.Text;

namespace RevitAddInLibrary.Tests
{
    [TestClass]
    public class RevitAddInLibUtilTests
    {
        private string _testLibFolderPath = System.IO.Path.Combine(System.IO.Path.GetTempPath(), "Revit Add-In Library Utility Unit Test Library");
        private string _executingFolder = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        private void InitLib()
        {
            if (Directory.Exists(_testLibFolderPath)) Directory.Delete(_testLibFolderPath,true);
            FolderPackageLibrary.CreateNew(_testLibFolderPath);
        }


        [TestMethod]
        public void AddPackage()
        {
            InitLib();

            string[] args = new string[] { "add-package", "-f", Path.Combine(_executingFolder, "com.acme.testpackage_1.0.0.rpk") };

            var result = RunRvtAddInLibUtil(args);

            //assert
            var lib = new FolderPackageLibrary(_testLibFolderPath, true);
            var p = lib.GetPackage("com.acme.testpackage", "1.0.0");

            Assert.IsNotNull(p);
            Assert.AreEqual("com.acme.testpackage", p.Package.PackageCode);
            Assert.AreEqual("1.0.0", p.Package.VersionName);
        }
        



        [TestMethod]
        public void CreateRevitAddInLibUtilTests()
        {

            

            //create a series of unit tests that test the RvtAddInLibUtil console utlity 

            
            throw new NotImplementedException();
        }



        TextWriter tw;


        private string RunRvtAddInLibUtil(string[] args)
        {
            tw = new StringWriter();
            Console.SetOut(tw);

            var cons = new RevitAddInLibrary.ConsoleUtility.ConsoleUtility();
            cons.Library = _testLibFolderPath;
            cons.Run(args);
                
            return tw.ToString();


            //proc = new Process();
            //proc.StartInfo.FileName = Path.Combine(_executingFolder, "RvtAddInLibUtil.exe");
            //proc.StartInfo.Arguments = args;
            //proc.StartInfo.UseShellExecute = false;
            //proc.StartInfo.RedirectStandardInput = true;
            //proc.StartInfo.RedirectStandardOutput = true;

            //proc.OutputDataReceived += proc_OutputDataReceived;
            //outputSb = new StringBuilder();


            //try
            //{
            //    proc.Start();
            //    proc.WaitForExit();
            //}
            //catch (Exception exp)
            //{

            //    throw;
            //}    
            //finally
            //{
            //    proc.OutputDataReceived -= proc_OutputDataReceived;
            //}


            //if (outputSb != null) return outputSb.ToString();
            //else return null;
        }

        //void proc_OutputDataReceived(object sender, DataReceivedEventArgs e)
        //{
        //    outputSb.AppendLine(e.Data);
        //}
    }
}
