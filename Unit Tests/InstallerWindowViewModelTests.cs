﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Linq;
using RevitAddInLibrary.Installer;

namespace RevitAddInLibrary.Tests
{
    [TestClass]
    public class InstallerWindowViewModelTests
    {
        private Package GetTestPackage(string packageCode, int number, int minRelease, int? maxRelease)
        {
            var p = new Package(
                packageCode,
                "Package Name " + number.ToString(), 
                "Package Notes " + number.ToString(),
                number.ToString()+".0",
                number,
                "Version Notes " + number.ToString(), 
                minRelease, 
                maxRelease,
                "Author " + number.ToString(),
                "email" + number.ToString()+"@acme.com", 
                null
                
                );

            return p;
        }


        private LibraryPackage GetTestLibPackage(string packageCode, int number, int minRelease, int? maxRelease, VersionStatus status)
        {
            return new LibraryPackage(GetTestPackage(packageCode, number, minRelease, maxRelease), status, "Added By " + number.ToString(), DateTime.Now.ToUniversalTime(), 0);
        }

        private InstalledPackage GetTestInstalledPackage(string packageCode, int number, int minRelease, int? maxRelease, int targetRelease)
        {
            return new InstalledPackage(GetTestPackage(packageCode, number, minRelease, maxRelease), targetRelease, DateTime.Now.ToUniversalTime());
        }


        [TestMethod]
        public void InstallCommand()
        {
            var libPackage = GetTestLibPackage("packagecode", 1, 2013, null, VersionStatus.Published);

            List<LibraryPackage> libPackages = new List<LibraryPackage>();
            libPackages.Add(libPackage);

            Moq.Mock<IPackageLibrary> mockLibrary = new Moq.Mock<IPackageLibrary>();
            mockLibrary.Setup(x => x.GetPackages()).Returns(libPackages);

            List<InstalledPackage> insList = new List<InstalledPackage>();
            

            Moq.Mock<IPackageInstaller> mockInstaller = new Moq.Mock<IPackageInstaller>();
            mockInstaller.Setup(x => x.GetInstalledPackages()).Returns(insList);

            

            List<int> insReleases = new List<int>();
            insReleases.Add(2013);


            Moq.Mock<IRevitApplicationUtil> mockRevitAppUtil = new Moq.Mock<IRevitApplicationUtil>();
            mockRevitAppUtil.Setup(m => m.GetInstalledRevitReleases()).Returns(insReleases);


            Moq.Mock<IDialogService> mockDialogService = new Moq.Mock<IDialogService>();


            
            RevitAddInLibrary.Installer.InstallerWindowViewModel windowViewModel = new Installer.InstallerWindowViewModel(mockLibrary.Object, mockInstaller.Object, null, mockRevitAppUtil.Object, mockDialogService.Object);



            var packVMList = windowViewModel.Packages.Cast<RevitAddInLibrary.Installer.PackageViewModel>().ToList();

            Assert.AreEqual(1, packVMList.Count);


            PackageViewModel vm = packVMList[0];
            Assert.IsFalse(vm.IsInstalled);
            Assert.IsNull(vm.InstalledVersion);


            mockInstaller.Setup(x => x.Install(mockLibrary.Object, vm.OfficialLibraryVersion.InstallParameter.TargetRevitRelease, vm.OfficialLibraryVersion.InstallParameter.Package.PackageCode, vm.OfficialLibraryVersion.InstallParameter.Package.VersionName)).Verifiable();
            


            //Act
            windowViewModel.InstallCommand.Execute(vm.OfficialLibraryVersion.InstallParameter);


            //Assert
            Assert.IsTrue(vm.IsInstalled);
            Assert.IsNotNull(vm.InstalledVersion);


            mockInstaller.Verify();




        }


        [TestMethod]
        public void UninstallCommand()
        {
            var libPackage = GetTestLibPackage("packagecode", 1, 2013, null, VersionStatus.Published);

            List<LibraryPackage> libPackages = new List<LibraryPackage>();
            libPackages.Add(libPackage);

            Moq.Mock<IPackageLibrary> mockLibrary = new Moq.Mock<IPackageLibrary>();
            mockLibrary.Setup(x => x.GetPackages()).Returns(libPackages);


            var insPack = GetTestInstalledPackage("packagecode", 1, 2013, null, 2013);

            List<InstalledPackage> insList = new List<InstalledPackage>();
            insList.Add(insPack);

            Moq.Mock<IPackageInstaller> mockInstaller = new Moq.Mock<IPackageInstaller>();
            mockInstaller.Setup(x => x.GetInstalledPackages()).Returns(insList);


            List<int> insReleases = new List<int>();
            insReleases.Add(2013);

            Moq.Mock<IRevitApplicationUtil> mockRevitAppUtil = new Moq.Mock<IRevitApplicationUtil>();
            mockRevitAppUtil.Setup(m => m.GetInstalledRevitReleases()).Returns(new List<int>());

            Moq.Mock<IDialogService> mockDialogService = new Moq.Mock<IDialogService>();


            RevitAddInLibrary.Installer.InstallerWindowViewModel windowViewModel = new Installer.InstallerWindowViewModel(mockLibrary.Object, mockInstaller.Object, null, mockRevitAppUtil.Object, mockDialogService.Object);



            var beforeUninstall = windowViewModel.Packages.Cast<RevitAddInLibrary.Installer.PackageViewModel>().ToList();

            Assert.AreEqual(1, beforeUninstall.Count);
            PackageViewModel vm = beforeUninstall[0];
            Assert.IsTrue(vm.IsInstalled);
            Assert.IsNotNull(vm.InstalledVersion);



            mockInstaller.Setup(x => x.Uninstall(vm.InstalledVersion.InstallParameter.TargetRevitRelease, vm.InstalledVersion.InstallParameter.Package.PackageCode)).Verifiable();


            //Act
            windowViewModel.UninstallCommand.Execute(vm.InstalledVersion.InstallParameter);


            //Assert
            Assert.IsFalse(vm.IsInstalled);
            Assert.IsNull(vm.InstalledVersion);


            mockInstaller.Verify();
        }



        [TestMethod]
        public void RefreshListCommand()
        {
            throw new NotImplementedException();
        }



        [TestMethod]
        public void UpdateAllCommand()
        {
            throw new NotImplementedException();
        }






        [TestMethod]
        public void ShowUnpublished()
        {
            //arrange
            var insPub = GetTestLibPackage("installed.published", 1, 2013, null, VersionStatus.Published);
            var notInsPub = GetTestLibPackage("notinstalled.published", 1, 2013, null, VersionStatus.Published);

            var insUnpub = GetTestLibPackage("installed.unpublished", 1, 2013, null, VersionStatus.Unpublished);
            var notInsUnpub = GetTestLibPackage("notinstalled.unpublished", 1, 2013, null, VersionStatus.Unpublished);


            var insSusp = GetTestLibPackage("installed.suspended", 1, 2013, null, VersionStatus.Suspended);
            var notInsSusp = GetTestLibPackage("notinstalled.suspended", 1, 2013, null, VersionStatus.Suspended);


            var ins2015 = GetTestLibPackage("installed.2015", 1, 2015, 2015, VersionStatus.Published);
            var noIns2015 = GetTestLibPackage("notinstalled.2015", 1, 2015, 2015, VersionStatus.Published);


            List<LibraryPackage> libPackages = new List<LibraryPackage>();

            libPackages.Add(insPub);
            libPackages.Add(notInsPub);
            libPackages.Add(insUnpub);
            libPackages.Add(notInsUnpub);
            libPackages.Add(insSusp);
            libPackages.Add(notInsSusp);
            libPackages.Add(ins2015);
            libPackages.Add(noIns2015);

            Moq.Mock<IPackageLibrary> mockLibrary = new Moq.Mock<IPackageLibrary>();
            mockLibrary.Setup(x => x.GetPackages()).Returns(libPackages);



            var pubInsPack = GetTestInstalledPackage("installed.published",1,2013,null,2013);
            var unPubInsPack = GetTestInstalledPackage("installed.unpublished",1,2013,null,2013);
            var suspInsPack = GetTestInstalledPackage("installed.suspended",1,2013,null,2013);
            var ins2015Pack = GetTestInstalledPackage("installed.2015", 1,2015,2015,2015);
            var ins2016Pack = GetTestInstalledPackage("installed.2016", 1, 2016, 2016, 2016);
           

            List<InstalledPackage> insList = new List<InstalledPackage>();
            insList.Add(pubInsPack);
            insList.Add(unPubInsPack);
            insList.Add(suspInsPack);
            insList.Add(ins2015Pack);
            insList.Add(ins2016Pack);

            


            Moq.Mock<IPackageInstaller> mockInstaller = new Moq.Mock<IPackageInstaller>();
            mockInstaller.Setup(x => x.GetInstalledPackages()).Returns(insList);


            List<int> insReleases = new List<int>();
            insReleases.Add(2013);
            insReleases.Add(2014);




            Moq.Mock<IRevitApplicationUtil> mockRevitAppUtil = new Moq.Mock<IRevitApplicationUtil>();
            mockRevitAppUtil.Setup(m => m.GetInstalledRevitReleases()).Returns(insReleases);

            Moq.Mock<IDialogService> mockDialogService = new Moq.Mock<IDialogService>();


            RevitAddInLibrary.Installer.InstallerWindowViewModel windowViewModel = new Installer.InstallerWindowViewModel(mockLibrary.Object, mockInstaller.Object,null, mockRevitAppUtil.Object, mockDialogService.Object);


            
            
            Assert.IsFalse(windowViewModel.ShowUnpublished);  //show unpublished is false by default




            var hideUnpubList = windowViewModel.Packages.Cast<RevitAddInLibrary.Installer.PackageViewModel>().ToList();



            Assert.AreEqual(8, hideUnpubList.Count);

            //check all published packages are visible
            Assert.IsTrue(hideUnpubList.Where(x => x.PackageCode == "installed.published" & x.TargetRelease == 2013).Count() == 1);
            Assert.IsTrue(hideUnpubList.Where(x => x.PackageCode == "installed.published" & x.TargetRelease == 2014).Count() == 1);
            Assert.IsTrue(hideUnpubList.Where(x => x.PackageCode == "notinstalled.published" & x.TargetRelease == 2013).Count() == 1);
            Assert.IsTrue(hideUnpubList.Where(x => x.PackageCode == "notinstalled.published" & x.TargetRelease == 2014).Count() == 1);

            //check all other installed packages are visible
            Assert.IsTrue(hideUnpubList.Where(x => x.PackageCode == "installed.unpublished" & x.TargetRelease == 2013).Count() == 1);
            Assert.IsTrue(hideUnpubList.Where(x => x.PackageCode == "installed.suspended" & x.TargetRelease == 2013).Count() == 1);
            Assert.IsTrue(hideUnpubList.Where(x => x.PackageCode == "installed.2015" & x.TargetRelease == 2015).Count() == 1);
            Assert.IsTrue(hideUnpubList.Where(x => x.PackageCode == "installed.2016" & x.TargetRelease == 2016).Count() == 1);


            //show unpublished
            windowViewModel.ShowUnpublished = true;



            var showUnpubList = windowViewModel.Packages.Cast<RevitAddInLibrary.Installer.PackageViewModel>().ToList();


            Assert.AreEqual(11, showUnpubList.Count);

            //all published on visible
            Assert.IsTrue(showUnpubList.Where(x => x.PackageCode == "installed.published" & x.TargetRelease == 2013).Count() == 1);
            Assert.IsTrue(showUnpubList.Where(x => x.PackageCode == "installed.published" & x.TargetRelease == 2014).Count() == 1);
            Assert.IsTrue(showUnpubList.Where(x => x.PackageCode == "notinstalled.published" & x.TargetRelease == 2013).Count() == 1);
            Assert.IsTrue(showUnpubList.Where(x => x.PackageCode == "notinstalled.published" & x.TargetRelease == 2014).Count() == 1);

            //all unpublished are visible
            Assert.IsTrue(showUnpubList.Where(x => x.PackageCode == "installed.unpublished" & x.TargetRelease == 2013).Count() == 1);
            Assert.IsTrue(showUnpubList.Where(x => x.PackageCode == "installed.unpublished" & x.TargetRelease == 2014).Count() == 1);
            Assert.IsTrue(showUnpubList.Where(x => x.PackageCode == "notinstalled.unpublished" & x.TargetRelease == 2013).Count() == 1);
            Assert.IsTrue(showUnpubList.Where(x => x.PackageCode == "notinstalled.unpublished" & x.TargetRelease == 2014).Count() == 1);

            //other installed are visible
            Assert.IsTrue(hideUnpubList.Where(x => x.PackageCode == "installed.suspended" & x.TargetRelease == 2013).Count() == 1);
            Assert.IsTrue(hideUnpubList.Where(x => x.PackageCode == "installed.2015" & x.TargetRelease == 2015).Count() == 1);
            Assert.IsTrue(hideUnpubList.Where(x => x.PackageCode == "installed.2016" & x.TargetRelease == 2016).Count() == 1);


        }
    }
}
