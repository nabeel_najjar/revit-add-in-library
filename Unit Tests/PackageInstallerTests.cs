﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RevitAddInLibrary;
using System.IO;
using Moq;

namespace RevitAddInLibrary.Tests
{
    /// <summary>
    /// Summary description for TestPackageInstaller
    /// </summary>
    [TestClass]
    public class PackageInstallerTests
    {

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion





        [TestInitialize()]
        public void InitTest()
        {
            //Removes all
            PackageInstaller installer = new PackageInstaller(new RevitApplicationUtil());
            var res = installer.UninstallAll();
        }



        [ClassCleanup()]
        public static void CleanUp()
        {
            PackageInstaller installer = new PackageInstaller();
            var res = installer.UninstallAll();
        }


        //installer.Install(@"C:\PackageFile");
        //installer.Install(lib, "package.code", 2013);


        //installer.Uninstall("package.code", 2013);
        //installer.UninstallAll();

        //installer.Update(lib, "package.code", 2013);
        //installer.UpdateAll(lib, 2013, required);


        [TestMethod]
        public void GetInstalledPackages()
        {
            //setup
            PackageInstaller installer = new PackageInstaller();

            

            //Act
            installer.UninstallAll();

            var installResult = installer.Install(2013,"com.acme.testpackage_1.0.0.rpk");

            var installedPackages = installer.GetInstalledPackages();



            //assert
            Assert.AreEqual(1, installedPackages.Count);

            var IPackage = installedPackages[0];

            Assert.AreEqual("1.0.0", IPackage.Package.VersionName);
            Assert.AreEqual("com.acme.testpackage", IPackage.Package.PackageCode);

        }




        [TestMethod]
        [ExpectedException(typeof (InvalidOperationException))]
        public void Install_ReleaseLessThenMinRelease_InvalidOperationException()
        {

            PackageInstaller installer = new PackageInstaller();

            try
            {    
                installer.Install(2012, "com.acme.testpackage_1.0.0.rpk");
            }
            finally
            {
                installer.UninstallAll();
            }


            
        }


        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Install_ReleaseGreaterThenMaxRelease_InvalidOperationException()
        {

            PackageInstaller installer = new PackageInstaller();
            try
            {
                installer.Install(2016, "com.acme.testpackage_1.0.0.rpk");
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                installer.UninstallAll();
            }


        }



        
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Uninstall_TargetReleaseRunning_InvalidOperationException()
        {

            try
            {

                Mock<IRevitApplicationUtil> mockRevitAppUtil = new Mock<IRevitApplicationUtil>();


                int targetRelease = 2013;

                List<int> installedReleases = new List<int>();
                installedReleases.Add(2013);


                mockRevitAppUtil.Setup(r => r.IsRevitRunning(targetRelease)).Returns(true);
                mockRevitAppUtil.Setup(r => r.GetInstalledRevitReleases()).Returns(installedReleases);



                PackageInstaller installer = new PackageInstaller(mockRevitAppUtil.Object);


                installer.Install(targetRelease, "com.acme.testpackage_1.0.0.rpk");



                installer.Uninstall(targetRelease, "com.acme.testpackage");




            }
            finally
            {
                PackageInstaller i = new PackageInstaller(new RevitApplicationUtil());
                i.UninstallAll();
            }

        }






        [TestMethod]
        public void InstallFromFile()
        {
            //setup
            PackageInstaller installer = new PackageInstaller();

            int targetRelease = 2013;

            //Install 1.0.0
            var result1 = installer.Install(targetRelease, "com.acme.testpackage_1.0.0.rpk");

            //Assert 1.0.0
            Assert.IsNull(result1.Previous);
            Assert.IsNotNull(result1.Current);
            Assert.AreEqual(InstallationType.Install, result1.Type);
            Assert.AreEqual("1.0.0", result1.Current.VersionName);
            AssertSamplePackageIsInstalled();


            //Upgrade to 3.0.0
            var result2 = installer.Install(targetRelease, "com.acme.testpackage_3.0.0.rpk");
            Assert.IsNotNull(result2.Previous);
            Assert.IsNotNull(result2.Current);
            Assert.AreEqual("1.0.0", result2.Previous.VersionName);
            Assert.AreEqual("3.0.0", result2.Current.VersionName);
            Assert.AreEqual(InstallationType.Upgrade, result2.Type);
            AssertSamplePackageIsInstalled();


            //downgrade to 2.0.0
            var result3 = installer.Install(targetRelease, "com.acme.testpackage_2.0.0.rpk");
            Assert.IsNotNull(result3.Previous);
            Assert.IsNotNull(result3.Current);
            Assert.AreEqual("3.0.0", result3.Previous.VersionName);
            Assert.AreEqual("2.0.0", result3.Current.VersionName);
            Assert.AreEqual(InstallationType.Downgrade, result3.Type);
            AssertSamplePackageIsInstalled();


            //reinstall 2.0.0
            var result4 = installer.Install(targetRelease, "com.acme.testpackage_2.0.0.rpk");
            Assert.IsNotNull(result4.Previous);
            Assert.IsNotNull(result4.Current);
            Assert.AreEqual("2.0.0", result4.Previous.VersionName);
            Assert.AreEqual("2.0.0", result4.Current.VersionName);
            Assert.AreEqual(InstallationType.Reinstall, result4.Type);
            AssertSamplePackageIsInstalled();


            //uninstall 
            var result5 = installer.Uninstall(targetRelease, "com.acme.testpackage");
            Assert.IsNotNull(result5.Previous);
            Assert.IsNull(result5.Current);
            Assert.AreEqual("2.0.0", result5.Previous.VersionName);
            Assert.AreEqual(InstallationType.Uninstall, result5.Type);
            AssertSamplePackageIsUninstalled();

        }


        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Update_PackageNotInstalled_InvalidOperationException()
        {
            PackageInstaller installer = new PackageInstaller();

            Mock<IPackageLibrary> lib = new Mock<IPackageLibrary>();

            installer.Update(lib.Object, 2013, "com.acme.testpackage");
        }




        [TestMethod]
        public void Update_OfficalVersionCodeGreaterThanInstalled()
        {
            //official version known
            //official version is newer then installed
            //expect upgrade

            int targetRelease = 2013;
            
            PackageInstaller installer = new PackageInstaller();
            installer.Install(targetRelease, "com.acme.testpackage_1.0.0.rpk");

            Package installedPackage = Package.ReadPackage("com.acme.testpackage_1.0.0.rpk");
            LibraryPackage installedLibPackage = new LibraryPackage(installedPackage, VersionStatus.Published, "added by user", DateTime.Now.ToUniversalTime(), 123);


            Package officalPackage = Package.ReadPackage("com.acme.testpackage_2.0.0.rpk");
            LibraryPackage officalLibPackage = new LibraryPackage(officalPackage, VersionStatus.Published, "added by user", DateTime.Now.ToUniversalTime(), 123);


            Mock<IPackageLibrary> libMock = new Mock<IPackageLibrary>(MockBehavior.Strict);
            libMock.Setup(x => x.GetPackage("com.acme.testpackage", "1.0.0")).Returns(installedLibPackage);
            libMock.Setup(x => x.GetLatestPublishedVersion("com.acme.testpackage", 2013)).Returns(officalLibPackage);
            libMock.Setup(x => x.GetPackageData("com.acme.testpackage", "2.0.0")).Returns(new FileStream("com.acme.testpackage_2.0.0.rpk",FileMode.Open,FileAccess.Read));

            //act
            var result = installer.Update(libMock.Object, 2013, "com.acme.testpackage");

            //assert
            Assert.IsNotNull(result.Current);
            Assert.IsNotNull(result.Previous);
            Assert.AreEqual("1.0.0", result.Previous.VersionName);
            Assert.AreEqual("2.0.0", result.Current.VersionName);
            Assert.AreEqual(UpdateResultType.Upgrade, result.Type);

        }


        [TestMethod]
        public void Update_OfficalVersionCodeLessThanInstalled_InstalledStatusNotKnown()
        {
            //official version known
            //official version is lessed then installed
            //installed status not know

            PackageInstaller installer = new PackageInstaller();
            installer.Install(2013, "com.acme.testpackage_2.0.0.rpk");

            Package installedPackage = Package.ReadPackage("com.acme.testpackage_2.0.0.rpk");
            


            Package officalPackage = Package.ReadPackage("com.acme.testpackage_1.0.0.rpk");
            LibraryPackage officalLibPackage = new LibraryPackage(officalPackage, VersionStatus.Published, "added by user", DateTime.Now.ToUniversalTime(), 123);


            Mock<IPackageLibrary> libMock = new Mock<IPackageLibrary>(MockBehavior.Strict);
            libMock.Setup(x => x.GetPackage("com.acme.testpackage", "2.0.0")).Returns((LibraryPackage)null);
            libMock.Setup(x => x.GetLatestPublishedVersion("com.acme.testpackage", 2013)).Returns(officalLibPackage);

            //act
            var result = installer.Update(libMock.Object, 2013, "com.acme.testpackage");

            //assert
            Assert.IsNotNull(result.Current);
            Assert.IsNotNull(result.Previous);
            Assert.AreEqual("2.0.0", result.Previous.VersionName);
            Assert.AreEqual("2.0.0", result.Current.VersionName);
            Assert.AreEqual(UpdateResultType.NewerThanOfficalInstalled, result.Type);
        }



        [TestMethod]
        public void Update_OfficalVersionCodeLessThanInstalled_InstalledSuspended()
        {

            PackageInstaller installer = new PackageInstaller();
            installer.Install(2013, "com.acme.testpackage_2.0.0.rpk");

            Package installedPackage = Package.ReadPackage("com.acme.testpackage_2.0.0.rpk");
            LibraryPackage installedLibPackage = new LibraryPackage(installedPackage, VersionStatus.Suspended, "added by user", DateTime.Now.ToUniversalTime(), 123);

            Package officalPackage = Package.ReadPackage("com.acme.testpackage_1.0.0.rpk");
            LibraryPackage officalLibPackage = new LibraryPackage(officalPackage, VersionStatus.Published, "added by user", DateTime.Now.ToUniversalTime(), 123);


            Mock<IPackageLibrary> libMock = new Mock<IPackageLibrary>(MockBehavior.Strict);
            libMock.Setup(x => x.GetPackage("com.acme.testpackage", "2.0.0")).Returns(installedLibPackage);
            libMock.Setup(x => x.GetLatestPublishedVersion("com.acme.testpackage", 2013)).Returns(officalLibPackage);
            libMock.Setup(x => x.GetPackageData("com.acme.testpackage", "1.0.0")).Returns(new FileStream("com.acme.testpackage_1.0.0.rpk",FileMode.Open,FileAccess.Read));

            //act
            var result = installer.Update(libMock.Object, 2013, "com.acme.testpackage");

            //assert
            Assert.IsNotNull(result.Current);
            Assert.IsNotNull(result.Previous);
            Assert.AreEqual("2.0.0", result.Previous.VersionName);
            Assert.AreEqual("1.0.0", result.Current.VersionName);
            Assert.AreEqual(UpdateResultType.DowngradeFromSuspended, result.Type);
        }


        [TestMethod]
        public void Update_OfficalVersionKnown_VersionCodeEqual()
        {
            //official version known
            //official version is the same as installed
            //expect no change



            PackageInstaller installer = new PackageInstaller();
            installer.Install(2013,"com.acme.testpackage_2.0.0.rpk");

            Package installedPackage = Package.ReadPackage("com.acme.testpackage_2.0.0.rpk");
            LibraryPackage installedLibPackage = new LibraryPackage(installedPackage, VersionStatus.Suspended, "added by user", DateTime.Now.ToUniversalTime(), 123);

            Package officalPackage = Package.ReadPackage("com.acme.testpackage_2.0.0.rpk");
            LibraryPackage officalLibPackage = new LibraryPackage(officalPackage, VersionStatus.Published, "added by user", DateTime.Now.ToUniversalTime(), 123);


            Mock<IPackageLibrary> libMock = new Mock<IPackageLibrary>(MockBehavior.Strict);
            libMock.Setup(x => x.GetPackage("com.acme.testpackage", "2.0.0")).Returns(installedLibPackage);
            libMock.Setup(x => x.GetLatestPublishedVersion("com.acme.testpackage", 2013)).Returns(officalLibPackage);

            //act
            var result = installer.Update(libMock.Object, 2013, "com.acme.testpackage");

            //assert
            Assert.IsNotNull(result.Current);
            Assert.IsNotNull(result.Previous);
            Assert.AreEqual("2.0.0", result.Previous.VersionName);
            Assert.AreEqual("2.0.0", result.Current.VersionName);
            Assert.AreEqual(UpdateResultType.OfficalInstalled, result.Type);
        }






        [TestMethod]
        public void Update_NoOfficalVersion_UnknownInstalledStatus()
        {
            //no official version
            //status of installed version not known
            //expect no change

            PackageInstaller installer = new PackageInstaller();
            installer.Install(2013,"com.acme.testpackage_2.0.0.rpk");

            Package installedPackage = Package.ReadPackage("com.acme.testpackage_2.0.0.rpk");
            

            Mock<IPackageLibrary> libMock = new Mock<IPackageLibrary>(MockBehavior.Strict);
            libMock.Setup(x => x.GetPackage("com.acme.testpackage", "2.0.0")).Returns((LibraryPackage)null);
            libMock.Setup(x => x.GetLatestPublishedVersion("com.acme.testpackage", 2013)).Returns((LibraryPackage)null);

            //act
            var result = installer.Update(libMock.Object, 2013, "com.acme.testpackage");

            //assert
            Assert.IsNotNull(result.Current);
            Assert.IsNotNull(result.Previous);
            Assert.AreEqual("2.0.0", result.Previous.VersionName);
            Assert.AreEqual("2.0.0", result.Current.VersionName);
            Assert.AreEqual(UpdateResultType.UnknownOfficalVersion, result.Type);


        }


        [TestMethod]
        public void Update_NoOfficalVersion_InstalledUnpublished()
        {
            //no official version
            //installed version Unpublished
            //expect no change





            PackageInstaller installer = new PackageInstaller();
            installer.Install(2013,"com.acme.testpackage_2.0.0.rpk");

            Package installedPackage = Package.ReadPackage("com.acme.testpackage_2.0.0.rpk");
            LibraryPackage installedLibPackage = new LibraryPackage(installedPackage, VersionStatus.Unpublished, "added by user", DateTime.Now.ToUniversalTime(), 123);

           

            Mock<IPackageLibrary> libMock = new Mock<IPackageLibrary>(MockBehavior.Strict);
            libMock.Setup(x => x.GetPackage("com.acme.testpackage", "2.0.0")).Returns(installedLibPackage);
            libMock.Setup(x => x.GetLatestPublishedVersion("com.acme.testpackage", 2013)).Returns((LibraryPackage)null);

            //act
            var result = installer.Update(libMock.Object, 2013, "com.acme.testpackage");

            //assert
            Assert.IsNotNull(result.Current);
            Assert.IsNotNull(result.Previous);
            Assert.AreEqual("2.0.0", result.Previous.VersionName);
            Assert.AreEqual("2.0.0", result.Current.VersionName);
            Assert.AreEqual(UpdateResultType.UnknownOfficalVersion, result.Type);


        }

        [TestMethod]
        public void Update_NoOfficalVersion_InstalledVersionSuspended()
        {
            //no official version
            //installed version is suspended
            //expect uninstall


            PackageInstaller installer = new PackageInstaller();
            installer.Install(2013, "com.acme.testpackage_2.0.0.rpk");

            Package installedPackage = Package.ReadPackage("com.acme.testpackage_2.0.0.rpk");
            LibraryPackage installedLibPackage = new LibraryPackage(installedPackage, VersionStatus.Suspended, "added by user", DateTime.Now.ToUniversalTime(), 123);



            Mock<IPackageLibrary> libMock = new Mock<IPackageLibrary>(MockBehavior.Strict);
            libMock.Setup(x => x.GetPackage("com.acme.testpackage", "2.0.0")).Returns(installedLibPackage);
            libMock.Setup(x => x.GetLatestPublishedVersion("com.acme.testpackage", 2013)).Returns((LibraryPackage)null);

            //act
            var result = installer.Update(libMock.Object, 2013, "com.acme.testpackage");

            //assert
            Assert.IsNull(result.Current);
            Assert.IsNotNull(result.Previous);
            Assert.AreEqual("2.0.0", result.Previous.VersionName);
            Assert.AreEqual(UpdateResultType.UninstallSuspended, result.Type);

        }


        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void UpdateAll_NotInstalledRelease_InvalidOperationException()
        {

            List<int> insRelease = new List<int>();
            insRelease.Add(2013);
            insRelease.Add(2014);

            Mock<IRevitApplicationUtil> appUtilMock = new Mock<IRevitApplicationUtil>();
            appUtilMock.Setup(a => a.GetInstalledRevitReleases()).Returns(insRelease);

            PackageInstaller installer = new PackageInstaller(appUtilMock.Object);


            Mock<IPackageLibrary> libMock = new Mock<IPackageLibrary>();


            installer.UpdateAll(libMock.Object, 2015, null);


        }

        [TestMethod]
        public void UpdateAll_InstallsRequiredPackages()
        {
            Package officalPackage = Package.ReadPackage("com.acme.testpackage_2.0.0.rpk");
            LibraryPackage officalLibPackage = new LibraryPackage(officalPackage, VersionStatus.Published, "added by user", DateTime.Now.ToUniversalTime(), 123);


            Mock<IPackageLibrary> libMock = new Mock<IPackageLibrary>(MockBehavior.Strict);
            libMock.Setup(x => x.GetLatestPublishedVersion("com.acme.testpackage", 2013)).Returns(officalLibPackage);
            libMock.Setup(x => x.GetLatestPublishedVersion("com.acme.testpackage", 2014)).Returns(officalLibPackage);
            libMock.Setup(x => x.GetPackageData("com.acme.testpackage", "2.0.0")).Returns(delegate() { return new FileStream("com.acme.testpackage_2.0.0.rpk", FileMode.Open, FileAccess.Read); });

            List<RequiredPackage> reqList = new List<RequiredPackage>();
            reqList.Add(new RequiredPackage(2013,"com.acme.testpackage"));
            reqList.Add(new RequiredPackage(2014, "com.acme.testpackage"));
            reqList.Add(new RequiredPackage(2015, "com.acme.testpackage"));


            List<int> installedReleases = new List<int>();
            installedReleases.Add(2013);
            installedReleases.Add(2014);

            
            Mock<IRevitApplicationUtil> revitAppUtilMock = new Mock<IRevitApplicationUtil>(MockBehavior.Strict);
            revitAppUtilMock.Setup(a=> a.GetInstalledRevitReleases()).Returns(installedReleases);


            PackageInstaller installer = new PackageInstaller(revitAppUtilMock.Object);


            //act
            var result = installer.UpdateAll(libMock.Object,null, reqList);

            //assert
            Assert.AreEqual(2,result.Count);
            Assert.IsNull(result[0].Previous);
            Assert.IsNotNull(result[0].Current);

            Assert.IsNull(result[1].Previous);
            Assert.IsNotNull(result[1].Current);

            Assert.AreEqual("2.0.0", result[0].Current.VersionName);
            Assert.AreEqual("2.0.0", result[1].Current.VersionName);

            Assert.AreEqual(UpdateResultType.InstalledRequired, result[0].Type);
            Assert.AreEqual(UpdateResultType.InstalledRequired, result[1].Type);

        }






        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Update_NullLib_ArgumentNullException()
        {
            PackageInstaller installer = new PackageInstaller();

            installer.Update(null, 2013, "Package.code");
        }



        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Update_NulLPackageCode_ArgumentNullException()
        {
            PackageInstaller installer = new PackageInstaller();

            Mock<IPackageLibrary> lib = new Mock<IPackageLibrary>();

            installer.Update(lib.Object, 2013, null);
        }






        private void AssertSamplePackageIsInstalled()
        {
            //addin files exists
            var appData = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var revitLibFolder = Path.Combine(appData, "Revit Add-In Library");
            var packageFilesDir = Path.Combine(revitLibFolder, "2013", "com.acme.testpackage");


            Assert.IsTrue(File.Exists(Path.Combine(packageFilesDir, "TestFolder", "TestFile3.txt")));
            Assert.IsTrue(File.Exists(Path.Combine(packageFilesDir, "LargeImage.png")));
            Assert.IsTrue(File.Exists(Path.Combine(packageFilesDir, "SmallImage.png")));
            Assert.IsTrue(File.Exists(Path.Combine(packageFilesDir, "TestFile1.txt")));
            Assert.IsTrue(File.Exists(Path.Combine(packageFilesDir, "TestFile2.txt")));
            Assert.IsTrue(File.Exists(Path.Combine(packageFilesDir, "TestPackage.dll")));
            Assert.IsTrue(File.Exists(Path.Combine(packageFilesDir, "ToolTipImage.png")));

            var addinManifestPath = Path.Combine(appData, "Autodesk", "Revit", "Addins", "2013", "com.acme.testpackage.addin");

            Assert.IsTrue(File.Exists(addinManifestPath));
        }



        private void AssertSamplePackageIsUninstalled()
        {


            var appData = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var revitLibFolder = Path.Combine(appData, "Revit Add-In Library");
            var packageFilesDir = Path.Combine(revitLibFolder, "2013", "com.acme.testpackage");

            Assert.IsFalse(Directory.Exists(packageFilesDir));
            Assert.IsFalse(File.Exists(Path.Combine(packageFilesDir, "TestFolder", "TestFile3.txt")));
            Assert.IsFalse(File.Exists(Path.Combine(packageFilesDir, "LargeImage.png")));
            Assert.IsFalse(File.Exists(Path.Combine(packageFilesDir, "SmallImage.png")));
            Assert.IsFalse(File.Exists(Path.Combine(packageFilesDir, "TestFile1.txt")));
            Assert.IsFalse(File.Exists(Path.Combine(packageFilesDir, "TestFile2.txt")));
            Assert.IsFalse(File.Exists(Path.Combine(packageFilesDir, "TestPackage.dll")));
            Assert.IsFalse(File.Exists(Path.Combine(packageFilesDir, "ToolTipImage.png")));

            var addinManifestPath = Path.Combine(appData, "Autodesk", "Revit", "Addins", "2013", "com.acme.testpackage.addin");

            Assert.IsFalse(File.Exists(addinManifestPath));
        }




        [TestMethod]
        public void InstallFromLib()
        {

            PackageInstaller installer = new PackageInstaller();

            string packageCode = "com.acme.testpackage";
            int release = 2013;
            string version = "1.0.0";

            Package package = new Package(packageCode, "Test Package", String.Empty, version, 1, String.Empty, release, null, "Author", "email@acme.com", null);



            LibraryPackage libPack = new LibraryPackage(package, VersionStatus.Published, "added by user", DateTime.Now.ToUniversalTime(), 1000);


            Moq.Mock<IPackageLibrary> mockLib = new Moq.Mock<IPackageLibrary>(Moq.MockBehavior.Strict);
            mockLib.Setup(x => x.GetLatestPublishedVersion(packageCode, release)).Returns(libPack);
            mockLib.Setup(x => x.GetPackageData(packageCode, version)).Returns(new FileStream("com.acme.testpackage_1.0.0.rpk", FileMode.Open, FileAccess.Read));


            //act
            var result = installer.Install(mockLib.Object, release, packageCode);

            Assert.IsNull(result.Previous);
            Assert.IsNotNull(result.Current);
            Assert.AreEqual(InstallationType.Install, result.Type);
        }







    }

}

