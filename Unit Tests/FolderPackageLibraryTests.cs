﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Linq;

namespace RevitAddInLibrary.Tests
{

    


    [TestClass]
    public class FolderPackageLibraryTests
    {

        private string _testLibFolderPath = System.IO.Path.Combine(System.IO.Path.GetTempPath(), "Revit Add-In Library Unit Test Library");

        [TestMethod]
        public void GetPackages()
        {
            InitLib();
            FolderPackageLibrary lib = new FolderPackageLibrary(_testLibFolderPath, false);


            using (var fs = new FileStream("com.acme.testpackage_1.0.0.rpk", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                lib.AddPackage(fs);
            }


            using (var fs = new FileStream("com.acme.testpackage_2.0.0.rpk", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                lib.AddPackage(fs);
            }


            using (var fs = new FileStream("com.acme.testpackage_3.0.0.rpk", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                lib.AddPackage(fs);
            }



            var packages = lib.GetPackages().ToList();
            Assert.AreEqual(3, packages.Count);

            var p1 = lib.GetPackage("com.acme.testpackage", "1.0.0");
            var p2 = lib.GetPackage("com.acme.testpackage", "2.0.0");
            var p3 = lib.GetPackage("com.acme.testpackage", "3.0.0");



            Assert.AreEqual("1.0.0", p1.Package.VersionName);
            Assert.AreEqual("2.0.0", p2.Package.VersionName);
            Assert.AreEqual("3.0.0", p3.Package.VersionName);

            

            

           


           

            //lib.UpdatePackage()



        }



        public void UpdatePackage()
        {
            InitLib();
            FolderPackageLibrary lib = new FolderPackageLibrary(_testLibFolderPath, false);


            using (var fs = new FileStream("com.acme.testpackage_1.0.0.rpk", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                lib.AddPackage(fs);
            }


            var libPack = lib.GetPackage("com.acme.testpackage", "1.0.0");


            VersionStatus newStatus = VersionStatus.Published;
            String newAuthor = "new author";
            string newEmail = "new@email.com";
            Uri newLink = new Uri(@"http://www.newlink.com");
            int newMaxRelease = 2020;
            int newMinRelease = 2011;
            string newPackNotes = "new package notes";
            string newVerNotes = "new version notes";
            string newPackName = "new package name";

            libPack.Status = newStatus;
            libPack.Package.Author = newAuthor;
            libPack.Package.Email = newEmail;
            libPack.Package.Link = newLink;
            libPack.Package.MaxRevitRelease = newMaxRelease;
            libPack.Package.MinRevitRelease = newMinRelease;
            libPack.Package.PackageNotes = newPackNotes;
            libPack.Package.VersionNotes = newVerNotes;
            libPack.Package.PackageName = newPackName;


            lib.UpdatePackage(libPack);


            var returnedPack = lib.GetPackage("com.acme.testpackage", "1.0.0");

            Assert.AreEqual(newStatus, returnedPack.Status);
            Assert.AreEqual(newAuthor, returnedPack.Package.Author);
            Assert.AreEqual(newEmail, returnedPack.Package.Email);
            Assert.AreEqual(newLink, returnedPack.Package.Link);
            Assert.AreEqual(newMaxRelease, returnedPack.Package.MaxRevitRelease);
            Assert.AreEqual(newMinRelease, returnedPack.Package.MinRevitRelease);
            Assert.AreEqual(newPackNotes, returnedPack.Package.PackageNotes);
            Assert.AreEqual(newVerNotes, returnedPack.Package.VersionNotes);
            Assert.AreEqual(newPackName, returnedPack.Package.PackageName);


            Stream modifiedpackageData = null;
            Package modifiedPackage = null;

            try
            {
                modifiedpackageData = lib.GetPackageData("com.acme.testpackage", "1.0.0");
                modifiedPackage = Package.ReadPackage(modifiedpackageData);
            }
            finally
            {
                if (modifiedpackageData != null) modifiedpackageData.Close();
            }

            Assert.IsNotNull(modifiedPackage);
            
            Assert.AreEqual(newAuthor, modifiedPackage.Author);
            Assert.AreEqual(newEmail, modifiedPackage.Email);
            Assert.AreEqual(newLink, modifiedPackage.Link);
            Assert.AreEqual(newMaxRelease, modifiedPackage.MaxRevitRelease);
            Assert.AreEqual(newMinRelease, modifiedPackage.MinRevitRelease);
            Assert.AreEqual(newPackNotes, modifiedPackage.PackageNotes);
            Assert.AreEqual(newVerNotes, modifiedPackage.VersionNotes);
            Assert.AreEqual(newPackName, modifiedPackage.PackageName);
            
            

       

        
        }



        [TestMethod]
        public void GetLatestPublishedVersion()
        {
            InitLib();
            FolderPackageLibrary lib = new FolderPackageLibrary(_testLibFolderPath, false);


            using (var fs = new FileStream("com.acme.testpackage_1.0.0.rpk", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                lib.AddPackage(fs);
            }


            using (var fs = new FileStream("com.acme.testpackage_2.0.0.rpk", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                lib.AddPackage(fs);
            }

            using (var fs = new FileStream("com.acme.testpackage_3.0.0.rpk", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                lib.AddPackage(fs);
            }


            var p1 = lib.GetPackage("com.acme.testpackage", "1.0.0");
            var p2 = lib.GetPackage("com.acme.testpackage", "2.0.0");
            var p3 = lib.GetPackage("com.acme.testpackage", "3.0.0");

            Assert.IsNotNull(p1);
            Assert.IsNotNull(p2);
            Assert.IsNotNull(p3);


            var v1 = lib.GetLatestPublishedVersion("com.acme.testpackage", 2013);
            Assert.IsNull(v1);

            p1.Status = VersionStatus.Published;
            p2.Status = VersionStatus.Published;

            lib.UpdatePackage(p1);
            lib.UpdatePackage(p2);
            
            var v2 = lib.GetLatestPublishedVersion("com.acme.testpackage", 2013);

            Assert.IsNotNull(v2);
            Assert.AreEqual("2.0.0", v2.Package.VersionName);


        




        }


        
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void AddPackage_DuplicateVersionNumber_InvalidOperationException()
        {
            InitLib();
            FolderPackageLibrary lib = new FolderPackageLibrary(_testLibFolderPath, false);


            using (var fs = new FileStream("com.acme.testpackage_1.0.0.rpk", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                lib.AddPackage(fs);
            }

            using (var fs = new FileStream("com.acme.testpackage_1.0.0.rpk", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                lib.AddPackage(fs);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void AddPackage_DuplicateVersionName_InvalidOperationException()
        {
            InitLib();
            FolderPackageLibrary lib = new FolderPackageLibrary(_testLibFolderPath, false);


            using (var fs = new FileStream("com.acme.testpackage_1.0.0.rpk", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                lib.AddPackage(fs);
            }

            using (var fs = new FileStream("com.acme.testpackage_1.0.0.rpk", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                lib.AddPackage(fs);
            }
        }



        [TestMethod]
        public void GetPackageCodes()
        {

            InitLib();
            FolderPackageLibrary lib = new FolderPackageLibrary(_testLibFolderPath, false);


            using (var fs = new FileStream("com.acme.testpackage_1.0.0.rpk", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                lib.AddPackage(fs);
            }

            using (var fs = new FileStream("com.acme.testpackage_2.0.0.rpk", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                lib.AddPackage(fs);
            }

            var codes = lib.GetPackageCodes().ToList();

            Assert.AreEqual(1, codes.Count);
            Assert.AreEqual("com.acme.testpackage", codes[0]);


            
            
        }


        [TestMethod]
        public void DeletePackage()
        {

            InitLib();
            FolderPackageLibrary lib = new FolderPackageLibrary(_testLibFolderPath, false);


            using (var fs = new FileStream("com.acme.testpackage_1.0.0.rpk", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                lib.AddPackage(fs);
            }

            using (var fs = new FileStream("com.acme.testpackage_2.0.0.rpk", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                lib.AddPackage(fs);
            }

            var packages1 = lib.GetPackages().ToList();
            var p1 = lib.GetPackage("com.acme.testpackage", "1.0.0");
            Assert.AreEqual(2, packages1.Count);
            Assert.IsNotNull(p1);

            
            lib.DeletePackage("com.acme.testpackage", "1.0.0");

            var packages2 = lib.GetPackages().ToList();
            var p2 = lib.GetPackage("com.acme.testpackage", "1.0.0");
            Assert.AreEqual(1, packages2.Count);
            Assert.IsNull(p2);
        }



        [TestMethod]
        public void ReadOnlyLibrary_ReadOnlyTrue()
        {
            InitLib();
            FolderPackageLibrary lib = new FolderPackageLibrary(_testLibFolderPath, true);
            Assert.IsTrue(lib.ReadOnly);
        }


        [TestMethod]
        public void LocationProperty()
        {
            InitLib();
            FolderPackageLibrary lib = new FolderPackageLibrary(_testLibFolderPath, true);
            Assert.AreEqual(_testLibFolderPath, lib.Location);
        }


        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ReadOnlyLibrary_AddPackage_InvalidOperationException()
        {
            InitLib();
            FolderPackageLibrary lib = new FolderPackageLibrary(_testLibFolderPath, true);
            lib.AddPackage(new MemoryStream());   
        }



        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ReadOnlyLibrary_DeletePackage_InvalidOperationException()
        {
            InitLib();
            FolderPackageLibrary lib = new FolderPackageLibrary(_testLibFolderPath, true);
            lib.DeletePackage("code.code", "1.0.0");
        }


        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ReadOnlyLibrary_UpdatePackage_InvalidOperationException()
        {
            InitLib();
            FolderPackageLibrary lib = new FolderPackageLibrary(_testLibFolderPath, false);
            Package p = new Package("code","name","notes","name",1,"notes",2013,null,"author","email@acme.com",null);
            lib.UpdatePackage(new LibraryPackage(p, VersionStatus.Published, "user", DateTime.Now.ToUniversalTime(), 123));
        }



        private void InitLib()
        {
            if (Directory.Exists(_testLibFolderPath)) Directory.Delete(_testLibFolderPath, true);
            FolderPackageLibrary.CreateNew(_testLibFolderPath);
        }


    }
}

