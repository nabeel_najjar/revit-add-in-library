﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Deployment.WindowsInstaller;
using System.Diagnostics;

namespace RvtAddInLib.Setup
{
    public class CustomActions
    {
        [CustomAction]
        public static ActionResult MigrateInstalledPackages(Session session)
        {
            try
            {
                session.Log("Migrating installed packages...");

                Migration.MigrateAllUsersInstalledPackages();

                return ActionResult.Success;

            }
            catch (Exception exp)
            {

                session.Log("Installed packages migration failed!");
                session.Log(exp.ToString());

                throw;
            }
        }
    }
}
