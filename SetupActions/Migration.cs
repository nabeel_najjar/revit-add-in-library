﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using RevitAddInLibrary;

namespace RvtAddInLib.Setup
{
    public static class Migration
    {
 



        public static void MigrateUserInstalledPackages(string userName)
        {
            var usersDir = GetUsersDir();


            string[] pathParts = new string[] { usersDir.FullName, userName, "AppData", "Roaming", "Revit Add-In Library", "InstalledPackages.xml" };


            var insPackPath = System.IO.Path.Combine(pathParts);




            if (File.Exists(insPackPath))
            {

                using (var insPackFs = new FileStream(insPackPath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {

                    System.Xml.XmlDocument oldXML = new System.Xml.XmlDocument();

                    oldXML.Load(insPackFs);


                    var revitReleaseNodes = oldXML.SelectNodes("/InstalledPackages/InstalledPackage/Package/RevitRelease").Count;
                    if (revitReleaseNodes == 0) return;

                    List<InstalledPackage> insPackages = new List<InstalledPackage>();

                    foreach (var insPackElem in oldXML.SelectNodes("/InstalledPackages/InstalledPackage").Cast<XmlElement>())
                    {
                        DateTime insOnLocalDate = DateTime.Parse(insPackElem["InstalledOnUTC"].InnerText);
                        DateTime insOnUTC = new DateTime(insOnLocalDate.Ticks, DateTimeKind.Utc);



                        string packageCode = insPackElem.SelectSingleNode(@"Package/PackageCode").InnerText;
                        string packageName = insPackElem.SelectSingleNode(@"Package/PackageName").InnerText;
                        string versionName = insPackElem.SelectSingleNode(@"Package/VersionNumber").InnerText;
                        int versionNumber = System.Convert.ToInt32(insPackElem.SelectSingleNode(@"Package/VersionCode").InnerText);
                        int minRelease = System.Convert.ToInt32(insPackElem.SelectSingleNode(@"Package/RevitRelease").InnerText);


                        string packageNotes = null;
                        var packNotes_node = insPackElem.SelectSingleNode(@"Package/PackageNotes");
                        if (packNotes_node != null && !String.IsNullOrEmpty(packNotes_node.InnerText)) packageNotes = packNotes_node.InnerText;


                        string versionNotes = null;
                        var versionNotes_node = insPackElem.SelectSingleNode(@"Package/VersionNotes");
                        if (versionNotes_node != null && !String.IsNullOrEmpty(versionNotes_node.InnerText)) versionNotes = versionNotes_node.InnerText;


                        string author = null;
                        var author_node = insPackElem.SelectSingleNode(@"Package/Author");
                        if (author_node != null && !String.IsNullOrEmpty(author_node.InnerText)) author = author_node.InnerText;


                        string email = null;
                        var email_node = insPackElem.SelectSingleNode(@"Package/Author");
                        if (email_node != null && !String.IsNullOrEmpty(email_node.InnerText)) email = email_node.InnerText;


                        Uri link = null;
                        var link_node = insPackElem.SelectSingleNode(@"Package/Link");
                        if (link_node != null && !String.IsNullOrEmpty(link_node.InnerText)) link = new Uri(link_node.InnerText);


                        Package package = new Package(packageCode, packageName, packageNotes, versionName, versionNumber, versionNotes, minRelease, null, author, email, link);


                        InstalledPackage insPackage = new InstalledPackage(package, minRelease, insOnUTC);

                        insPackages.Add(insPackage);
                    }


                    //clear existing file
                    insPackFs.SetLength(0);


                    var ser = new System.Runtime.Serialization.DataContractSerializer(typeof(List<InstalledPackage>), "InstalledPackages", string.Empty);
                    ser.WriteObject(insPackFs, insPackages);
                }
            }
        }


        
        public static void MigrateAllUsersInstalledPackages()
        {
            var usersDir = GetUsersDir();
            var userProfileDirs = usersDir.GetDirectories("*", System.IO.SearchOption.TopDirectoryOnly);

            foreach (var d in userProfileDirs)
            {
                try
                {
                    MigrateUserInstalledPackages(d.Name);
                }
                catch
                {

                    continue;
                }
            }
        }

        private static DirectoryInfo GetUsersDir()
        {
            var userProfilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);

            System.IO.DirectoryInfo userProfileDir = new System.IO.DirectoryInfo(userProfilePath);

            var usersDir = userProfileDir.Parent;

            return usersDir;

        }

        public static void MigrateCurrentUserInstalledPackages()
        {
            MigrateUserInstalledPackages(System.Environment.UserName);
        }


        /// <summary>
        /// Migrates 0.5.0 Add-In library format to the latest format
        /// </summary>
        /// <param name="sourceFolder"></param>
        /// <param name="destFolder"></param>
        public static void MigrateLibrary(string sourceFolder, string destFolder)
        {

            //checks source and destination are not equal or blank
            if (string.IsNullOrEmpty(sourceFolder)) throw new NotImplementedException("sourceFolder");
            if (string.IsNullOrEmpty(destFolder)) throw new NotImplementedException("destFolder");
            if (sourceFolder == destFolder) throw new ArgumentException("Destination folder can not be the same location as source folder", "destFolder");

            //creates a new destination library
            FolderPackageLibrary.CreateNew(destFolder);
            FolderPackageLibrary newlib = new FolderPackageLibrary(destFolder, false);


            //iterates over all RPK files
            System.IO.DirectoryInfo sourceDir = new DirectoryInfo(sourceFolder);
            FileInfo[] rpkFiles = sourceDir.GetFiles("*.rpk", SearchOption.AllDirectories);


            foreach (FileInfo orig_fInfo in rpkFiles)
            {

                using (var orig_ms = new MemoryStream())
                {


                    using (var orig_fs = new FileStream(orig_fInfo.FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {

                        orig_fs.CopyTo(orig_ms);

                        orig_ms.Position = 0;


                        var orig_zip = Ionic.Zip.ZipFile.Read(orig_ms);


                        var packageXMLEntry = orig_zip["Package.xml"];


                        string packageCode;
                        string packageName;
                        string packageNotes = null;
                        string versionName;
                        int versionNumber;
                        string versionNotes = null;
                        int minRelease;
                        string author = null;
                        string email = null;
                        Uri link = null;


                        using (var origPkgXml_ms = new MemoryStream())
                        {

                            packageXMLEntry.Extract(origPkgXml_ms);

                            origPkgXml_ms.Position = 0;


                            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                            xmlDoc.Load(origPkgXml_ms);

                            packageCode = xmlDoc.SelectSingleNode("/Package/PackageCode").InnerText;
                            packageName = xmlDoc.SelectSingleNode("/Package/PackageName").InnerText;
                            versionName = xmlDoc.SelectSingleNode("/Package/VersionNumber").InnerText;
                            versionNumber = System.Convert.ToInt32(xmlDoc.SelectSingleNode("/Package/VersionCode").InnerText);
                            minRelease = System.Convert.ToInt32(xmlDoc.SelectSingleNode("/Package/RevitRelease").InnerText);


                            var packageNotes_node = xmlDoc.SelectSingleNode("/Package/PackageNotes");
                            if(packageNotes_node != null) packageNotes = packageNotes_node.InnerText;


                            var versionNotes_node = xmlDoc.SelectSingleNode("/Package/VersionNotes");
                            if (versionNotes_node != null) versionNotes = versionNotes_node.InnerText;


                            var author_node = xmlDoc.SelectSingleNode("/Package/Author");
                            if (author_node != null) author = author_node.InnerText;


                            var email_node = xmlDoc.SelectSingleNode("/Package/Email");
                            if (email_node != null) email = email_node.InnerText;


                            var link_node = xmlDoc.SelectSingleNode("/Package/Link");
                            if (link_node != null && !String.IsNullOrEmpty(link_node.InnerText))
                            {
                                link = new Uri(link_node.InnerText);
                            }

                             
                            
                            
                            

                            


                        }


                        string newPackageSourceFolder = Path.Combine(Path.GetTempPath(), packageCode + "_" + versionName);


                        try
                        {

                            Directory.CreateDirectory(newPackageSourceFolder);


                            foreach (var entry in orig_zip)
                            {
                                if (entry.FileName != "Package.xml" & entry.FileName != "package.xml")
                                {
                                    entry.Extract(newPackageSourceFolder);
                                }
                            }

                            Package newPackage = new Package(packageCode, packageName, packageNotes, versionName, versionNumber, versionNotes, minRelease, null, author, email, link);

                            using (var new_ms = new MemoryStream())
                            {
                                //build new package
                                Package.BuildPackage(new_ms, newPackageSourceFolder, newPackage);


                                //rest stream
                                new_ms.Position = 0;


                                //add package to new library
                                newlib.AddPackage(new_ms);
                            }



                        }
                        finally
                        {
                            if (Directory.Exists(newPackageSourceFolder))
                            {
                                Directory.Delete(newPackageSourceFolder,true);
                            }
                        }

                    }
                }
            }
        }

    }
}



