﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Input;

namespace RevitAddInLibrary.Installer
{

    public class MockLibrary : IPackageLibrary
    {

        public string Location
        {
            get { return "Current Library Location"; }
        }

        public void AddPackage(System.IO.Stream packageData)
        {
            throw new NotImplementedException();
        }

        public void DeletePackage(string packageCode, string versionNumber)
        {
            throw new NotImplementedException();
        }

        public System.IO.Stream GetPackageData(string packageCode, string versionNumber)
        {
            throw new NotImplementedException();
        }

        public void UpdatePackage(LibraryPackage updatedPackage)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetPackageCodes()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LibraryPackage> GetPackages()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LibraryPackage> GetPackages(int? release, string packageCode)
        {
            throw new NotImplementedException();
        }

        public LibraryPackage GetPackage(string packageCode, string versionNumber)
        {
            throw new NotImplementedException();
        }

        public LibraryPackage GetLatestPublishedVersion(string packageCode, int revitRelease)
        {
            throw new NotImplementedException();
        }
    }

    class MockInstallerWindowViewModel
    {

        public IPackageLibrary CurrentLibrary
        {
            get;
            private set;
        }

        public PackageViewModel SelectedPackage { get; set; }


        

        private ObservableCollection<PackageViewModel> _packageList;

        public ICollectionView Packages { get; private set; }



        private Package GetSamplePackage(string packageCode, string packageName, int version, int targetRelease)
        { 
        
            return new Package(packageCode, packageName, "Package Notes" + version.ToString(), version.ToString() + ".0", version, "Version notes " + version.ToString(), targetRelease, null, "Author " + version.ToString(), "email@acme.com", new Uri("http://www.acme.com"));
        
        }


        private LibraryPackage GetSampleLibPackage(string packageCode, string packageName, int version, int targetRelease, VersionStatus libStatus)
        {

            string notes = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eleifend nibh eu tortor consectetur, eget egestas est sodales. Nulla facilisi. Suspendisse tincidunt tempus placerat. Aliquam erat volutpat. Sed id magna quis est pulvinar aliquet. Phasellus tincidunt sodales diam, sed vestibulum libero lobortis id. Integer ut eros in urna adipiscing interdum at sit amet neque. Cras nunc urna, tempus a justo quis, egestas vestibulum erat. Nunc ut velit sapien. Sed consectetur tellus et nisi rutrum, a mollis nulla scelerisque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum pharetra dapibus felis, et lobortis ligula gravida in. Curabitur quis urna elit. Vestibulum a placerat turpis, ut tristique diam.";



            Package package = new Package(packageCode, packageName, notes, version.ToString() + ".0", version, "Version notes " + version.ToString(), targetRelease, targetRelease+1, "Author " + version.ToString(), "email@acme.com", new Uri("http://www.acme.com"));

            LibraryPackage libPack = new LibraryPackage(package, libStatus, "Added By", DateTime.Now.ToUniversalTime(), 0);

            return libPack;
        
        }



        public MockInstallerWindowViewModel()
        {

            this.CurrentLibrary = new MockLibrary();
            this._packageList = new ObservableCollection<PackageViewModel>();
            
            int targetRelease = 2013;



            //Not Installed Published
            List<LibraryPackage> libPackList1 = new List<LibraryPackage>();
            libPackList1.Add(GetSampleLibPackage("package1","Not Installed Published",1,targetRelease,VersionStatus.Published));
            libPackList1.Add(GetSampleLibPackage("package1", "Not Installed Published", 2, targetRelease, VersionStatus.Published));
            libPackList1.Add(GetSampleLibPackage("package1", "Not Installed Published", 3, targetRelease, VersionStatus.Published));
            libPackList1.Add(GetSampleLibPackage("package1", "Not Installed Published", 4, targetRelease, VersionStatus.Published));

            PackageViewModel packVM1 = new PackageViewModel("package1", targetRelease, libPackList1, false);
            _packageList.Add(packVM1);


            //Not Installed Published
            List<LibraryPackage> libPackList2 = new List<LibraryPackage>();
            libPackList2.Add(GetSampleLibPackage("package2", "Not Installed Unpublished", 1, targetRelease, VersionStatus.Unpublished));
            PackageViewModel packVM2 = new PackageViewModel("package2", targetRelease, libPackList2, false);
            packVM2.VersionStatusFilter = VersionStatus.Unpublished;
            _packageList.Add(packVM2);

            //Not Installed Published
            List<LibraryPackage> libPackList3 = new List<LibraryPackage>();
            libPackList3.Add(GetSampleLibPackage("package3", "Not Installed Suspended", 1, targetRelease, VersionStatus.Suspended));
            PackageViewModel packVM3 = new PackageViewModel("package3", targetRelease, libPackList3, false);
            packVM3.VersionStatusFilter = VersionStatus.Suspended;
            _packageList.Add(packVM3);



            //Installed From File/Not In library
            var package4 = GetSamplePackage("package4","Installed From File",1,targetRelease);
            PackageViewModel packVM4 = new PackageViewModel("package4", targetRelease, package4, false);
            _packageList.Add(packVM4);



            //Installed Published
            List<LibraryPackage> libPackList5 = new List<LibraryPackage>();
            libPackList5.Add(GetSampleLibPackage("package5", "Installed Published", 1, targetRelease, VersionStatus.Published));
            libPackList5.Add(GetSampleLibPackage("package5", "Installed Published", 2, targetRelease, VersionStatus.Published));
            libPackList5.Add(GetSampleLibPackage("package5", "Installed Published", 3, targetRelease, VersionStatus.Published));
            libPackList5.Add(GetSampleLibPackage("package5", "Installed Published", 4, targetRelease, VersionStatus.Published));

            PackageViewModel packVM5 = new PackageViewModel("package5", targetRelease, libPackList5, false);
            
            packVM5.SetInstalled(libPackList5[3].Package);
            _packageList.Add(packVM5);




            //Installed Published Upgrade
            List<LibraryPackage> libPackList6 = new List<LibraryPackage>();
            libPackList6.Add(GetSampleLibPackage("package6", "Installed Published Upgrade", 1, targetRelease, VersionStatus.Published));
            libPackList6.Add(GetSampleLibPackage("package6", "Installed Published Upgrade", 2, targetRelease, VersionStatus.Published));
            libPackList6.Add(GetSampleLibPackage("package6", "Installed Published Upgrade", 3, targetRelease, VersionStatus.Published));
            libPackList6.Add(GetSampleLibPackage("package6", "Installed Published Upgrade", 4, targetRelease, VersionStatus.Published));

            PackageViewModel packVM6 = new PackageViewModel("package6", targetRelease, libPackList6, false);

            packVM6.SetInstalled(libPackList6[2].Package);
            _packageList.Add(packVM6);





            //Installed Unpublished Downgrade
            List<LibraryPackage> libPackList7 = new List<LibraryPackage>();
            libPackList7.Add(GetSampleLibPackage("package7", "Installed Unpublished Downgrade", 1, targetRelease, VersionStatus.Published));
            libPackList7.Add(GetSampleLibPackage("package7", "Installed Unpublished Downgrade", 2, targetRelease, VersionStatus.Published));
            libPackList7.Add(GetSampleLibPackage("package7", "Installed Unpublished Downgrade", 3, targetRelease, VersionStatus.Unpublished));
            libPackList7.Add(GetSampleLibPackage("package7", "Installed Unpublished Downgrade", 4, targetRelease, VersionStatus.Unpublished));

            PackageViewModel packVM7 = new PackageViewModel("package7", targetRelease, libPackList7, false);

            packVM7.VersionStatusFilter = VersionStatus.Suspended;

            packVM7.SetInstalled(libPackList7[3].Package);
            _packageList.Add(packVM7);




            //Suspended Installed
            List<LibraryPackage> libPackList8 = new List<LibraryPackage>();
            libPackList8.Add(GetSampleLibPackage("package8", "Suspended Installed", 1, targetRelease, VersionStatus.Suspended));
            libPackList8.Add(GetSampleLibPackage("package8", "Suspended Installed", 2, targetRelease, VersionStatus.Suspended));
            

            PackageViewModel packVM8 = new PackageViewModel("package8", targetRelease, libPackList8, false);

            packVM8.VersionStatusFilter = VersionStatus.Suspended;

            packVM8.SetInstalled(libPackList8[0].Package);
            _packageList.Add(packVM8);

            

            
            


            Packages = CollectionViewSource.GetDefaultView(_packageList);

            SelectedPackage = _packageList[5];

            Packages.GroupDescriptions.Clear();
            Packages.GroupDescriptions.Add(new PropertyGroupDescription("TargetRelease"));
            //Packages.GroupDescriptions.Add(new PropertyGroupDescription("IsInstalled"));
            

            
        }
    }
}
