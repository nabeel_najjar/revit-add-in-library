﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RevitAddInLibrary.Installer
{
    public interface IDialogService
    {
        void ShowDialog(string windowTitle, string mainInstruction, string content, DialogIcon icon);

        void ShowDialog(string windowTitle, string mainInstruction, Exception error, DialogIcon icon);

        T ShowDialog<T>(T viewModel);
    }

    public enum DialogIcon
    {
        Information,
        Warning,
        Error

    }
}
