﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using RevitAddInLibrary;
using System.Windows.Data;

using System.Globalization;

namespace RevitAddInLibrary.Installer
{
    public class InstallerWindowViewModel : INotifyPropertyChanged
    {



        private IList<RequiredPackage> _requiredPackages;
        private IPackageInstaller _installer;
        IList<int> _installedReleases;
        private IRevitApplicationUtil _revitAppUtil;
        private IDialogService _dialogService;





        private IPackageLibrary _library;
        public IPackageLibrary CurrentLibrary
        {
            get
            {
                return _library;
            }
        }





        #region Commands

        public ICommand InstallFromFileCommand { get; private set; }
        public ICommand RefreshListCommand { get; private set; }


        public ICommand InstallCommand { get; private set; }
        public ICommand UninstallCommand { get; private set; }


        public ICommand UpdateAllCommand { get; private set; }


        private void InstallFromFile(object o)
        {
            if (_installedReleases.Count == 0)
            {

                _dialogService.ShowDialog("Error", "No installations of Revit were detected.", String.Empty, DialogIcon.Error);
            }
            else
            {
                InstallFileWindow insFileWindow = new InstallFileWindow(_installedReleases);

                var res = insFileWindow.ShowDialog();


                if (res.HasValue && res.Value)
                {
                    //Installs the file
                    InstallResult result = _installer.Install(insFileWindow.SelectedRelease, insFileWindow.AddInFilePath);


                    var installedPackage = result.Current;


                    //searches for existing view model
                    var packageVM = _packageList.Where(p => p.PackageCode == installedPackage.PackageCode & p.TargetRelease == insFileWindow.SelectedRelease).FirstOrDefault();



                    if (packageVM != null)
                    {
                        packageVM.SetInstalled(installedPackage);
                    }
                    else
                    {

                        bool isRequired = IsRequired(insFileWindow.SelectedRelease, installedPackage.PackageCode);

                        PackageViewModel newVm = new PackageViewModel(installedPackage.PackageCode, insFileWindow.SelectedRelease, installedPackage, isRequired);

                        _packageList.Add(newVm);




                    }



                }










            }







        }


        private void RefreshList(object o)
        {


            //saves current selection
            int selectedRelease = 0;
            string selectedPackageCode = null;

            if (this.SelectedPackage != null)
            {
                selectedPackageCode = this.SelectedPackage.PackageCode;
                selectedRelease = this.SelectedPackage.TargetRelease;
            }



            this.RefreshPackages();



            if (!String.IsNullOrEmpty(selectedPackageCode))
            {

                var pack = Packages.Cast<PackageViewModel>().Where(p => p.PackageCode == selectedPackageCode & p.TargetRelease == selectedRelease).FirstOrDefault();

                this.SelectedPackage = pack;




            }

        }



        private bool CanUninstall(InstallCommandData obj)
        {
            if (obj == null) return false;







            if (IsRequired(obj.TargetRevitRelease, obj.Package.PackageCode))
            {
                return false;
            }
            else
            {
                return true;
            }


        }


        private void Uninstall(InstallCommandData cmdData)
        {
            try
            {
                if (_revitAppUtil.IsRevitRunning(cmdData.TargetRevitRelease))
                {
                    ShowRevitRunningError(cmdData.TargetRevitRelease);
                    return;
                }




                _installer.Uninstall(cmdData.TargetRevitRelease, cmdData.Package.PackageCode);


                //finds the view model of the uninstalled package
                var packageVM = _packageList.Where(p => p.PackageCode == cmdData.Package.PackageCode & p.TargetRelease == cmdData.TargetRevitRelease).FirstOrDefault();


                if (packageVM != null)
                {
                    packageVM.SetInstalled(null);

                    if (packageVM.OfficialLibraryVersion == null)
                    {
                        //if there are no official library version to install remove from list
                        _packageList.Remove(packageVM);
                    }
                    else
                    {
                        //Otherwise refresh the the list which reapplies the filters
                        Packages.Refresh();
                    }
                }




            }
            catch (Exception exp)
            {

                _dialogService.ShowDialog("Error", "There was a problem uninstalling the package", exp, DialogIcon.Error);
            }
        }




        private bool CanInstall(InstallCommandData obj)
        {
            if (obj == null) return false;

            if (IsRequired(obj.TargetRevitRelease, obj.Package.PackageCode))
            {

                var packageVM = _packageList.Where(p => p.PackageCode == obj.Package.PackageCode & p.TargetRelease == obj.TargetRevitRelease).FirstOrDefault();


                if (packageVM.OfficialLibraryVersion.Package.VersionName == obj.Package.VersionName)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return true;

        }

        private void Install(InstallCommandData cmdData)
        {
            try
            {
                var packageVM = _packageList.Where(p => p.PackageCode == cmdData.Package.PackageCode & p.TargetRelease == cmdData.TargetRevitRelease).FirstOrDefault();

                if (packageVM != null && packageVM.IsInstalled && _revitAppUtil.IsRevitRunning(cmdData.TargetRevitRelease))
                {
                    ShowRevitRunningError(cmdData.TargetRevitRelease);
                    return;
                }

                _installer.Install(_library, cmdData.TargetRevitRelease, cmdData.Package.PackageCode, cmdData.Package.VersionName);


                packageVM.SetInstalled(cmdData.Package);
            }
            catch (Exception exp)
            {
                _dialogService.ShowDialog("Error", "There was a problem installing the package", exp, DialogIcon.Error);
            }
        }

        private void ShowRevitRunningError(int p)
        {
            _dialogService.ShowDialog("Revit Running", "Revit.exe is running.", "Revit add-ins can not be uninstalled or reinstalled while Revit is running. Please close Revit and try again.", DialogIcon.Warning);
        }


        /// <summary>
        /// Clears the entire list, reads the installed packages and the library if available, and updates the package list. 
        /// </summary>
        public void RefreshPackages()
        {
            try
            {
                List<PackageViewModel> newPackageViewModelList = new List<PackageViewModel>();

                //Find all installed packages
                var installed = _installer.GetInstalledPackages().ToList();


                //Generates list of releases to show based on what is installed


                //check if library is available
                if (_library != null)
                {
                    var allLibPackages = _library.GetPackages().ToList();

                    foreach (var packageGroup in allLibPackages.GroupBy(p => p.Package.PackageCode))
                    {
                        foreach (int currentRelease in this._installedReleases)
                        {


                            string packageCode = packageGroup.Key;
                            bool required = IsRequired(currentRelease, packageCode);


                            List<LibraryPackage> currentLibPackages = new List<LibraryPackage>();


                            foreach (var libPack in packageGroup)
                            {
                                //checks each library package to see if it's valid for currentRelease
                                if (libPack.Package.MinRevitRelease <= currentRelease)
                                {
                                    if (libPack.Package.MaxRevitRelease.HasValue)
                                    {
                                        if (libPack.Package.MaxRevitRelease >= currentRelease)
                                        {
                                            currentLibPackages.Add(libPack);
                                        }
                                    }
                                    else
                                    {
                                        currentLibPackages.Add(libPack);
                                    }
                                }
                            }

                            //creates a new PackageViewModel
                            if (currentLibPackages.Count > 0)
                            {
                                PackageViewModel newViewModel = new PackageViewModel(packageCode, currentRelease, currentLibPackages, required);

                                newPackageViewModelList.Add(newViewModel);
                            }
                        }
                    }
                }





                foreach (var installedPackage in _installer.GetInstalledPackages())
                {
                    PackageViewModel pViewModel = newPackageViewModelList.Where(p => p.PackageCode == installedPackage.Package.PackageCode && p.TargetRelease == installedPackage.RevitRelease).FirstOrDefault();

                    if (pViewModel == null)
                    {
                        pViewModel = new PackageViewModel(installedPackage.Package.PackageCode, installedPackage.RevitRelease, installedPackage.Package, IsRequired(installedPackage.RevitRelease, installedPackage.Package.PackageCode));
                        newPackageViewModelList.Add(pViewModel);
                    }


                    pViewModel.SetInstalled(installedPackage.Package);
                }




                _packageList.Clear();
                foreach (var vm in newPackageViewModelList) _packageList.Add(vm);

                Packages.Refresh();
            }
            catch (Exception exp)
            {
                _dialogService.ShowDialog("Error", "There was a problem refreshing the package list", exp, DialogIcon.Error);
            }
        }

        private bool IsRequired(int currentRelease, string packageCode)
        {
            if (_requiredPackages.Where(r => r.PackageCode == packageCode && r.RevitRelease == currentRelease).Count() > 0) return true;
            else return false;
        }


        private bool CanUpdateAll(object obj)
        {
            bool canUpdateAll = false;

            foreach (var vm in _packageList)
            {

                if (vm.IsInstalled
                    && vm.OfficialLibraryVersion != null
                    && vm.OfficialLibraryVersion.Package.VersionNumber > vm.InstalledVersion.Package.VersionNumber)
                {
                    canUpdateAll = true;
                    break;
                }
            }

            return canUpdateAll;
        }

        private void UpdateAll(object obj)
        {
            try
            {

                //update components
                foreach (var vm in _packageList)
                {

                    if (vm.IsInstalled
                        && vm.OfficialLibraryVersion != null
                        && vm.OfficialLibraryVersion.Package.VersionNumber > vm.InstalledVersion.Package.VersionNumber)
                    {


                        InstallCommandData data = new InstallCommandData(vm.TargetRelease, vm.OfficialLibraryVersion.Package);

                        this.Install(data);

                    }
                }

            }
            catch (Exception exp)
            {



                _dialogService.ShowDialog("Error", "There was a problem running the update", exp, DialogIcon.Error);
            }



        }




        #endregion


        #region Constructor

        /// <summary>
        /// Constructor for InstallerWindowViewModel
        /// </summary>
        /// <param name="library">The library to read available add-ins from. This value can be null, in which case the tool only displays the installed add-ins</param>
        /// <param name="installer"></param>
        /// <exception cref="ArgumentNullException">installer can not be null</exception>
        public InstallerWindowViewModel(IPackageLibrary library,
            IPackageInstaller installer,
            IEnumerable<RequiredPackage> requiredPackages,
            IRevitApplicationUtil revitAppUtil,
            IDialogService diagService)
        {
            //installer can not be null, tool doesn't do much if it can't see what's installed or uninstall
            if (installer == null) throw new ArgumentNullException("installer");
            if (revitAppUtil == null) throw new ArgumentNullException("revitAppUtil");
            if (diagService == null) throw new ArgumentNullException("diagService");

            this._installer = installer;
            this._library = library;
            this._revitAppUtil = revitAppUtil;
            this._dialogService = diagService;

            //Sets Required packages
            if (requiredPackages != null)
            {
                _requiredPackages = new List<RequiredPackage>(requiredPackages);
            }
            else
            {
                _requiredPackages = new List<RequiredPackage>();
            }

            //sets installed release
            _installedReleases = revitAppUtil.GetInstalledRevitReleases().Distinct().OrderBy(r => r).ToList();


            //sets delegates





            //wires commands
            this.InstallFromFileCommand = new RelayCommand(new Action<object>(this.InstallFromFile));
            this.RefreshListCommand = new RelayCommand(new Action<object>(this.RefreshList));
            this.InstallCommand = new RelayCommand<InstallCommandData>(new Action<InstallCommandData>(this.Install), new Predicate<InstallCommandData>(this.CanInstall));
            this.UninstallCommand = new RelayCommand<InstallCommandData>(new Action<InstallCommandData>(this.Uninstall), new Predicate<InstallCommandData>(this.CanUninstall));
            this.UpdateAllCommand = new RelayCommand(new Action<object>(this.UpdateAll), new Predicate<object>(this.CanUpdateAll));



            //Sets up package list
            this._packageList = new ObservableCollection<PackageViewModel>();
            Packages = CollectionViewSource.GetDefaultView(_packageList);


            using (var refresh = Packages.DeferRefresh())
            {
                Packages.Filter = new Predicate<object>(this.FilterPackageList);
                Packages.SortDescriptions.Add(new SortDescription("TargetRelease", ListSortDirection.Ascending));
                Packages.SortDescriptions.Add(new SortDescription("PackageName", ListSortDirection.Ascending));
                Packages.GroupDescriptions.Add(new PropertyGroupDescription("TargetRelease"));
            }



            this.RefreshPackages();
        }


        #endregion


        #region Package List Members

        private ObservableCollection<PackageViewModel> _packageList;

        public ICollectionView Packages { get; private set; }



        private PackageViewModel _selectedPackage;

        public PackageViewModel SelectedPackage
        {

            get { return _selectedPackage; }

            set
            {

                if (value != _selectedPackage)
                {
                    _selectedPackage = value;

                    OnPropertyChanged("SelectedPackage");
                }

            }
        }



        private bool FilterPackageList(object o)
        {
            PackageViewModel vm = (PackageViewModel)o;

            if (!String.IsNullOrEmpty(SearchText))
            {
                if (!PassesTextSearch(vm))
                {
                    return false;
                }
            }

            if (vm.IsInstalled)
            {
                return true;
            }

            if (ShowUnpublished)
            {

                if (vm.PackageStatus == VMVersionStatus.Published | vm.PackageStatus == VMVersionStatus.Unpublished)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (vm.PackageStatus == VMVersionStatus.Published)
                {

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        //Determines if 
        private bool PassesTextSearch(PackageViewModel vm)
        {

            var sep = new char[] { ' ' };

            var searchTerms = SearchText.Split(sep, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Trim());




            //each search term must be found


            StringBuilder sb = new StringBuilder();

            try
            {
                sb.Append(vm.PackageCode);
                sb.Append(vm.PackageName);


                if (vm.PackageNotes != null) sb.Append(vm.PackageNotes);
                if (vm.Author != null) sb.Append(vm.Author);
                if (vm.Email != null) sb.Append(vm.Email);
                if (vm.Link != null) sb.Append(vm.Link.OriginalString);
            }
            catch (Exception)
            {

                throw;
            }

            string combinedString = sb.ToString();



            foreach (var term in searchTerms)
            {
                int index = CultureInfo.InvariantCulture.CompareInfo.IndexOf(combinedString, term, CompareOptions.IgnoreCase);

                if (index >= 0)
                {
                    continue;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        #endregion


        #region UI Properties





        private string _searchText;
        public string SearchText
        {
            get
            {
                return _searchText;
            }
            set
            {
                if (_searchText != value)
                {
                    _searchText = value;
                    OnPropertyChanged("SearchText");

                    //filters packages
                    Packages.Refresh();
                }
            }
        }


        private bool _showUnpublished;
        public bool ShowUnpublished
        {
            get
            {
                return _showUnpublished;
            }
            set
            {
                if (_showUnpublished != value)
                {
                    _showUnpublished = value;



                    foreach (PackageViewModel pVM in _packageList)
                    {
                        if (value) pVM.VersionStatusFilter = VersionStatus.Unpublished;
                        else pVM.VersionStatusFilter = VersionStatus.Published;
                    }

                    OnPropertyChanged("ShowUnpublished");

                    Packages.Refresh();
                }
            }
        }









        #endregion









        #region INotifyPropertyChanged Members


        public event PropertyChangedEventHandler PropertyChanged;



        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion



    }
}

