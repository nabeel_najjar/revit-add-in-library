﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using RevitAddInLibrary;
using NLog;
using System.IO;
using Ookii.Dialogs.Wpf;
using System.Windows.Input;

namespace RevitAddInLibrary.Installer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {


        private static Logger logger = LogManager.GetCurrentClassLogger();

        private void Application_Startup(object sender, StartupEventArgs e)
        {

            PackageInstaller pkInstaller = new PackageInstaller(new RevitApplicationUtil());

            IDialogService dialogService = new DialogService();

            IRevitApplicationUtil revitAppUtil = new RevitApplicationUtil();
            

            string libraryLocation = ConfigurationManager.AppSettings["Library"];

            IPackageLibrary library = null;

            if (!String.IsNullOrEmpty(libraryLocation))
            {
                if (Directory.Exists(libraryLocation))
                {
                    try
                    {
                        library = new FolderPackageLibrary(libraryLocation, true);
                    }
                    catch (Exception exp)
                    {
                        logger.Error(exp);

                        dialogService.ShowDialog("Library Error", "There was a problem opening the library", exp, DialogIcon.Error);

                        library = null;
                    }
                }
                else
                {
                    dialogService.ShowDialog("Library Error", "A Revit add-in library was not found in the configured location", libraryLocation, DialogIcon.Warning);
                    library = null;
                }
            }



            IList<RequiredPackage> reqList = null;

            string reqListLoc = ConfigurationManager.AppSettings["RequiredList"];


            if (!Keyboard.IsKeyDown(Key.LeftShift) & !Keyboard.IsKeyDown(Key.RightShift))
            {
                if (!String.IsNullOrEmpty(reqListLoc))
                {
                    //attempts to read required package lis
                    //silently fails if there is an issue
                    if (File.Exists(reqListLoc))
                    {
                        try
                        {
                            reqList = RevitAddInLibrary.RequiredPackage.ReadRequiredPackageList(reqListLoc);
                        }
                        catch (Exception exp)
                        {
                            logger.Error(exp);
                        }
                    }
                }
            }











            //sets the window datacontext
            var windowVM = new InstallerWindowViewModel(
                library,
                pkInstaller,
                reqList,
                revitAppUtil,
                dialogService);





            this.MainWindow = new InstallerWindow(windowVM);
            this.MainWindow.Show();

            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledExceptionHandler);
        }

        private void ex_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            logger.Error(e.Exception);
        }

        static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs args)
        {
            logger.Error(args.ExceptionObject);
        }
    }
}


