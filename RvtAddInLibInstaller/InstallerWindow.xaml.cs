﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RevitAddInLibrary;
using System.Windows.Navigation;
using System.Diagnostics;
using NLog;
using System.Configuration;
using Ookii.Dialogs.Wpf;



namespace RevitAddInLibrary.Installer
{

    public partial class InstallerWindow : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public InstallerWindow(InstallerWindowViewModel viewModel)
        {
            this.InitializeComponent();

           
            this.DataContext = viewModel;

        }






        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.OriginalString));
            e.Handled = true;
        }


        private void Hyperlink_RequestEmailNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo("mailto:" + e.Uri));
            e.Handled = true;
        }


        private static void ShowErrorDialog(string message, Exception exp)
        {
            Ookii.Dialogs.Wpf.TaskDialog diag = new Ookii.Dialogs.Wpf.TaskDialog();

            TaskDialogButton okButton = new TaskDialogButton(ButtonType.Ok);

            diag.MainIcon = TaskDialogIcon.Error;

            diag.Buttons.Add(okButton);

            diag.MainInstruction = message;

            diag.Content = exp.Message;

            diag.ExpandedByDefault = false;

            diag.ExpandFooterArea = true;

            diag.ExpandedInformation = exp.ToString();


            diag.ShowDialog();
        }


        private void ShowRevitRunningError()
        {
            Ookii.Dialogs.Wpf.TaskDialog diag = new Ookii.Dialogs.Wpf.TaskDialog();

            TaskDialogButton okButton = new TaskDialogButton(ButtonType.Ok);

            diag.Buttons.Add(okButton);

            diag.MainIcon = TaskDialogIcon.Error;

            diag.WindowTitle = "Revit.exe Process Found";
            diag.MainInstruction = "Revit.exe Process Found";
            diag.Content = "Revit add-ins can not be installed or uninstalled while Revit is running.\n\n Please close Revit before modifying Revit Add-Ins.";

            diag.ExpandedByDefault = false;

            diag.ShowDialog();
        }


    

    }
}
