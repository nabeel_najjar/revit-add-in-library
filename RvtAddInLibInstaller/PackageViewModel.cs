﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Input;

namespace RevitAddInLibrary.Installer
{

    public enum InstallButtonType
    {
        Install,
        Update,
        Downgrade
    }

    public class PackageViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Package Data
        /// </summary>
        /// 
        #region Immutable Properties

        public VMVersionStatus PackageStatus { get; private set; }
        public string PackageCode { get; private set; }
        public int TargetRelease { get; private set; }
        public bool IsRequired { get; set; }


        /// <summary>
        /// This property stores the official version of the package that should be installed from the library.
        /// <remarks>
        /// This is determined by the list of LibraryPackages sent to the constructor. 
        /// If there are published versions it will be the latest published version. 
        /// If there are no published versions it will be the latest non published version.
        /// If there are no unpublished versions it will be the latest suspended version.
        /// </remarks>
        /// </summary>
        public PackageVersionViewModel OfficialLibraryVersion { get; private set; }


        #endregion



        #region Package Data Properties




        /// <summary>
        // Controls what status is show in the package panel. If no package is installedd shows PackageViewModel.Status 
        //otherwise shows InstalledVersion.Status
        /// </summary>
        private VMVersionStatus _displayStatus;
        public VMVersionStatus DisplayStatus
        {
            get
            {
                return _displayStatus;
            }
            set
            {
                if (_displayStatus != value)
                {
                    _displayStatus = value;

                    OnPropertyChanged("DisplayStatus");
                }
            }
        }




        //The values are usually set to the values from the official library version
        //Or if installing package not in the library will show the values from the installed version

        private string _packageName;
        public string PackageName
        {
            get
            {
                return _packageName;
            }
            private set
            {
                if (_packageName != value)
                {
                    _packageName = value;
                    OnPropertyChanged("PackageName");
                }
            }
        }

        private string _author;
        public string Author
        {
            get
            {
                return _author;
            }
            private set
            {
                if (_author != value)
                {
                    _author = value;
                    OnPropertyChanged("Author");
                }
            }
        }

        private string _email;
        public string Email
        {
            get
            {
                return _email;
            }
            private set
            {
                if (_email != value)
                {
                    _email = value;
                    OnPropertyChanged("Email");
                }
            }
        }

        private Uri _link;
        public Uri Link
        {
            get
            {
                return _link;
            }
            private set
            {
                if (_link != value)
                {
                    _link = value;
                    OnPropertyChanged("Link");
                }
            }
        }

        private string _packageNotes;
        public string PackageNotes
        {
            get
            {
                return _packageNotes;
            }
            private set
            {
                if (_packageNotes != value)
                {
                    _packageNotes = value;
                    OnPropertyChanged("PackageNotes");
                }
            }
        }

        /// <summary>
        /// Sets the package meta data displayed
        /// </summary>
        /// <param name="p"></param>
        private void SetMetaData(Package p)
        {
            if (p != null)
            {
                if (p.PackageCode != PackageCode) throw new InvalidOperationException("PackageCodes do not match");

                this.PackageName = p.PackageName;
                this.PackageNotes = p.PackageNotes;
                this.Author = p.Author;
                this.Email = p.Email;
                this.Link = p.Link;
            }
            else
            {
                this.PackageName = null;
                this.PackageNotes = null;
                this.Author = null;
                this.Email = null;
                this.Link = null;
            }
        }

        #endregion

        /// <summary>
        /// This is a list of all the available versions of the package
        /// </summary>
        public ICollectionView PackageVersionsView { get; private set; }
        public ObservableCollection<PackageVersionViewModel> PackageVersions { get; private set; }


        /// <summary>
        /// Changes the installed version displayed
        /// </summary>
        /// <remarks>
        /// This sets the InstalledVersion property and updates all the UI related properties
        /// If a matching version is found in PackageVersions Installed version is set to that
        /// Otherwise it assumes that the version being installed is not in the library and creates a new PackageVersionView model with Status=Unknown
        /// 
        /// If an existing Unknow Status package is installed it is removed after adding the next
        /// 
        /// When the package is uninstalled this method should be passed null to clear the InstalledVersion property and update the UI
        /// </remarks>
        /// <param name="installedVersion">The name of the version installed, or null if the package has been uninstalled.</param>
        public void SetInstalled(Package installedVersion)
        {
            if (installedVersion != null)
            {



                //verify installedVersion is valid
                if (installedVersion.PackageCode != this.PackageCode) throw new InvalidOperationException("installedVersion.PackageCode does not match PackageViewModel.PackageCode");
                if (this.TargetRelease < installedVersion.MinRevitRelease) throw new ArgumentOutOfRangeException("PackageViewModel.TargetRelease < installedVersion.MinRevitRelease");
                if (installedVersion.MaxRevitRelease.HasValue && this.TargetRelease > installedVersion.MaxRevitRelease) throw new ArgumentOutOfRangeException("PackageViewModel.TargetRelease > installedVersion.MaxRevitRelease");


                //Clears existing installed version
                if (InstalledVersion != null)
                {
                    //Trying to reinstall the same version return
                    if (InstalledVersion.Package.VersionName == installedVersion.VersionName) return;

                    this.InstalledVersion.IsInstalled = false;

                    //removes version if not in library
                    if (InstalledVersion.Status == VMVersionStatus.Unknown) this.PackageVersions.Remove(InstalledVersion);
                }



                //Fines the matching version vm
                var newInstalledVersionVM = this.PackageVersions.Where(p => p.Package.VersionName == installedVersion.VersionName).FirstOrDefault();

                if (newInstalledVersionVM == null)
                {
                    //creates a new view model
                    newInstalledVersionVM = new PackageVersionViewModel(installedVersion, this.TargetRelease, VMVersionStatus.Unknown);
                    newInstalledVersionVM.IsInstalled = true;


                    if (PackageVersions.Count == 0) SetMetaData(installedVersion);


                    this.PackageVersions.Add(newInstalledVersionVM);
                }
                else
                {
                    newInstalledVersionVM.IsInstalled = true;
                }



                this.InstalledVersion = newInstalledVersionVM;

                this.DisplayStatus = InstalledVersion.Status;

                IsInstalled = true;
                ShowUninstallButton = true;




                //set InstallButton
                if (OfficialLibraryVersion != null)
                {
                    if (OfficialLibraryVersion.Package.VersionName == InstalledVersion.Package.VersionName)
                    {
                        //hide install button if published version is installed
                        this.InstallButtonType = Installer.InstallButtonType.Install;
                        this.ShowInstallButton = false;
                    }
                    else
                    {
                        this.ShowInstallButton = true;

                        if (OfficialLibraryVersion.Package.VersionNumber > installedVersion.VersionNumber) this.InstallButtonType = Installer.InstallButtonType.Update;
                        else if (OfficialLibraryVersion.Package.VersionNumber < installedVersion.VersionNumber) this.InstallButtonType = Installer.InstallButtonType.Downgrade;

                        //Installed version is a different version then the offical, but has the same version code
                        else this.InstallButtonType = Installer.InstallButtonType.Update;
                    }
                }
                else
                {

                    //hide the install button if there are no published versions to install
                    ShowInstallButton = false;
                    InstallButtonType = Installer.InstallButtonType.Install;
                }



            }
            else
            {
                //Clears existing installed version
                if (InstalledVersion != null)
                {
                    this.InstalledVersion.IsInstalled = false;

                    //removes version if not in library
                    if (InstalledVersion.Status == VMVersionStatus.Unknown) this.PackageVersions.Remove(InstalledVersion);


                    if (this.PackageVersions.Count == 0) SetMetaData(null);
                }

                this.InstalledVersion = null;
                this.IsInstalled = false;
                this.ShowUninstallButton = false;
                this.InstallButtonType = Installer.InstallButtonType.Install;
                this.DisplayStatus = PackageStatus; 



                if (OfficialLibraryVersion != null)
                {
                    ShowInstallButton = true;
                }
                else
                {
                    ShowInstallButton = false;
                }
            }


            //refreshes the versions view
            this.PackageVersionsView.Refresh();
        }



        /// <summary>
        /// This property stores the version installed
        /// <remarks>
        /// 
        /// 
        /// This is determined by the list of LibraryPackages sent to the constructor. 
        /// If there are published versions it will be the latest published version. 
        /// If there are no published versions it will be the latest non published version.
        /// 
        /// </remarks>
        /// </summary>
        /// 
        private PackageVersionViewModel _installedVersion;
        public PackageVersionViewModel InstalledVersion
        {
            get
            {
                return _installedVersion;
            }
            private set
            {
                if (_installedVersion != value)
                {
                    _installedVersion = value;
                    OnPropertyChanged("InstalledVersion");
                }
            }
        }














        #region UX Properties

        private InstallButtonType _installButtonType;
        public InstallButtonType InstallButtonType
        {
            get
            {
                return _installButtonType;
            }
            set
            {
                if (_installButtonType != value)
                {
                    _installButtonType = value;
                    OnPropertyChanged("InstallButtonType");
                }
            }
        }

        private bool _showInstallButton;
        public bool ShowInstallButton
        {
            get
            {
                return _showInstallButton;
            }
            set
            {
                if (_showInstallButton != value)
                {
                    _showInstallButton = value;
                    OnPropertyChanged("ShowInstallButton");
                }
            }
        }

        private bool _isInstalled;
        public bool IsInstalled
        {
            get
            {
                return _isInstalled;
            }
            private set
            {
                if (_isInstalled != value)
                {
                    _isInstalled = value;
                    OnPropertyChanged("IsInstalled");
                }
            }
        }


        private bool _showUninstallButton;
        public bool ShowUninstallButton
        {
            get
            {
                return _showUninstallButton;
            }
            set
            {
                if (_showUninstallButton != value)
                {
                    _showUninstallButton = value;
                    OnPropertyChanged("ShowUninstallButton");
                }
            }
        }



        #endregion



        private VersionStatus _versionFilter;
        public VersionStatus VersionStatusFilter
        {
            get
            {
                return _versionFilter;
            }
            set
            {
                if (value != _versionFilter)
                {
                    this._versionFilter = value;
                    OnPropertyChanged("VersionStatusFilter");
                    PackageVersionsView.Refresh();
                }
            }
        }


        private bool FilterPackageVersionsList(object o)
        {
            PackageVersionViewModel vm = (PackageVersionViewModel)o;

            if (vm.IsInstalled)
            {
                //Always show installed versions
                return true;
            }
            else
            {
                if (vm.Status != VMVersionStatus.Unknown)
                {
                    if ((int)vm.Status <= (int)this.VersionStatusFilter) return true;
                    else return false;
                }
                else
                {
                    throw new InvalidOperationException("PackageVersionViewModel has Unknown status and is not Installed");
                }
            }
        }






        #region Constructors



        private PackageViewModel(int targetRelease, string packageCode, bool required)
        {
            if (String.IsNullOrEmpty(packageCode)) throw new ArgumentNullException("packageCode");

            this.PackageCode = packageCode;
            this.TargetRelease = targetRelease;
            this.IsRequired = required;



            //Initializes packageVersion list, sets filter/sort code
            PackageVersions = new ObservableCollection<PackageVersionViewModel>();

            PackageVersionsView = CollectionViewSource.GetDefaultView(PackageVersions);
            PackageVersionsView.Filter = new Predicate<object>(this.FilterPackageVersionsList);
            PackageVersionsView.SortDescriptions.Add(new SortDescription("Package.VersionNumber", ListSortDirection.Descending));
            PackageVersionsView.SortDescriptions.Add(new SortDescription("Package.VersionName", ListSortDirection.Descending));

        }



        /// <summary>
        /// Used to create view model, when
        /// </summary>
        /// <param name="packageCode"></param>
        /// <param name="targetReleas"></param>
        /// <param name="installedPackage"></param>
        /// <param name="required"></param>
        public PackageViewModel(string packageCode, int targetRelease, Package installedPackage, bool required)
            : this(targetRelease, packageCode, required)
        {

            if (installedPackage == null) throw new ArgumentNullException("installedPackage");
            if (packageCode != installedPackage.PackageCode) throw new ArgumentException("installedPackage");


            if (targetRelease < installedPackage.MinRevitRelease) throw new ArgumentOutOfRangeException("lib package MinRevitRelease greater then targetRelease", "libPackages");
            if (installedPackage.MaxRevitRelease.HasValue && targetRelease > installedPackage.MaxRevitRelease.Value) throw new ArgumentOutOfRangeException("lib package MaxRevitRelease less then targetRelease", "libPackages");

            //sets status to unknown
            this.PackageStatus = VMVersionStatus.Unknown;


            //Sets the metadata
            SetMetaData(installedPackage);

            //sets the installed version
            SetInstalled(installedPackage);
        }


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="libpackages">The library packages. This p</param>
        /// <param name="installedPackage">The currently installed version of the package, or null if not installed</param>
        public PackageViewModel(string packageCode, int targetRelease, IList<LibraryPackage> libpackages, bool required)
            : this(targetRelease, packageCode, required)
        {
            if (libpackages == null | (libpackages != null && libpackages.Count == 0)) throw new ArgumentNullException("libpackages");


            //checks for duplicate library versions
            if (libpackages.GroupBy(p => p.Package.VersionName).Count() < libpackages.Count) throw new ArgumentException("libpackages has duplicate versions", "libpackages");

            foreach (LibraryPackage libPack in libpackages)
            {
                if (packageCode != libPack.Package.PackageCode) throw new ArgumentException("Missmatched package code", "libpackages");

                //verifies packages supported by target release
                if (targetRelease < libPack.Package.MinRevitRelease) throw new ArgumentOutOfRangeException("lib package MinRevitRelease greater then targetRelease", "libPackages");
                if (libPack.Package.MaxRevitRelease.HasValue && targetRelease > libPack.Package.MaxRevitRelease.Value) throw new ArgumentOutOfRangeException("lib package MaxRevitRelease less then targetRelease", "libPackages");



                VMVersionStatus vmStatus = VMVersionStatus.Unknown;

                switch (libPack.Status)
                {
                    case VersionStatus.Published:
                        vmStatus = VMVersionStatus.Published;
                        break;
                    case VersionStatus.Unpublished:
                        vmStatus = VMVersionStatus.Unpublished;
                        break;
                    case VersionStatus.Suspended:
                        vmStatus = VMVersionStatus.Suspended;
                        break;
                }

                PackageVersionViewModel versionVm = new PackageVersionViewModel(libPack.Package, TargetRelease, vmStatus);
                PackageVersions.Add(versionVm);
            }


            //Sets OfficialLibraryVersion 
            //Determines and sets the official library version
            //This will be the latest published version if any version are published
            // or the latest unpublished version
            // or the latest suspended version if all versions are suspended
            OfficialLibraryVersion = PackageVersions.OrderBy(p => p.Status).ThenByDescending(p => p.Package.VersionNumber).First();



            SetMetaData(OfficialLibraryVersion.Package);






            //sets the package status to be the status of the official library version
            this.PackageStatus = OfficialLibraryVersion.Status;
            this.DisplayStatus = PackageStatus;

            this.InstallButtonType = Installer.InstallButtonType.Install;
            this.ShowInstallButton = true;

          
        }



        #endregion





        public override string ToString()
        {
            return this.PackageCode + " " + this.TargetRelease + this.PackageStatus;
        }

        #region INotifyPropertyChanged Members


        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion





    }
}
