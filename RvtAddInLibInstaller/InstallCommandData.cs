﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RevitAddInLibrary.Installer
{
    public class InstallCommandData
    {
        public int TargetRevitRelease { get; set; }
        public Package Package { get; set; }


        public InstallCommandData(int revitRelease, Package package)
        {
            this.TargetRevitRelease = revitRelease;
            this.Package = package;
            
        }
    }
}


