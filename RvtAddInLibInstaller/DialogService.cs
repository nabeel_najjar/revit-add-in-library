﻿using Ookii.Dialogs.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RevitAddInLibrary.Installer
{
    public class DialogService : IDialogService
    {

        public void ShowDialog(string windowTitle, string mainInstruction, string content, DialogIcon icon)
        {
   




            Ookii.Dialogs.Wpf.TaskDialog diag = new Ookii.Dialogs.Wpf.TaskDialog();

            TaskDialogButton okButton = new TaskDialogButton(ButtonType.Ok);

            diag.Buttons.Add(okButton);



            diag.WindowTitle = windowTitle;


            if (!String.IsNullOrEmpty(mainInstruction))
            {
                diag.MainInstruction = mainInstruction;
            }


            if (!String.IsNullOrEmpty(content))
            {
                diag.Content = content;
            }



            switch (icon)
            {
                case DialogIcon.Information:

                    diag.MainIcon = TaskDialogIcon.Information;
                    break;
                case DialogIcon.Warning:

                    diag.MainIcon = TaskDialogIcon.Warning;
                    break;
                case DialogIcon.Error:
                    diag.MainIcon = TaskDialogIcon.Error;
                    break;
                default:
                    break;
            }

         
            var res = diag.ShowDialog();

     

        }

        public void ShowDialog(string windowTitle, string mainInstruction, Exception error, DialogIcon icon)
        {



            Ookii.Dialogs.Wpf.TaskDialog diag = new Ookii.Dialogs.Wpf.TaskDialog();

            TaskDialogButton okButton = new TaskDialogButton(ButtonType.Ok);

            diag.Buttons.Add(okButton);



            diag.WindowTitle = windowTitle;


            if (!String.IsNullOrEmpty(mainInstruction))
            {
                diag.MainInstruction = mainInstruction;
            }


            if (error != null)
            {
                diag.Content = error.ToString();
            }



            switch (icon)
            {
                case DialogIcon.Information:

                    diag.MainIcon = TaskDialogIcon.Information;
                    break;
                case DialogIcon.Warning:

                    diag.MainIcon = TaskDialogIcon.Warning;
                    break;
                case DialogIcon.Error:
                    diag.MainIcon = TaskDialogIcon.Error;
                    break;
                default:
                    break;
            }


            var res = diag.ShowDialog();


        }

        public T ShowDialog<T>(T viewModel)
        {
            throw new NotImplementedException();
        }
    }
}
