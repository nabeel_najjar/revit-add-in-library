﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RevitAddInLibrary.Installer
{
    public enum VMVersionStatus
    {
        Published  = 0,
        Unpublished = 1,
        Suspended = 2,
        Unknown = 3
    }
}
