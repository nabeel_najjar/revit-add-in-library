﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RevitAddInLibrary.Installer
{
    public class MockPackageViewModel
    {

        public bool IsInstalled { get; set; }

        public string PackageName { get; set; }
        public int TargetRelease { get; set; }
    }
}
