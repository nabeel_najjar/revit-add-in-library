﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RevitAddInLibrary.Installer
{
    /// <summary>
    /// Interaction logic for InstallFileWindow.xaml
    /// </summary>
    public partial class InstallFileWindow : Window, INotifyPropertyChanged
    {



        public InstallFileWindow(IEnumerable<int> installedReleases)
        {
            InitializeComponent();


            InstalledReleases = new List<InstallFileTargetRelease>();

            foreach (var r in installedReleases.Distinct().OrderBy(r => r))
            {

                InstalledReleases.Add(new InstallFileTargetRelease(r));

            }



            this.InstallCommand = new RelayCommand(new Action<object>(this.Install), new Predicate<object>(this.CanInstall));


            DataContext = this;


            UpdateForSelectedFile();
        }


        private void Install(object o)
        {
            this.SelectedRelease = InstalledReleases.Where(r => r.IsSelected).First().Release;

            this.DialogResult = true;
            this.Close();
        }

        private bool CanInstall(object o)
        {
            if (System.IO.File.Exists(AddInFilePath))
            {

                foreach (var r in InstalledReleases)
                {

                    if (r.IsSelected) return true;
                }

                return false;

            }
            else
            {
                return false;
            }
        }

        public ICommand InstallCommand { get; private set; }
        



        public int SelectedRelease { get; private set; }


        public void UpdateForSelectedFile()
        {

            if (String.IsNullOrEmpty(AddInFilePath))
            {

                this.PackageCode = string.Empty;
                this.PackageName = "No add-in package file selected";
                this.VersionName = string.Empty;
                this.SupportedReleases = string.Empty;
            }
            else
            {

                if (System.IO.File.Exists(AddInFilePath))
                {
                    try
                    {
                        Package package = Package.ReadPackage(AddInFilePath);



                        this.PackageCode = package.PackageCode;
                        this.PackageName = package.PackageName;
                        this.VersionName = package.VersionName;
                        this.SupportedReleases = package.SupportedRevitReleases;


                        if (package.MaxRevitRelease.HasValue)
                        {


                            foreach (var r in InstalledReleases)
                            {
                                if (r.Release >= package.MinRevitRelease & r.Release <= package.MaxRevitRelease.Value)
                                {
                                    r.IsEnabled = true;
                                    r.IsSelected = false;
                                }
                                else
                                {
                                    r.IsEnabled = false;
                                    r.IsSelected = false;
                                }
                            }





                        }
                        else
                        {
                            foreach (var r in InstalledReleases)
                            {
                                if (r.Release >= package.MinRevitRelease)
                                {
                                    r.IsEnabled = true;
                                    r.IsSelected = false;
                                }
                                else
                                {
                                    r.IsEnabled = false;
                                    r.IsSelected = false;
                                }
                            }
                        }


                        var top = InstalledReleases.Where(r => r.IsEnabled).FirstOrDefault();
                        if (top != null) top.IsSelected = true;

                    }
                    catch
                    {
                        foreach (var r in InstalledReleases)
                        {
                            r.IsEnabled = false;
                            r.IsSelected = false;
                        }

                        this.PackageCode = null;
                        this.PackageName = "Invalid Package File!";
                        this.VersionName = null;
                        this.SupportedReleases = null;


                        return;
                    }

                }
                else
                {
                    foreach (var r in InstalledReleases)
                    {
                        r.IsEnabled = false;
                        r.IsSelected = false;
                    }

                    this.PackageCode = null;
                    this.PackageName = "File not found!";
                    this.VersionName = null;
                    this.SupportedReleases = null;




                    return;
                }


            }




        }


        private string _addinPath;
        public string AddInFilePath
        {
            get
            {
                return _addinPath;
            }
            set
            {
                if (value != _addinPath)
                {
                    _addinPath = value;
                    OnPropertyChanged("AddInFilePath");

                    UpdateForSelectedFile();
                }
            }
        }



        private string _packageName;
        public string PackageName
        {
            get
            {
                return _packageName;
            }
            set
            {
                if (value != _packageName)
                {
                    _packageName = value;
                    OnPropertyChanged("PackageName");


                }
            }
        }


        private string _packageCode;
        public string PackageCode
        {
            get
            {
                return _packageCode;
            }
            set
            {
                if (value != _packageCode)
                {
                    _packageCode = value;
                    OnPropertyChanged("PackageCode");


                }
            }
        }


        private string _versionName;
        public string VersionName
        {
            get
            {
                return _versionName;
            }
            set
            {
                if (value != _versionName)
                {
                    _versionName = value;
                    OnPropertyChanged("VersionName");
                }
            }
        }



        private string _supRel;
        public string SupportedReleases
        {
            get
            {
                return _supRel;
            }
            set
            {
                if (value != _supRel)
                {
                    _supRel = value;
                    OnPropertyChanged("SupportedReleases");
                }
            }
        }






        public List<InstallFileTargetRelease> InstalledReleases { get; set; }





        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void browseButton_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog oDiag = new Microsoft.Win32.OpenFileDialog();

            oDiag.Title = "Revit Add-In Package File";

            oDiag.Filter = "Revit Add-In Package (*.rpk)|*.rpk|Zip File (*.zip)|*.zip";
            var res = oDiag.ShowDialog();

            if (res.HasValue && res.Value)
            {
                this.AddInFilePath = oDiag.FileName;
            }

        }





        #region INotifyPropertyChanged Members


        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion




    }


    public class InstallFileTargetRelease : INotifyPropertyChanged
    {

        public InstallFileTargetRelease(int release)
        {
            this.Release = release;
            this.IsSelected = false;
            this.IsEnabled = false;
        }

        public int Release { get; private set; }

        private bool _selected;
        public bool IsSelected
        {
            get
            {
                return _selected;
            }
            set
            {
                if (value != _selected)
                {
                    _selected = value;
                    OnPropertyChanged("IsSelected");

                }
            }
        }


        private bool _enabled;
        public bool IsEnabled
        {
            get
            {
                return _enabled;
            }
            set
            {
                if (value != _enabled)
                {
                    _enabled = value;
                    OnPropertyChanged("IsEnabled");
                }
            }
        }




        #region INotifyPropertyChanged Members


        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion

    }
}

