﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevitAddInLibrary;
using System.Windows.Input;
using System.ComponentModel;

namespace RevitAddInLibrary.Installer
{



    public class PackageVersionViewModel : INotifyPropertyChanged
    {
        public VMVersionStatus Status { get; private set; }
        public Package Package { get; private set; }
        public InstallCommandData InstallParameter { get; private set; }




        private bool _isInstalled;
        public bool IsInstalled
        {
            get
            {
                return _isInstalled;
            }
            set
            {
                if (_isInstalled != value)
                {
                    _isInstalled = value;

                    OnPropertyChanged("IsInstalled");
                }
            }
        }



        public PackageVersionViewModel(Package package, int targetRelease, VMVersionStatus status)
        {

            if (package == null) throw new ArgumentNullException("package");

            this.Package = package;
            this.Status = status;

            if (package.MaxRevitRelease.HasValue)
            {
                if (targetRelease < package.MinRevitRelease | targetRelease > package.MaxRevitRelease)
                {
                    throw new ArgumentOutOfRangeException("targetRelease");
                }
            }
            else
            {
                if (targetRelease < package.MinRevitRelease)
                {
                    throw new ArgumentOutOfRangeException("targetRelease");
                }
            }

            this.InstallParameter = new InstallCommandData(targetRelease, package);
        }




        #region INotifyPropertyChanged Members


        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion



    }
}


