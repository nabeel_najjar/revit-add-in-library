﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommandLine;
using CommandLine.Text;

namespace RevitAddInLibrary.ConsoleUtility
{


    public class ListOptions
    {
        [Option('p', "PackageCode", HelpText = "The package code of the installed package(s) to list.")]
        public string PackageCode { get; set; }

        [Option('r', "release", HelpText = "The release of Revit to list installed add-ins from.")]
        public int? Release { get; set; }
    }

    public class InstallOptions
    {
        [Option('p', "PackageCode", Required = true, HelpText = "The package code.")]
        public string PackageCode { get; set; }

        [Option('r', "Release", Required = true, HelpText = "Release of Revit (2013, 2014, 2015 etc..)")]
        public int Release { get; set; }

        [Option('v', "Version", Required = false, HelpText = "Specific version to install from library.")]
        public string Version { get; set; }
    }


    public class DownloadPackagesOptions
    {
        [Option('f', "PackageFile", Required = false, HelpText = "Destination package file to save. If omitted the packages will be saved as \"packagecode_versionnumber.rpk\"")]
        public string PackageFile { get; set; }

        [Option('p', "PackageCode", Required = true, HelpText = "The package code of the package.")]
        public string PackageCode { get; set; }

        [Option('v', "Version", Required = true, HelpText = "The version of the package.")]
        public string Version { get; set; }
    }

    public class ReinstallOptions
    {
        [Option('p', "PackageCode", Required = true, HelpText = "The package code.")]
        public string PackageCode { get; set; }

        [Option('r', "Release", Required = true, HelpText = "Release of Revit (2013, 2014, 2015 etc..)")]
        public int Release { get; set; }
    }

    public class UpdateOptions
    {
        [Option('p', "PackageCode", Required = true, HelpText = "The package to update.")]
        public string PackageCode { get; set; }

        [Option('r', "Release", Required = true, HelpText = "The target release of Revit to update the package.")]
        public int Release { get; set; }
    }

    public class UpdateAllOptions
    {
        [Option('i', "installreq", DefaultValue = false, Required = false, HelpText = "If true update process will install missing required add-ins, otherwise if false will only update currently installed add-ins.")]
        public bool InstallRequired { get; set; }

        [Option('r', "release", Required = false, HelpText = "The release of Revit to update add-ins for. If omitted all installed releases are updated.")]
        public int? Release { get; set; }
    }

    public class UninstallOptions
    {
        [Option('r', "Release", Required = true, HelpText = "The release of Revit to uninstall from.")]
        public int Release { get; set; }

        [Option('p', "PackageCode", Required = true, HelpText = "The package to uninstall.")]
        public string PackageCode { get; set; }
    }

    public class UninstallAllOptions
    {
        [Option('r', "Release", Required = false, HelpText = "The release of Revit to uninstall all add-ins from. If omitted all add-ins from all releases will be uninstalled.")]
        public int? Release { get; set; }
    }



    public class InstallFileOptions
    {
        [Option('f', "PackageFile", Required = true, HelpText = "Package file to install.")]
        public string PackageFile { get; set; }

        [Option('r', "Release", Required = true, HelpText = "The release of Revit to install the add-in to.")]
        public int Release { get; set; }
    }



    public class ListLibraryOptions
    {
        [Option('p', "PackageCode", HelpText = "The package code of the library package(s) to list.")]
        public string PackageCode { get; set; }

        [Option('r', "release", HelpText = "The release of Revit to list library packages from.")]
        public int? Release { get; set; }
    }



    public class NewLibraryOptions
    {
        [Option('l', "library", Required = true, HelpText = "Directory to create a new library in. If directory does not exist it will be created.")]
        public string Library { get; set; }
    }



    public class AddPackageOptions
    {
        [Option('f', "PackageFile", Required = true, HelpText = "Package file to add to the library.")]
        public string PackageFile { get; set; }
    }


    public class DeletePackageOptions
    {
        [Option('p', "PackageCode", Required = true, HelpText = "The package code of the package.")]
        public string PackageCode { get; set; }

        [Option('v', "Version", Required = true, HelpText = "The version of the package.")]
        public string Version { get; set; }
    }



    public class SetStatusOptions
    {
        [Option('p', "PackageCode", Required = true, HelpText = "The package code of the package.")]
        public string PackageCode { get; set; }

        [Option('v', "Version", Required = true, HelpText = "The version of the package.")]
        public string Version { get; set; }

        [Option('s', "Status", Required = true, HelpText = "The new package status: Published, Unpublished, or Suspended.")]
        public RevitAddInLibrary.VersionStatus Status { get; set; }

    }



    public class BuildPackageOptions
    {
        [Option('f', "PackageFile", Required = true, HelpText = "Destination file.")]
        public string PackageFile { get; set; }

        [Option('s', "Source", Required = true, HelpText = "Package files source directory. This directory must contain a \"Package.xml\" file, and a corresponding Revit add-in manifest file named according to the package code.")]
        public string Source { get; set; }
    }

    public class Options
    {
        [VerbOption("list", HelpText = "Lists the installed packages.")]
        public ListOptions ListPackages { get; set; }

        [VerbOption("install", HelpText = "Installs a package from the library.")]
        public InstallOptions InstallPackage { get; set; }


        [VerbOption("reinstall", HelpText = "Reinstalls a currently installed package from the library.")]
        public ReinstallOptions ReinstallPackage { get; set; }

        [VerbOption("install-file", HelpText = "Installs a package file.")]
        public InstallFileOptions InstallPackageFile { get; set; }

        [VerbOption("update", HelpText = "Updates a package to the latest version in the library.")]
        public UpdateOptions UpdatePackages { get; set; }

        [VerbOption("update-all", HelpText = "Updates installed packages, and installs missing required packages.")]
        public UpdateAllOptions UpdateAllPackages { get; set; }

        [VerbOption("uninstall", HelpText = "Uninstalls an installed package.")]
        public UninstallOptions UninstallPackage { get; set; }

        [VerbOption("uninstall-all", HelpText = "Uninstalls all installed packages.")]
        public UninstallAllOptions UninstallAllPackages { get; set; }



        //THE FOLLOWING ARE Library management commands


        [VerbOption("new-library", HelpText = "Creates a new package library.")]
        public NewLibraryOptions NewLibrary { get; set; }

        [VerbOption("list-library", HelpText = "Lists the contents of a package library.")]
        public ListLibraryOptions ListLibrary { get; set; }

        [VerbOption("build-package", HelpText = "Builds a new package file from source files.")]
        public BuildPackageOptions BuildPackage { get; set; }

        [VerbOption("add-package", HelpText = "Adds a package file to the library.")]
        public AddPackageOptions AddLibraryPackage { get; set; }

        [VerbOption("delete-package", HelpText = "Deletes a package from the library.")]
        public DeletePackageOptions DeleteLibraryPackage { get; set; }

        [VerbOption("download-package", HelpText = "Downloads a package from the library.")]
        public DownloadPackagesOptions DownloadLibraryPackage { get; set; }

        [VerbOption("set-status", HelpText = "Sets the status of a package in the library.")]
        public SetStatusOptions SetLibraryPackageStatus { get; set; }






        [HelpOption]
        public string GetUsage()
        {
            StringBuilder sb = new StringBuilder();

            var v = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            var version = v.Major + "." + v.Minor + "." + v.Build;

            sb.AppendLine();
            sb.AppendLine("Revit Add-In Library Utility " + version);
            sb.AppendLine();


            sb.AppendLine("Usage: RvtAddInLibUtil [Command] [Options]");
            sb.AppendLine();
            sb.AppendLine("Commands:");


            HelpText help = new HelpText();
            help.AdditionalNewLineAfterOption = true;
            help.AddOptions(this);
            sb.AppendLine(help.ToString());


            string hr = "--------------------------------------------------------------------------------";

            sb.AppendLine(hr);
            sb.AppendLine(GetVerbUsage("list"));
            sb.AppendLine(hr);
            sb.AppendLine(GetVerbUsage("install"));
            sb.AppendLine(hr);
            sb.AppendLine(GetVerbUsage("install-file"));
            sb.AppendLine(hr);
            sb.AppendLine(GetVerbUsage("reinstall"));
            sb.AppendLine(hr);
            sb.AppendLine(GetVerbUsage("update"));
            sb.AppendLine(hr);
            sb.AppendLine(GetVerbUsage("update-all"));
            sb.AppendLine(hr);
            sb.AppendLine(GetVerbUsage("uninstall"));
            sb.AppendLine(hr);
            sb.AppendLine(GetVerbUsage("uninstall-all"));
            sb.AppendLine(hr);


            sb.AppendLine(GetVerbUsage("new-library"));
            sb.AppendLine(hr);
            sb.AppendLine(GetVerbUsage("list-library"));


            //Uncomment this when build package is implemented
            //sb.AppendLine(hr);
            //sb.AppendLine(GetVerbUsage("build-package"));


            sb.AppendLine(hr);
            sb.AppendLine(GetVerbUsage("add-package"));
            sb.AppendLine(hr);
            sb.AppendLine(GetVerbUsage("delete-package"));
            sb.AppendLine(hr);
            sb.AppendLine(GetVerbUsage("download-package"));
            sb.AppendLine(hr);
            sb.AppendLine(GetVerbUsage("set-status"));

            var helpString = sb.ToString();


            return helpString;
        }

        [HelpVerbOption]
        public string GetVerbUsage(string verb)
        {

            var help = new HelpText();


            switch (verb)
            {

                case "list":

                    help.AddOptions(new ListOptions());
                    help.AddPreOptionsLine("Command: list");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Lists the installed packages.");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Usage: RvtAddInLibUtil list [-r 2013] [-p packagecode]");
                    return help.ToString();

                case "install":

                    help.AddOptions(new InstallOptions());
                    help.AddPreOptionsLine("Command: install");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Installs a package from the library.");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Usage: RvtAddInLibUtil install -p packagecode -r 2013");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("- Installs the latest version in the library.");
                    help.AddPreOptionsLine("- Does nothing if any version of package is already installed");

                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Usage: RvtAddInLibUtil install -p packagecode -r 2013 -v 1.0");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("- Installs the specified version from the library.");
                    help.AddPreOptionsLine("- Upgrades/downgrades if another version is already installed");
                    help.AddPreOptionsLine("- Does nothing if specified version is already installed.");



                    return help.ToString();


                case "reinstall":

                    help.AddOptions(new ReinstallOptions());
                    help.AddPreOptionsLine("Command: reinstall");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Reinstalls a currently installed package from the library.");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Usage: RvtAddInLibUtil reinstall -p packagecode -r 2013");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("- Reinstalls the currently installed version from library");
                    help.AddPreOptionsLine("- Does nothing if package is not currently installed");
                    help.AddPreOptionsLine("- Fails if installed version can not be found in the library");
                    return help.ToString();


                case "install-file":

                    help.AddOptions(new InstallFileOptions());
                    help.AddPreOptionsLine("Command: install-file");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Installs a package file.");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Usage: RvtAddInLibUtil install-file -f \"C:\\Path\\To\\File.rpk\"  -r 2013");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("- Installs the specified package file");
                    help.AddPreOptionsLine("- Any existing installed versions of the package for the target release are replaced");
                    return help.ToString();


                case "update":

                    help.AddOptions(new UpdateOptions());
                    help.AddPreOptionsLine("Command: update");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Updates a package to the latest version in the library.");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Usage: RvtAddInLibUtil update -p packagecode -r 2013");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("- Does nothing if no version of the package is installed");
                    help.AddPreOptionsLine("- Upgrades to latest published version in library if it is newer than installed");
                    help.AddPreOptionsLine("- Does nothing if installed version is newer then published and not suspended");
                    help.AddPreOptionsLine("- Upgrades/downgrades to latest published version if installed version is suspended");
                    help.AddPreOptionsLine("- Uninstalls if installed version is suspended and no versions are published");
                    return help.ToString();


                case "update-all":

                    help.AddOptions(new UpdateAllOptions());
                    help.AddPreOptionsLine("Command: update-all");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Updates installed packages, and installs missing required packages.");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Usage: RvtAddInLibUtil update-all [-i] [-r 2013]");
                    return help.ToString();

                case "uninstall":

                    help.AddOptions(new UninstallOptions());
                    help.AddPreOptionsLine("Command: uninstall");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Uninstalls an installed package.");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Usage: RvtAddInLibUtil uninstall -r 2013 -p packagecode");
                    return help.ToString();

                case "uninstall-all":

                    help.AddOptions(new UninstallAllOptions());
                    help.AddPreOptionsLine("Command: uninstall-all");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Uninstalls all installed packages.");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Usage: RvtAddInLibUtil uninstall-all [-r 2013]");
                    return help.ToString();


                case "new-library":


                    help.AddOptions(new NewLibraryOptions());
                    help.AddPreOptionsLine("Command: new-library");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Creates a new package library.");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Usage: RvtAddInLibUtil new-library -l \"X:\\New Library Directory\\\"");

                    return help.ToString();

                case "list-library":


                    help.AddOptions(new ListLibraryOptions());
                    help.AddPreOptionsLine("Command: list-library");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Lists the contents of a package library.");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Usage: RvtAddInLibUtil list-library [-p packagecode] [-r 2013]");


                    return help.ToString();

                case "add-package":

                    help.AddOptions(new AddPackageOptions());
                    help.AddPreOptionsLine("Command: add-package");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Adds a package file to the library.");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Usage: RvtAddInLibUtil add-package -f \"C:\\Path\\To\\File.rpk\"");

                    return help.ToString();

                case "delete-package":

                    help.AddOptions(new DeletePackageOptions());
                    help.AddPreOptionsLine("Command: delete-package");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Deletes a package from the library.");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Usage: RvtAddInLibUtil delete-package -p packagecode -v 1.0");
                    return help.ToString();


                case "set-status":

                    help.AddOptions(new SetStatusOptions());
                    help.AddPreOptionsLine("Command: set-status");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Sets the status of a package in the library.");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Usage: RvtAddInLibUtil set-status -p packagecode -v 1.0 -s Published|Unpublished|Suspended");
                    return help.ToString();

                case "build-package":


                    help.AddOptions(new BuildPackageOptions());
                    help.AddPreOptionsLine("Command: build-package");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Builds a new package file from source files.");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Usage: RvtAddInLibUtil build-package -f \"C:\\package file.rpk -s \"C:\\Source Folder\\\"");


                    return help.ToString();


                case "download-package":

                    
                    help.AddOptions(new DownloadPackagesOptions());
                    help.AddPreOptionsLine("Command: download-package");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Downloads a package from the library.");
                    help.AddPreOptionsLine(" ");
                    help.AddPreOptionsLine("Usage: RvtAddInLibUtil download-package [-f \"C:\\package file.rpk] -p packagecode -v 1.0");


                    return help.ToString();




                default:
                    return GetUsage();
            }




        }
    }
}
