﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using RevitAddInLibrary;
using CommandLine;
using CommandLine.Text;
using NLog;
using System.Configuration;

namespace RevitAddInLibrary.ConsoleUtility
{

    public class ConsoleUtility
    {
        private Logger logger = LogManager.GetCurrentClassLogger();

        public string Library { get; set; }

        public void Run(string[] args)
        {
            try
            {

                var options = new Options();

                logger.Info("RvtAddInLibUtil.exe started...");
                logger.Info("args = " + string.Join(" ", args));


                Parser p = new Parser(new Action<ParserSettings>(SetupParser));

                bool r = p.ParseArgumentsStrict(args, options, new Action<string, object>(FindVerb));

            }
            catch (Exception exp)
            {
                logger.Error(exp);
                Console.WriteLine(exp.ToString());
            }
        }


        private void SetupParser(ParserSettings set)
        {
            set.HelpWriter = Console.Out;
            set.IgnoreUnknownArguments = false;
            set.MutuallyExclusive = true;
        }


        private void FindVerb(string verb, object options)
        {

            if (options == null) return;

            switch (verb)
            {

                case "list":

                    ListOptions listOpt = (ListOptions)options;
                    List(listOpt.Release, listOpt.PackageCode);
                    break;

                case "install":

                    InstallOptions insOpt = (InstallOptions)options;
                    Install(insOpt.Release, insOpt.PackageCode, insOpt.Version);

                    break;

                case "install-file":

                    InstallFileOptions insFileOPt = (InstallFileOptions)options;
                    InstallFile(insFileOPt.Release, insFileOPt.PackageFile);
                    break;

                case "reinstall":

                    ReinstallOptions reinsOpt = (ReinstallOptions)options;
                    Reinstall(reinsOpt.Release, reinsOpt.PackageCode);
                    break;

                case "update":

                    UpdateOptions updateOpt = (UpdateOptions)options;
                    Update(updateOpt.Release, updateOpt.PackageCode);
                    break;


                case "update-all":

                    UpdateAllOptions updateAllOpt = (UpdateAllOptions)options;
                    UpdateAll(updateAllOpt.Release, updateAllOpt.InstallRequired);
                    break;

                case "uninstall":

                    UninstallOptions uninstallOpt = (UninstallOptions)options;
                    UnInstall(uninstallOpt.Release, uninstallOpt.PackageCode);
                    break;

                case "uninstall-all":

                    UninstallAllOptions uninstallAllOpt = (UninstallAllOptions)options;
                    UnInstallAll(uninstallAllOpt.Release);
                    break;


                case "new-library":

                    NewLibraryOptions newLibOpt = (NewLibraryOptions)options;
                    NewLibraryOptions(newLibOpt.Library);
                    break;

                case "list-library":

                    ListLibraryOptions listLibOpt = (ListLibraryOptions)options;
                    ListLibrary(listLibOpt.PackageCode, listLibOpt.Release);
                    break;

                case "add-package":


                    AddPackageOptions addPackageOpt = (AddPackageOptions)options;
                    AddPackage(addPackageOpt.PackageFile);
                    break;


                case "delete-package":

                    DeletePackageOptions delPackageOpt = (DeletePackageOptions)options;
                    DeletePackage(delPackageOpt.PackageCode, delPackageOpt.Version);
                    break;

                case "set-status":

                    SetStatusOptions setStatusOpt = (SetStatusOptions)options;
                    SetStatus(setStatusOpt.PackageCode, setStatusOpt.Version, setStatusOpt.Status);
                    break;

                case "build-package":


                    BuildPackageOptions buildPkgOpt = (BuildPackageOptions)options;
                    BuildPackage(buildPkgOpt.Source, buildPkgOpt.PackageFile);
                    break;


                case "download-package":


                    DownloadPackagesOptions downPackOpt = (DownloadPackagesOptions)options;
                    DownloadPackage(downPackOpt.PackageCode, downPackOpt.Version, downPackOpt.PackageFile);
                    break;




                default:
                    return;
            }

        }

        private void DownloadPackage(string packageCode, string versionNumber, string file)
        {

          

            FolderPackageLibrary lib = new FolderPackageLibrary(Library, true);

            var package = lib.GetPackage(packageCode, versionNumber);

            if (package == null)
            {
                Console.WriteLine("Library does not contain a package " + packageCode + " (" + versionNumber + ")");
                return;
            }

            string destFile = string.Empty;

            if (String.IsNullOrEmpty(file))
            {
                destFile = packageCode + "_" + versionNumber + ".rpk";


            }
            else
            {
                destFile = file;

            }


            using (var fs = new FileStream(destFile, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.None))
            {
                var data = lib.GetPackageData(packageCode, versionNumber);

                data.CopyTo(fs);
            }

        }

        private void BuildPackage(string source, string destFile)
        {
            using (var fs = new FileStream(destFile, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.None))
            {
                Package.BuildPackage(fs, source);
            }
        }



        private void SetStatus(string package, string version, VersionStatus status)
        {
            


            FolderPackageLibrary lib = new FolderPackageLibrary(Library, false);


            var libPack = lib.GetPackage(package, version);


            if (libPack == null)
            {
                Console.WriteLine("Could not find " + package + " " + version + " in the library");
            }

            if (libPack.Status == status)
            {
                Console.WriteLine(libPack.Package.PackageCode + " " + libPack.Package.VersionName + " is already " + status.ToString());
            }
            else
            {
                libPack.Status = status;

                lib.UpdatePackage(libPack);

                Console.WriteLine(libPack.Package.PackageCode + " " + libPack.Package.VersionName + " set to " + status.ToString());

            }


        }



        private void AddPackage(string packageFile)
        {
  


            FolderPackageLibrary lib = new FolderPackageLibrary(Library,false);

            if (File.Exists(packageFile))
            {

                using (FileStream fs = new FileStream(packageFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                {

                    using (MemoryStream ms = new MemoryStream())
                    {

                        fs.CopyTo(ms);

                        ms.Position = 0;


                        var package = Package.ReadPackage(ms);

                        ms.Position = 0;

                        lib.AddPackage(ms);

                        Console.WriteLine(package.ToString() + " added to the library");

                    }
                }
            }
            else
            {
                Console.WriteLine("Can not find package file " + packageFile);
            }




        }

        private void DeletePackage(string packageCode, string versionNumber)
        {
            


            FolderPackageLibrary lib = new FolderPackageLibrary(Library, false);


            var libPack = lib.GetPackage(packageCode, versionNumber);

            if (libPack == null)
            {
                Console.WriteLine("Could not find " + packageCode + " " + versionNumber + " in the library");
            }
            else
            {
                lib.DeletePackage(packageCode, versionNumber);
                Console.WriteLine("Deleted " + packageCode + " " + versionNumber + " from the library");

            }


        }

        private  void ListLibrary(string packageCode, int? release)
        {
            


            FolderPackageLibrary lib = new FolderPackageLibrary(Library, true);

            List<LibraryPackage> libPackages = lib.GetPackages(release, packageCode).ToList();

            Console.WriteLine(libPackages.Count.ToString() + " packages found");

            foreach (var pkg in libPackages)
            {

                StringBuilder sb = new StringBuilder();

                //hatch22 (mertens3d.hatch22) 1.0.0 [1] [Revit 2013] Published

                sb.Append(pkg.Package.PackageName);
                sb.Append(" (");


                sb.Append(pkg.Package.PackageCode);
                sb.AppendLine(")");



                sb.Append(pkg.Package.VersionName);
                sb.Append(" [");

                sb.Append(pkg.Package.VersionName.ToString());


                sb.Append("] [Revit ");
                sb.Append(pkg.Package.SupportedRevitReleases);
                sb.AppendLine("]");



                sb.AppendLine(pkg.Status.ToString());

                sb.AppendLine();

                Console.WriteLine(sb.ToString());

            }

        }

        private void NewLibraryOptions(string dir)
        {
            FolderPackageLibrary.CreateNew(dir);
            Console.WriteLine("New package library created in " + dir);
        }

        private void Reinstall(int release, string packageCode)
        {
            logger.Info("Reinstall(release = {0}, packageCode = {1})", release, packageCode);


            var installer = GetInstaller();
            var lib = GetLibrary();


            logger.Info("Checking for installed version of package...");

            var previous = installer.GetInstalledPackage(packageCode, release);

            if (previous != null)
            {
                logger.Info(previous.Package.ToString() + " is installed");

                var libpackage = lib.GetPackage(packageCode, previous.Package.VersionName);

                if (libpackage == null)
                {
                    Console.WriteLine(previous.ToString() + " was not found in the library to reinstall from");
                    return;
                }


                logger.Info("Package " + previous.Package.ToString() + " is installed, uninstalling...");
                var uninstallResult = installer.Uninstall(release, packageCode);


                var version = previous.Package.VersionName;

                logger.Info("Reinstalling...");
                var installResult = installer.Install(lib, release, packageCode, version);

                Console.WriteLine("Reinstalled " + installResult.Current.ToString());
            }
            else
            {
                logger.Info("Package is not installed");
                Console.WriteLine("No version of " + packageCode + " is installed for Revit " + release.ToString());
            }


        }

        private void Update(int release, string packageCode)
        {
            logger.Info("Update(release = {0}, packageCode = {1})", release, packageCode);


            var installer = GetInstaller();
            var lib = GetLibrary();



            var res = installer.Update(lib, release, packageCode);

            Console.WriteLine(GetUpdateResultDescription(res));

        }


        private void List(int? release, string packageCode)
        {
            logger.Info("List(release = {0}, packageCode = {1})", release, packageCode);


            PackageInstaller installer = GetInstaller();

            List<InstalledPackage> installed = null;

            if (String.IsNullOrEmpty(packageCode) & release.HasValue == false)
            {
                //package code null & release null
                //list all
                logger.Info("Listing all installed packages...");
                installed = installer.GetInstalledPackages()
                    .OrderBy(p => p.Package.PackageCode).ThenBy(p => p.Package.VersionNumber)
                    .ToList();
            }
            else if (String.IsNullOrEmpty(packageCode) & release.HasValue == true)
            {
                //package code null & release set
                logger.Info("Listing installed packages for Revit" + release.ToString() + "...");
                installed = installer.GetInstalledPackages()
                    .Where(p => p.Package.MinRevitRelease <= release.Value & (p.Package.MaxRevitRelease.HasValue && p.Package.MaxRevitRelease >= release.Value))
                    .OrderBy(p => p.Package.PackageCode).ThenBy(p => p.Package.VersionNumber)
                    .ToList();
            }
            else if (!String.IsNullOrEmpty(packageCode) & release.HasValue == false)
            {
                //package code not set & release null
                logger.Info("Listing installed packages for package " + packageCode.ToString() + "...");
                installed = installer.GetInstalledPackages()
                    .Where(p => p.Package.PackageCode == packageCode)
                    .OrderBy(p => p.Package.PackageCode).ThenBy(p => p.Package.VersionNumber)
                    .ToList();
            }
            else if (!String.IsNullOrEmpty(packageCode) & release.HasValue == true)
            {
                //package code set & release set
                logger.Info("Listing installed packages for package " + packageCode.ToString() + " and Revit " + release.ToString() + "...");
                installed = installer.GetInstalledPackages()
                    .Where(p => p.Package.PackageCode == packageCode & p.Package.MinRevitRelease <= release.Value & (p.Package.MaxRevitRelease.HasValue && p.Package.MaxRevitRelease >= release.Value))
                    .OrderBy(p => p.Package.PackageCode).ThenBy(p => p.Package.VersionNumber)
                    .ToList();
            }

            int count = installed.Count;

            Console.WriteLine();

            if (count == 0) Console.WriteLine("No installed Revit add-in packages found");
            else if (count == 1) Console.WriteLine("1 installed Revit add-in package found");
            else if (count > 1) Console.WriteLine(count + " installed Revit add-in packages found");

            foreach (var pk in installed)
            {
                Console.WriteLine();
                Console.WriteLine(pk.Package.PackageName + " (" + pk.Package.PackageCode + ")");
                Console.WriteLine("Version Name: " + pk.Package.VersionName);
                Console.WriteLine("Version Number: " + pk.Package.VersionNumber.ToString());
                Console.WriteLine("Min Revit Release: " + pk.Package.MinRevitRelease);
                Console.WriteLine("Max Revit Release: " + pk.Package.MaxRevitRelease ?? String.Empty);
                Console.WriteLine("Author: " + pk.Package.Author);
                Console.WriteLine("Email: " + pk.Package.Email);
                Console.WriteLine("Link: " + pk.Package.Link.ToString());
                Console.WriteLine("Installed On: " + pk.InstalledOnUTC.ToLocalTime().ToString());

            }
        }

        private void Install(int release, string packageCode, string version)
        {
            logger.Info("Install(release = {0}, packageCode = {1}), version = {2})", release, packageCode, version);


            var lib = GetLibrary();
            var installer = GetInstaller();

            InstallResult res = null;

            var previouslyInstalled = installer.GetInstalledPackage(packageCode, release);

            if (previouslyInstalled != null)
            {

                if (previouslyInstalled.Package.VersionName != version)
                {
                    res = installer.Install(lib, release, packageCode, version);
                    Console.WriteLine(GetInstallerResultDescription(res));
                }
                else
                {
                    Console.WriteLine(previouslyInstalled.ToString() + " already installed");
                }
            }
            else
            {
                res = installer.Install(lib, release, packageCode, version);
                Console.WriteLine(GetInstallerResultDescription(res));
            }

        }


        private void InstallFile(int release, string packageFile)
        {
            logger.Info("InstallFile(packageFile = {0})", packageFile);


            PackageInstaller installer = GetInstaller();

            var r = installer.Install(release, packageFile);

            Console.WriteLine(GetInstallerResultDescription(r));


        }

        private void UpdateAll(int? release, bool installReq)
        {

            logger.Info("UpdateAll(release = {0}, installReq = {1})", release, installReq);


            var ins = GetInstaller();
            var lib = GetLibrary();
            var reqPkgs = GetRequiredPackages();

            IList<UpdateResult> results = null;

            if (installReq)
            {
                results = ins.UpdateAll(lib, release, reqPkgs);
            }
            else
            {
                results = ins.UpdateAll(lib, release, null);
            }

            foreach (var r in results)
            {
                Console.WriteLine(GetUpdateResultDescription(r));
            }

        }

        private void UnInstall(int release, string packagecode)
        {

            logger.Info("UnInstall(release = {0}, packageCode = {1})", release, packagecode);



            PackageInstaller installer = GetInstaller();

            var res = installer.Uninstall(release, packagecode);

            Console.WriteLine(GetInstallerResultDescription(res));



        }



        private void UnInstallAll(int? release)
        {

            logger.Info("UnInstallAll(release = {0})", release);


            PackageInstaller installer = GetInstaller();

            IList<InstallResult> results = null;

            if (release.HasValue)
            {
                results = installer.UninstallAll(release.Value);
            }
            else
            {
                results = installer.UninstallAll();
            }

            Console.WriteLine("Uninstalled " + results.Count.ToString() + " package(s)");
            Console.WriteLine();

            foreach (var res in results)
            {
                Console.WriteLine(GetInstallerResultDescription(res));
            }

        }

        private PackageInstaller GetInstaller()
        {
            PackageInstaller i = new PackageInstaller(new RevitApplicationUtil());
            return i;
        }

        private IPackageLibrary GetLibrary()
        {
            var lib = new FolderPackageLibrary(Library, true);
            return lib;
        }

        private IList<RequiredPackage> GetRequiredPackages()
        {

            if (File.Exists(RequiredList))
            {
                return RequiredPackage.ReadRequiredPackageList(RequiredList);
            }
            else
            {
                return null;
            }
        }


        private static string GetUpdateResultDescription(UpdateResult result)
        {

            switch (result.Type)
            {
                case UpdateResultType.Upgrade:

                    return "Upgraded from " + result.Previous.ToString() + " to " + result.Current.ToString();


                case UpdateResultType.DowngradeFromSuspended:

                    return "Downgraded from suspended package " + result.Previous.ToString() + " to " + result.Current.ToString();


                case UpdateResultType.UninstallSuspended:

                    return "Uninstalled suspended package " + result.Previous.ToString();

                case UpdateResultType.OfficalInstalled:

                    return "Official version installed " + result.Current.ToString();

                case UpdateResultType.NewerThanOfficalInstalled:

                    return "Newer than official version installed " + result.Current.ToString();

                case UpdateResultType.UnknownOfficalVersion:

                    return "Unknown official version for " + result.Current.ToString();


                case UpdateResultType.InstalledRequired:

                    return "Installed required package " + result.Current.ToString();

                default:
                    throw new InvalidOperationException("Unexpected UpdateResultType");
            }


        }

        private static string GetInstallerResultDescription(InstallResult result)
        {

            switch (result.Type)
            {
                case InstallationType.Install:

                    return result.Current.ToString() + " installed";

                case InstallationType.Reinstall:

                    return result.Current.ToString() + "  reinstalled";

                case InstallationType.Uninstall:

                    return result.Previous.ToString() + " uninstalled";

                case InstallationType.Upgrade:

                    return result.Previous.ToString() + " upgraded to " + result.Current.ToString();

                case InstallationType.Downgrade:

                    return result.Previous.ToString() + " downgraded to " + result.Current.ToString();


                default:
                    throw new InvalidOperationException("Unexpected InstallerResultType");
            }

        }







        public string RequiredList { get; set; }
    }


    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            var c = new ConsoleUtility();
            c.Library = System.Configuration.ConfigurationManager.AppSettings["Library"];
            c.RequiredList = System.Configuration.ConfigurationManager.AppSettings["RequiredList"];
            c.Run(args);
        }
    }
}

