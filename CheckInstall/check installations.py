import os
import time
import datetime
from win32api import GetFileVersionInfo, LOWORD, HIWORD




def get_version_number (filename):
  info = GetFileVersionInfo (filename, "\\")
  ms = info['FileVersionMS']
  ls = info['FileVersionLS']
  return HIWORD (ms), LOWORD (ms), HIWORD (ls), LOWORD (ls)




#Reads Computer list
computerListFileName = "complist.txt"
print("Reading "+computerListFileName+"...")
with open(computerListFileName) as f:
    compList = f.readlines()
compList = [x.strip('\n') for x in compList] #Removes new lines



#Generates Computer List
#compList = list()
#for num in range(0,1000,1):
#  comp = "SFW"+"{0:0>3}".format(num)
#  compList.append(comp)



results = []


for comp in compList:

    cPath = "\\\\"+comp+"\\C$"
    exePath = cPath+"\\Program Files (x86)\\Revit Add-In Library\RvtAddInLibInstaller.exe"

    
    print
    print("Checking "+comp+"...")
    
    foundComp = False
    isInstalled = False
    rvtAddInLibVersion = "";

    #Check if computer can be found
    if os.path.isdir(cPath):

        foundComp = True
        print("\tFound "+comp)


        #Check if Add-In Library is installed
        if os.path.exists(exePath):
          
          isInstalled = True
          print("\tFound Revit Add-In Library installed")
          
          
          #Check if Revit Add-In Library is installed
          rvtAddInLibVersion = get_version_number(exePath)
          print("\tRvtAddInLibInstaller.exe Version: "+str(rvtAddInLibVersion[0])+"."+str(rvtAddInLibVersion[1])+"."+str(rvtAddInLibVersion[2])+"."+str(rvtAddInLibVersion[3]))
          
        else:
          print("\tDid not find Revit Add-In Library installed")
    else:
        print("\tDid not find "+comp)

    results.append([comp,foundComp,isInstalled,rvtAddInLibVersion])            

#Print Results
print
print

header = "Machine\tLocated\tInstalled\tVersion"



      
print("[Revit Add In Library Installed]")
print(header)
for item in results:
    if item[1] == True and item[2] == True:
      print(item[0]+"\t"+str(item[1])+"\t"+str(item[2])+"\t\t"+str(item[3][0])+"."+str(item[3][1])+"."+str(item[3][2])+"."+str(item[3][3]))
print

print("[Revit Add In Library Not Installed]")
print(header)
for item in results:
    if item[1] == True and item[2] == False:
        print(item[0]+"\t"+str(item[1])+"\t"+str(item[2]))
print



print("[Computer Not Found]")
print(header)
for item in results:
    if item[1] == False:
        print(item[0]+"\t"+str(item[1])+"\t"+str(item[2]))
print



    
